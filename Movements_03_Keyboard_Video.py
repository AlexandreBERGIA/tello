import time

from djitellopy import tello
from time import sleep
import Module_KeyPress as keyPress
import time
import cv2

controlSafety = True

keyPress.init()
drone = tello.Tello()
drone.connect()

def getKeyboardInput():
    # Controls
    lr, fb, ud, yv = 0, 0, 0, 0
    # Landing and takeoff
    to, ld = False, False
    # Stream
    son, soff, scap = False, False, False
    # General speed
    speed = 50

    # Movement Controls
    if(keyPress.getKey("LEFT")): lr = -speed
    elif (keyPress.getKey("RIGHT")): lr = speed

    if (keyPress.getKey("UP")): fb = speed
    elif (keyPress.getKey("DOWN")): fb = -speed

    if (keyPress.getKey("w")): ud = speed
    elif (keyPress.getKey("s")): ud = -speed

    if (keyPress.getKey("a")): yv = -speed
    elif (keyPress.getKey("d")): yv = speed

    if (keyPress.getKey("q")): ld = True
    if (keyPress.getKey("e")): to = True

    # Stream Controls
    if (keyPress.getKey("z")): son = True
    elif (keyPress.getKey("x")): soff = True

    # Image Capture
    if (keyPress.getKey("c")): scap = True

    return [lr, fb, ud, yv, ld, to, son, soff, scap]

while True:
    # Get the keyboard inputs
    keyboardInput = getKeyboardInput()
    print(drone.get_battery(), keyboardInput)

    # Uses the keyboad controls for movement
    if not controlSafety:
        drone.send_rc_control(keyboardInput[0], keyboardInput[1], keyboardInput[2], keyboardInput[3])
        if keyboardInput[4]: drone.land()
        if keyboardInput[5]: drone.takeoff()

    # Gets the image
    if keyboardInput[6] and not drone.stream_on:
        drone.streamon()
    elif keyboardInput[7] and drone.stream_on:
        drone.streamoff()

    if drone.stream_on:
        # Display the stream in a new window
        image = drone.get_frame_read().frame
        resizedImage = cv2.resize(image, (360, 240))
        cv2.imshow("ImageFeed", resizedImage)
        cv2.waitKey(5)

        if keyboardInput[8]:
            cv2.imwrite(f'Resources/Images/{time.time()}.jpg', image)

