import logging
import threading
import time
from queue import Queue

import cv2

from ModulesV2.Drones import DroneFactory
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image import ImageFetcherFactory
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher
from ModulesV2.Loggers import Logger

# Declare the initial variables
# drone_ref = DroneFactory.DJI_TELLO
drone_ref = DroneFactory.STUB
computer_camera_index = 1  # -1 for a black image loader, >=0 to use a real connected camera
camera_ref = ImageFetcherFactory.FETCHER_CAMERA_COMPUTER if computer_camera_index >= 0 else ImageFetcherFactory.FETCHER_STUB_BLACK
working_image_size = Point2D(1000, 800)
logging_level = logging.DEBUG
# logging_level = main_logger.info

# Init the logger
logging.basicConfig(format=Logger.MESSAGE_FORMAT_CSV, level=logging_level, datefmt=Logger.TIME_FORMAT)
main_logger = logging.getLogger(Logger.LOGGER_MAIN)

# Defines the drone and connect to it
main_logger.info('Instance the Drone')
drone = DroneFactory.create(drone_ref)
main_logger.info('Drone instance: {}'.format(drone.__class__))
drone.connect()

# Defines the image fetchers to gather images
main_logger.info('Instance Image Fetchers')
image_fetcher_factory = ImageFetcherFactory.ImageFetcherFactory()
image_fetcher_factory.drone = drone
image_fetcher_factory.camera_index = computer_camera_index
drone_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_CAMERA_DRONE)
# hud_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_STUB_BLACK)
camera_image_fetcher = image_fetcher_factory.create(camera_ref)
# status_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_STUB_BLACK)
drone.image_fetcher = drone_image_fetcher

main_logger.info('Start Image Fetcher Threads')


def capture_image(queue_param: Queue, sleep_time: int, image_fetcher: AbstractImageFetcher):
    logging.debug('Init')

    while True:
        logging.debug('Loop Start')
        image_fetcher.fetch()
        image = image_fetcher.get_image()
        queue_param.put(image)

        logging.debug('Queue Size: {}'.format(queue_param.qsize()))

        time.sleep(sleep_time)
        logging.debug('Loop End')


def apply_controls(queue_param: Queue, sleep_time: int):
    logging.debug('Init')

    while True:
        # logging.debug('Loop Start')

        if queue_param.empty():
            time.sleep(0.01)
            # logging.debug('Empty Queue')
        else:
            pressed_key_internal = queue_param.get()

            if pressed_key_internal == 255:
                time.sleep(0.01)
            else:
                logging.debug('Pressed key: {}'.format(pressed_key_internal))

        time.sleep(sleep_time)
        # logging.debug('Loop End')


drone_queue = Queue(1)
camera_queue = Queue(1)
pressed_key_queue = Queue(1)
args_capture_drone = {
    'queue_param': drone_queue,
    'sleep_time': 0.01,
    'image_fetcher': drone_image_fetcher,
}
args_capture_webcam = {
    'queue_param': camera_queue,
    'sleep_time': 0.01,
    'image_fetcher': camera_image_fetcher,
}
args_key_pressed = {
    'queue_param': pressed_key_queue,
    'sleep_time': 0.01
}
main_loop_sleep_time = 0.01

processes = [
    threading.Thread(target=capture_image, kwargs=args_capture_drone, name='Drone Capture'),
    threading.Thread(target=capture_image, kwargs=args_capture_webcam, name='Webcam Capture'),
    threading.Thread(target=apply_controls, kwargs=args_key_pressed, name='Control Apply'),
]

main_logger.info('Start Processes')
for process in processes:
    process.daemon = True
    process.start()

main_logger.info('Start Main Loop')
while True:
    # Sets the time start of the loop
    logging.debug('Loop Start')

    if not drone_queue.empty():
        cv2.imshow('Drone', drone_queue.get())
        logging.debug('Drone Image Displayed')
    else:
        logging.debug('Drone Image Not Available')

    if not camera_queue.empty():
        cv2.imshow('Camera', camera_queue.get())
        logging.debug('Camera Image Displayed')
    else:
        logging.debug('Camera Image Not Available')

    pressed_key = cv2.waitKey(1) & 0xFF
    if pressed_key == ord('q'):
        break
    else:
        pressed_key_queue.put(pressed_key)

    time.sleep(main_loop_sleep_time)

    logging.debug('Loop End')

main_logger.info('End')
