# A graphical version of the Joystick that can be painted in the HUD

import cv2
from Modules.HUD.HUDElement.Element import Element
from Modules.Control.Joystick import Joystick as ControlJoystick


class Joystick(Element):
    def __init__(self, controlJoystick: ControlJoystick):
        self.controlJoystick = controlJoystick
        self.joyColors = [(0, 255, 255), (255, 255, 0)]
        self.joyMinThick = 1
        self.joyMaxThick = 3
        self.joyHandRadius = 4
        self.joyHandThick = 6
        self.joyMaxRadius = self.controlJoystick.joyMaxRadius

        # Paint the joystick values
        self.paintJoystickValues = True
        self.valueSize = 0.7
        self.valueFont = cv2.FONT_HERSHEY_SIMPLEX
        self.valueColor = (255, 255, 255)
        self.valueThick = 2

    # If the Joystick is enabled, draws the main joystick targets.
    def paint(self, image):
        # Returns if the joystick is disabled
        if not self.controlJoystick.isJoystickEnabled():
            return

        # Paint the Joystick cirles
        index = 0
        for joystickCenter in self.controlJoystick.getJoystickCenters():
            # Min Radius
            cv2.circle(
                image,
                joystickCenter,
                self.controlJoystick.joyMinRadius,
                self.joyColors[index],
                self.joyMinThick
            )
            # Draw the Max Radius
            cv2.circle(
                image,
                joystickCenter,
                self.controlJoystick.joyMaxRadius,
                self.joyColors[index],
                self.joyMaxThick
            )

            index += 1

        # Pain the joystick Inputs
        self._paintJoystickInputs(image)

    # If the joystick is enabled, draws the position of the sticks
    def _paintJoystickInputs(self, image):
        joystickInputs = self.controlJoystick.getJoystickInputs()
        # Returns if the joystick is disabled
        if not self.controlJoystick.isJoystickEnabled():
            return

        # Daw the joystick positions
        for index in range(len(joystickInputs)):
            locationX = self.controlJoystick.getJoystickCenters()[index][0] + int(joystickInputs[index][0] * self.controlJoystick.joyMaxRadius / 100)
            locationY = self.controlJoystick.getJoystickCenters()[index][1] - int(joystickInputs[index][1] * self.controlJoystick.joyMaxRadius / 100)
            cv2.circle(image, (locationX, locationY), self.joyHandRadius, self.joyColors[index], self.joyHandThick)

            if self.paintJoystickValues:
                valueLocationX = self.controlJoystick.getJoystickCenters()[index][0] + self.joyMaxRadius + 5
                valueLocationY = self.controlJoystick.getJoystickCenters()[index][1]
                cv2.putText(
                    image,
                    str(self.controlJoystick.percentJoystickInputs[index][0]) + "%",
                    (valueLocationX, valueLocationY),
                    self.valueFont,
                    self.valueSize,
                    self.valueColor,
                    self.valueThick
                )

                valueLocationX = self.controlJoystick.getJoystickCenters()[index][0]
                valueLocationY = self.controlJoystick.getJoystickCenters()[index][1] - self.joyMaxRadius - 5
                cv2.putText(
                    image,
                    str(self.controlJoystick.percentJoystickInputs[index][1]) + "%",
                    (valueLocationX, valueLocationY),
                    self.valueFont,
                    self.valueSize,
                    self.valueColor,
                    self.valueThick
                )

