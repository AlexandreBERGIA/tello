from Modules.HUD.HUDElement import Label, Value, ValueControl
from Modules.Control import DroneControl


class Inputs:
    def paint(self, image, droneControl: DroneControl):
        hudValues = [
            Label.Label('Frame time: ' + str(droneControl.droneInputs["SystemStatusLoopTime"].getValue()) + " ms"),
            Label.Label('Drone Enabled: ' + str(droneControl.droneInputs["StatusDroneEnabled"].getValue())),
            Label.Label('Control Enabled: ' + str(droneControl.droneInputs["StatusDroneControlEnabled"].getValue())),
            Label.Label('Battery: ' + str(droneControl.droneInputs["StatusDroneBattery"].getValue()) + " %"),
            Label.Label('Temperature: ' + str(droneControl.droneInputs["StatusDroneTemperature"].getValue()) + " F"),
            Label.Label('Drone Flying: ' + str(droneControl.droneInputs["StatusDroneFlying"].getValue())),
            Label.Label('Height: ' + str(droneControl.droneInputs["StatusDroneHeight"].getValue()) + " cm"),
            Label.Label(),
            Label.Label('Right Hand'),
            ValueControl.ValueControl(
                'Command HUD',
                droneControl.droneInputs["CommandHUDSwitch"],
                "",
                DroneControl.HandPatternHUDCommandSwitch[1],
                True
            ),
            ValueControl.ValueControl(
                'Command Movement',
                droneControl.droneInputs["CommandDroneMovementSwitch"],
                "",
                DroneControl.HandFlightPatternCommandSwitch[1],
                True
            ),
            ValueControl.ValueControl(
                'Command Control',
                droneControl.droneInputs["CommandControlTransferSwitch"],
                "",
                DroneControl.HandPatternCommandControlTransferSwitch[1],
                True
            ),
            ValueControl.ValueControl(
                'Command Media',
                droneControl.droneInputs["CommandDroneMediaSwitch"],
                "",
                DroneControl.HandMediaPatternCommandSwitch[1],
                True
            ),
            Label.Label(),
            Label.Label('Left Hand'),
        ]

        if droneControl.droneInputs["CommandHUDSwitch"].getInternalValue():
            hudValues.append(Value.Value('Joystick', droneControl.droneInputs["StatusHUDJoystickEnabled"]))
            hudValues.append(ValueControl.ValueControl(
                'Joystick Enable',
                droneControl.droneInputs["CommandHUDJoystickEnable"],
                "",
                DroneControl.HandPatternHUDJoystickInit[0]
            ))
            hudValues.append(ValueControl.ValueControl(
                'Joystick Disable',
                droneControl.droneInputs["CommandHUDJoystickDisable"],
                "",
                DroneControl.HandPatternHUDJoystickTerminate[0]
            ))
            hudValues.append(Value.Value('Scheme', droneControl.droneInputs["StatusHUDColorScheme"]))
            hudValues.append(ValueControl.ValueControl(
                'Scheme Day',
                droneControl.droneInputs["CommandHUDColorSchemeDay"],
                "",
                DroneControl.HandPatternHUDSchemeDay[0]
            ))
            hudValues.append(ValueControl.ValueControl(
                'Scheme Night',
                droneControl.droneInputs["CommandHUDColorSchemeNight"],
                "",
                DroneControl.HandPatternHUDSchemeNight[0]
            ))

        if droneControl.droneInputs["CommandDroneMovementSwitch"].getInternalValue():
            hudValues.append(ValueControl.ValueControl(
                'Take Off',
                droneControl.droneInputs["CommandDroneTakeoff"],
                "",
                DroneControl.HandPatternCommandDroneTakeOff[0]
            ))
            hudValues.append(ValueControl.ValueControl(
                'Landing',
                droneControl.droneInputs["CommandDroneLand"],
                "",
                DroneControl.HandPatternCommandDroneLand[0]
            ))
            hudValues.append(Value.Value())

        if droneControl.droneInputs["CommandControlTransferSwitch"].getInternalValue():
            hudValues.append(ValueControl.ValueControl(
                'Trans. Ctrl. To Far',
                droneControl.droneInputs["CommandControlTransferToFar"],
                "",
                DroneControl.HandPatternCommandControlTransferToFar[0]
            ))
            hudValues.append(ValueControl.ValueControl(
                'Trans. Ctrl. To Near',
                droneControl.droneInputs["CommandControlTransferToNear"],
                "",
                DroneControl.HandPatternCommandControlTransferToNear[0]
            ))

        if droneControl.droneInputs["CommandDroneMediaSwitch"].getInternalValue():
            hudValues.append(ValueControl.ValueControl(
                'Take Picture',
                droneControl.droneInputs["CommandDroneTakePicture"],
                "",
                DroneControl.HandPatternCommandDroneTakePicture[0]
            ))
            hudValues.append(ValueControl.ValueControl(
                'Recording',
                droneControl.droneInputs["CommandDroneVideoRecording"],
                "",
                DroneControl.HandPatternCommandDroneStartRecording[0]
            ))
            hudValues.append(ValueControl.ValueControl(
                'Recording Start',
                droneControl.droneInputs["CommandDroneStartRecording"],
                "",
                DroneControl.HandPatternCommandDroneStartRecording[0]
            ))
            hudValues.append(ValueControl.ValueControl(
                'Recording Stop',
                droneControl.droneInputs["CommandDroneStopRecording"],
                "",
                DroneControl.HandPatternCommandDroneStopRecording[0]
            ))
            hudValues.append(Value.Value())

        index = 0
        for hudValue in hudValues:
            hudValue.colorScheme = droneControl.droneInputs["StatusHUDColorScheme"].getValue()
            hudValue.index = index
            hudValue.paint(image)
            index += 1