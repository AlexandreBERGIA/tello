from Modules.Control import DroneControl
import Modules.HUD.HUDElement.Graphics.AirSpeed as AirSpeed
import Modules.HUD.HUDElement.Graphics.BarometerAltitude as BarometerAltitude
import Modules.HUD.HUDElement.Graphics.Horizon as Horizon


class Drone:

    def __init__(self):
        self.isPaintHUD: bool = True
        self.isPaintHUDJoystick: bool = True
        self.isPaintHUDGraphics: bool = True

    def paint(self, joystickImage, droneImage, droneControl: DroneControl):
        if not self.isPaintHUD:
            return

        if self.isPaintHUDJoystick:
            self.paintHUDJoystick(joystickImage, droneControl)

        if self.isPaintHUDGraphics:
            self.paintHUDGraphics(droneImage, droneControl)

    def paintHUDJoystick(self, image, droneControl: DroneControl):
        # Enable / Disable the joystick
        if droneControl.droneInputs["CommandHUDJoystickDisable"].getValue():
            droneControl.joystick.joystickDisable()
        if droneControl.droneInputs["CommandHUDJoystickEnable"].getValue():
            droneControl.joystick.joystickEnable(droneControl.droneInputs["StatusHUDJoystickCenters"].getValue())

    def paintHUDGraphics(self, image, droneControl: DroneControl):
        airSpeed = AirSpeed.AirSpeed(str(droneControl.droneInputs["StatusDroneSpeedX"].getValue()))
        airSpeed.colorScheme = droneControl.droneInputs["StatusHUDColorScheme"].getValue()
        airSpeed.paint(image)

        barometerAltitude = BarometerAltitude.BarometerAltitude(
            str(droneControl.droneInputs["StatusDroneBarometer"].getValue())
        )
        barometerAltitude.colorScheme = droneControl.droneInputs["StatusHUDColorScheme"].getValue()
        barometerAltitude.paint(image)

        horizon = Horizon.Horizon(
            droneControl.droneInputs["StatusDronePitch"].getValue(),
            droneControl.droneInputs["StatusDroneRoll"].getValue()
        )
        horizon.colorScheme = droneControl.droneInputs["StatusHUDColorScheme"].getValue()
        horizon.paint(image)
