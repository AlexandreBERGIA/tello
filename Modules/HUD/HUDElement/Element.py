# A HUD Elements is an element that can be rendered in the HUD


class Element:
    # Declares the different colors for the graphics and text
    COLOR_DAY = 1
    COLOR_NIGHT = 2

    def __init__(self):
        self.colorScheme = Element.COLOR_DAY

    def paint(self, image):
        """The content should be overwritten by a subclass"""
        pass

    def getElementColor(self):
        if self.colorScheme == Element.COLOR_DAY:
            return 255, 255, 255
        elif self.colorScheme == Element.COLOR_NIGHT:
            return 0, 255, 0