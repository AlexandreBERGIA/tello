from Modules.DroneProperties import DroneProperty
from Modules.HUD.HUDElement.Value import Value


class ValueControl(Value):
    def __init__(
            self,
            label: str = "",
            droneInput: DroneProperty = None,
            suffix: str = "",
            handPattern: list = [0, 0, 0, 0, 0],
            is_right_hand: bool = False
    ):
        super(ValueControl, self).__init__(
            label,
            droneInput,
            suffix
        )
        self.is_right_hand: bool = is_right_hand
        self.handPattern: list = handPattern
        self.handPattern1: str = "|"
        self.handPattern0: str = "-"
        self.handPatternS: str = "*"

    def _paintText(self, image):
        self.suffix += "" + self.getTextHandPattern()
        super()._paintText(image)

    def getTextHandPattern(self):
        textPattern: str = "   "

        # Revere the array if needed
        if not self.is_right_hand:
            self.handPattern = self.handPattern[::-1]

        for pattern in self.handPattern:
            if pattern == 0:
                textPattern += self.handPattern0
            elif pattern == 1:
                textPattern += self.handPattern1
            elif pattern == "*":
                textPattern == self.handPatternS

        return textPattern
