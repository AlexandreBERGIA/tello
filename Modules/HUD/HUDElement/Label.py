import cv2
from Modules.HUD.HUDElement.Element import Element


class Label(Element):
    def __init__(self, label: str = "", index: int = 0):
        super(Label, self).__init__()

        self.textSize = 1
        self.textFont = cv2.FONT_HERSHEY_SIMPLEX
        self.textThick = 2

        self.startingPositionX = 30
        self.startingPositionY = 30
        self.indexSize = 30

        self.label = label
        self.index = index

    def paint(self, image):
        # Draw the status
        self._paintText(image)

    def _paintText(self, image):
        # Define the text position
        textPositionX = self.startingPositionX
        textPositionY = self.startingPositionY + (self.index * self.indexSize)

        text = self.label

        # Draw
        cv2.putText(
            image,
            text,
            (textPositionX, textPositionY),
            self.textFont,
            self.textSize,
            self.getElementColor(),
            self.textThick
        )
