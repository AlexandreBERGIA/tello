import cv2
import math
from Modules.Math import Math2D
from Modules.HUD.HUDElement.Element import Element


class Horizon(Element):
    def __init__(self, dronePitch: int = 0, droneRoll: int = 0):
        super(Horizon, self).__init__()

        self.rectangleSize = (160, 40)
        self.rectanglePaddingTop = 400
        self.rectanglePaddingHorizontal = 300

        self.graphicThick = 5

        self.textSize = 1
        self.textFont = cv2.FONT_HERSHEY_SIMPLEX
        self.textThick = 2

        self.startingPositionX = 30
        self.startingPositionY = 30
        self.indexSize = 30

        self.horizonLinesQuantity: int = 5
        self.horizonDegreeIncrement: int = 5

        self.dronePitch = dronePitch
        self.droneRoll = droneRoll

    def paint(self, image):
        hzPrimaryLength = 800
        hzSecondaryLength = 200
        hzEndingLength = 15
        hzSecondaryIncrement = 22
        pitchCoeff = 23  # Helps converting the input pitch to a translation in pixels
        pitchYOffset = -5  # Takes into account the natural angle of the camera

        # Convert the roll from degrees to radian
        droneRollRad = self.droneRoll * math.pi / 180

        # Get the Horizon Center point. For now, it is centered in the image
        hzCenterPoint = int(image.shape[1] / 2), int(image.shape[0] / 2 + (pitchYOffset * pitchCoeff))
        # Draw the center point
        cv2.circle(
            image,
            hzCenterPoint,
            5,
            self.getElementColor(),
            6
        )

        indexes = self.getHorizonIndexList(self.dronePitch)
        for index in indexes:
            # Determine the length of the horizontal bar
            hzLength = hzSecondaryLength
            if index == 0: hzLength = hzPrimaryLength

            # Determine the sign based on the index
            sign = 1
            if index < 0: sign = -1

            # Set the points for the horizon line
            hzPoint1X = int(hzCenterPoint[0] - hzLength / 2)
            hzPoint1Y = hzCenterPoint[1] + hzSecondaryIncrement * -index
            hzPoint2X = int(hzCenterPoint[0] + hzLength / 2)
            hzPoint2Y = hzCenterPoint[1] + hzSecondaryIncrement * -index
            # Set the end lines
            hzPointEnding1X = hzPoint1X
            hzPointEnding1Y = hzPoint1Y + (hzEndingLength * sign)
            hzPointEnding2X = hzPoint2X
            hzPointEnding2Y = hzPoint2Y + (hzEndingLength * sign)
            # Set the text representing the pitch
            textAdjustY = 10 * sign
            hzText1X = hzPoint1X - 40
            hzText1Y = hzPoint1Y + textAdjustY
            hzText2X = hzPoint2X + 15
            hzText2Y = hzPoint1Y + textAdjustY

            # Shift the points based on the pitch
            hzPoint1Y -= -self.dronePitch * pitchCoeff
            hzPoint2Y -= -self.dronePitch * pitchCoeff
            hzPointEnding1Y -= -self.dronePitch * pitchCoeff
            hzPointEnding2Y -= -self.dronePitch * pitchCoeff
            hzText1Y -= -self.dronePitch * pitchCoeff
            hzText2Y -= -self.dronePitch * pitchCoeff

            # Rotate the horizon line
            hzPoint1X, hzPoint1Y = Math2D.rotate((hzPoint1X, hzPoint1Y), hzCenterPoint, -droneRollRad)
            hzPoint2X, hzPoint2Y = Math2D.rotate((hzPoint2X, hzPoint2Y), hzCenterPoint, -droneRollRad)
            # Rotate the end lines
            hzPointEnding1X, hzPointEnding1Y = Math2D.rotate((hzPointEnding1X, hzPointEnding1Y), hzCenterPoint, -droneRollRad)
            hzPointEnding2X, hzPointEnding2Y = Math2D.rotate((hzPointEnding2X, hzPointEnding2Y), hzCenterPoint, -droneRollRad)
            # Rotate the text
            hzText1X, hzText1Y = Math2D.rotate((hzText1X, hzText1Y), hzCenterPoint, -droneRollRad)
            hzText2X, hzText2Y = Math2D.rotate((hzText2X, hzText2Y), hzCenterPoint, -droneRollRad)

            cv2.line(
                image,
                (hzPoint1X, hzPoint1Y),
                (hzPoint2X, hzPoint2Y),
                self.getElementColor(),
                self.graphicThick,
            )
            # Draw the end lines
            cv2.line(
                image,
                (hzPoint1X, hzPoint1Y),
                (hzPointEnding1X, hzPointEnding1Y),
                self.getElementColor(),
                self.graphicThick
            )
            cv2.line(
                image,
                (hzPoint2X, hzPoint2Y),
                (hzPointEnding2X, hzPointEnding2Y),
                self.getElementColor(),
                self.graphicThick
            )
            if index == 0: continue
            # Draw the text
            cv2.putText(
                image,
                str(index),
                (hzText1X, hzText1Y),
                self.textFont,
                self.textSize,
                self.getElementColor(),
                self.textThick
            )
            cv2.putText(
                image,
                str(index),
                (hzText2X, hzText2Y),
                self.textFont,
                self.textSize,
                self.getElementColor(),
                self.textThick
            )

    def getHorizonIndexList(self, pitch: int = 0) -> list:
        centerHorizonLine = int(pitch / self.horizonDegreeIncrement) * self.horizonDegreeIncrement
        start = centerHorizonLine - (self.horizonLinesQuantity * self.horizonDegreeIncrement)
        end = centerHorizonLine + (self.horizonLinesQuantity * self.horizonDegreeIncrement)
        indexList = [*range(start, end, self.horizonDegreeIncrement)]

        # Trim the values above and below +90 and -90
        indexList[:] = [x for x in indexList if -90 <= x <= 90]

        return indexList

