import cv2
from Modules.HUD.HUDElement.Element import Element


class AirSpeed(Element):
    def __init__(self, airSpeed: str = ""):
        super(AirSpeed, self).__init__()

        self.rectangleSize = (160, 40)
        self.rectanglePaddingTop = 400
        self.rectanglePaddingHorizontal = 300

        self.graphicThick = 5

        self.textSize = 1
        self.textFont = cv2.FONT_HERSHEY_SIMPLEX
        self.textThick = 2

        self.startingPositionX = 30
        self.startingPositionY = 30
        self.indexSize = 30

        self.airSpeed = airSpeed

    def paint(self, image):
        rectangleStartPoint = (self.rectanglePaddingHorizontal, self.rectanglePaddingTop)
        rectangleEndPoint = (
            rectangleStartPoint[0] + self.rectangleSize[0],
            rectangleStartPoint[1] + self.rectangleSize[1]
        )
        cv2.rectangle(image, rectangleStartPoint, rectangleEndPoint, self.getElementColor(), self.graphicThick)

        textStartPoint = (rectangleStartPoint[0] + 10, int(rectangleStartPoint[1] + self.textSize * 30))
        cv2.putText(
            image,
            self.airSpeed,
            textStartPoint,
            self.textFont,
            self.textSize,
            self.getElementColor(),
            self.textThick
        )
