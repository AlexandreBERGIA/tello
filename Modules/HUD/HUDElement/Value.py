import cv2
from Modules.DroneProperties import DroneProperty
from Modules.HUD.HUDElement.Element import Element


class Value(Element):
    def __init__(
        self,
        label: str = "",
        droneInput: DroneProperty = None,
        suffix: str = "",
        index: int = 0,
        displayEnabledStatus: bool = True
    ):
        super(Value, self).__init__()

        self.textSize = 1
        self.textFont = cv2.FONT_HERSHEY_SIMPLEX
        self.textThick = 2

        self.startingPositionX = 30
        self.startingPositionY = 30
        self.indexSize = 30
        self.enabledCircleRadius = 10
        self.enabledCircleTextPadding = 8
        self.enabledCircleThick = 8
        self.enabledCircleColorD = (22, 22, 204)

        self.label = label
        self.droneInput = droneInput
        self.suffix = suffix
        self.index = index
        self.displayEnabledStatus = displayEnabledStatus

    def paint(self, image):
        # Draw the status
        self._paintStatus(image)
        self._paintText(image)

    def _paintStatus(self, image):
        # If both the label and the DroneInput are empty, don't render anything
        if self.label == "" and self.droneInput is None: return

        # Define how the Enabled circle will be rendered
        circleCenterX = self.startingPositionX + self.enabledCircleRadius + self.enabledCircleThick
        circleCenterY = self.startingPositionY + (self.index * self.indexSize) - self.enabledCircleRadius
        if (self.droneInput is None or not self.droneInput.isEnabled) and self.displayEnabledStatus:
            cv2.circle(
                image,
                (circleCenterX, circleCenterY),
                self.enabledCircleRadius,
                self.enabledCircleColorD,
                self.enabledCircleThick
            )

    def _paintText(self, image):
        # Define the text position
        textPositionX = self.startingPositionX + self.enabledCircleRadius * 2 + self.enabledCircleThick * 2 + self.enabledCircleTextPadding
        textPositionY = self.startingPositionY + (self.index * self.indexSize)

        text = self.label
        if self.droneInput is not None: text += ": " + str(self.droneInput.getInternalValue())
        text += self.suffix

        # Draw
        cv2.putText(
            image,
            text,
            (textPositionX, textPositionY),
            self.textFont,
            self.textSize,
            self.getElementColor(),
            self.textThick
        )