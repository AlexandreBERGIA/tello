import math


def rotate(point, center, angle):
    x = ((point[0] - center[0]) * math.cos(angle)) - ((point[1] - center[1]) * math.sin(angle)) + center[0]
    y = ((point[0] - center[0]) * math.sin(angle)) + ((point[1] - center[1]) * math.cos(angle)) + center[1]

    x = int(x)
    y = int(y)
    return x, y