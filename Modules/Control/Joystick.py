# The Joystick class helps drawing and getting control inputs on a picture
import math
import numpy
from Modules.Control import Hands as Hands


class Joystick:
    def __init__(self):
        # Declare the Joystick Centers
        self.rightJoystickIndex = 0
        self.leftJoystickIndex = 1

        # Defines the Joystick Min, max, Hand Position display
        self.joyMinRadius = 35
        self.joyMaxRadius = 170

        # Declare the internal variables
        self.defaultJoystickInputs = [(0, 0), (0, 0)]

        # Contains the joystick center coordinates when the joystick is enabled
        self._joystickCenters = self.defaultJoystickInputs
        self.percentJoystickInputs = self.defaultJoystickInputs

        self.hands: list = None

    # Returns True if the joystick is enabled
    def isJoystickEnabled(self):
        return self._joystickCenters != self.defaultJoystickInputs

    # Sets the joystick to enable status
    def joystickEnable(self, joystickCenters):
        self._joystickCenters = joystickCenters

    # Sets the joystick to disable status
    def joystickDisable(self):
        self._joystickCenters = self.defaultJoystickInputs

    # Returns the Joystick Centers
    def getJoystickCenters(self):
        return self._joystickCenters

    # This is expressed as the actual length between the Joystick center and the Hand location
    # If the joystick is disabled, or there are no hands detected, returns False
    # If both hands are detected, [0] is right hand, [1] is left hand
    def _getRawJoystickInputs(self):
        handLocations = Hands.getHandsCenters(self.hands)

        joystickInputs = [None] * len(handLocations)
        handIndex = 0
        for handLocation in handLocations:
            if handLocation == (0, 0):
                joystickX = 0
                joystickY = 0
                joystickInputs[handIndex] = (joystickX, joystickY)
            else:
                # Reverse the values of Y since the top left corner is 0,0
                joystickY = -(handLocation[1] - self._joystickCenters[handIndex][1])
                joystickX = handLocation[0] - self._joystickCenters[handIndex][0]
                joystickInputs[handIndex] = (joystickX, joystickY)
            handIndex += 1

        return joystickInputs

    # Returns the joystick input values expressed in %. The percentage can go over 100% if not trimmed
    def getJoystickInputs(self):
        rawJoystickInputs = self._getRawJoystickInputs()

        # If the joystick is disabled or there are no joystick inputs,
        if not self.isJoystickEnabled() or not rawJoystickInputs:
            return self.defaultJoystickInputs

        percentJoystickInputs = []
        for rawJoystickInput in rawJoystickInputs:
            joystickDistance = math.hypot(rawJoystickInput[0], rawJoystickInput[1])

            # If the distance is 0, there is no need to perform any calculation
            if joystickDistance == 0:
                percentJoyX = 0
                percentJoyY = 0
            # If the hand is no longer here (0, 0)
            elif rawJoystickInput == (0, 0):
                percentJoyX = 0
                percentJoyY = 0
            else:
                # Trim the joystick distance so it never goes above the max joystick radius
                joystickDistance = int(numpy.clip(joystickDistance, 0, self.joyMaxRadius))

                # Adds a deadzone that corresponds to the min joystick radius
                trimDistance = int(numpy.clip(joystickDistance, 0, self.joyMinRadius))
                joystickDistance -= trimDistance

                # Transform the value in %
                joystickAngle = math.atan2(rawJoystickInput[1], rawJoystickInput[0])
                trimmedJoyX = joystickDistance * math.cos(joystickAngle)
                trimmedJoyY = joystickDistance * math.sin(joystickAngle)
                percentJoyX = int(trimmedJoyX / (self.joyMaxRadius - self.joyMinRadius) * 100)
                percentJoyY = int(trimmedJoyY / (self.joyMaxRadius - self.joyMinRadius) * 100)

            percentJoystickInputs.append((percentJoyX, percentJoyY))
        self.percentJoystickInputs = percentJoystickInputs
        return percentJoystickInputs
