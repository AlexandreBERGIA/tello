# All this module is built based on the fact that we will work with two hands and that
# One if the right hand, the other one is the left hand

rightHandIndex, leftHandIndex = 1, 0
applyFingerCorrection = True


# Returns a pair X, Y location for a given handPoint that contains more information
def getHandPointLocation(handPoint):
    return handPoint[0], handPoint[1]


def getWorkingHands(hands):
    if hands is None or len(hands) == 0 or len(hands) == 1:
        workingHands = []
    elif len(hands) == 2:
        workingHands = [None] * len(hands)
        if hands[0]['type'] == 'Right':
            workingHands[rightHandIndex] = hands[0]
            workingHands[leftHandIndex] = hands[1]
        else:
            workingHands[leftHandIndex] = hands[0]
            workingHands[rightHandIndex] = hands[1]
    else:
        raise Exception("Too many hand provided: "+str(len(hands)))

    return workingHands


# Returns True if the passed hands match the fingerPattern
# Expected Finger Pattern for one or two hands
# [0] => Right hand
#         [0, 0, 0, 0, 0] => Finger values
# [1] => Left hand
#         [0, 0, 0, 0, 0] => Finger values
#
# Finger values
# 0 => Finger closed
# 1 => Finger opened
def matchesFingerPattern(detector, hands, fingerPattern):
    workingHands = getWorkingHands(hands)

    # Return false if the hand quantity doesn't match the finger pattern
    if len(fingerPattern) != len(workingHands): return False

    for handIndex in range(len(workingHands)):
        for fingerIndex in range(5):
            fingerValue = detector.fingersUp(workingHands[handIndex])[fingerIndex]

            # If the finger pattern has the wildcard, skip the validation
            if fingerPattern[handIndex][fingerIndex] == "*":
                continue

            # Apply correction for thumbs when asking FlipType = False
            if applyFingerCorrection and fingerIndex == 0:
                fingerValue = int(not fingerValue)

            # If the actual hand finger doesn't match the finger pattern, return false
            if fingerValue != fingerPattern[handIndex][fingerIndex]:
                return False

    # The fingers match the finger patter if returning here
    return True


def handsDetected(hands):
    return len(hands) >= 2


def getHandsCenters(hands):
    handsCenters = [(0, 0), (0, 0)]

    # If there are no hands, return right away
    workingHands = getWorkingHands(hands)

    index = 0
    for hand in workingHands:
        handsCenters[index] = getHandPointLocation(hand['lmList'][9])
        index += 1

    return handsCenters
