# This module allows controlling the Drone via hand gestures
import time
import cv2
from Modules.Control import Joystick as Joystick, Hands as Hands
from Modules.DroneProperties import DroneProperty, DronePropertyTime
from Modules.HUD.HUDElement.Element import Element
from Modules.HUD.HUDElement.Value import Value

_HandPatternAnyFinger = ['*', '*', '*', '*', '*']

_HandHUDPatternCommand = [1, 0, 0, 0, 0]
HandPatternHUDCommandSwitch = [_HandPatternAnyFinger, _HandHUDPatternCommand] # HUD Pattern Switch
HandPatternHUDJoystickInit = [[1, 0, 0, 0, 0], _HandHUDPatternCommand]
HandPatternHUDJoystickTerminate = [[0, 0, 0, 0, 1], _HandHUDPatternCommand]
HandPatternHUDSchemeDay = [[0, 1, 0, 0, 0], _HandHUDPatternCommand]
HandPatternHUDSchemeNight = [[0, 1, 1, 0, 0], _HandHUDPatternCommand]

_HandFlightPatternCommand = [0, 0, 0, 0, 1]
HandFlightPatternCommandSwitch = [_HandPatternAnyFinger, _HandFlightPatternCommand] # Flight Pattern Switch
HandPatternCommandDroneTakeOff = [[0, 0, 0, 0, 1], _HandFlightPatternCommand]  # Small Finger Up
HandPatternCommandDroneLand = [[1, 0, 0, 0, 0], _HandFlightPatternCommand]  # Thumb Up

_HandControlPatternCommand = [1, 1, 1, 1, 1]
HandPatternCommandControlTransferSwitch = [_HandPatternAnyFinger, _HandControlPatternCommand]
HandPatternCommandControlTransferToFar = [[0, 1, 1, 0, 0], _HandControlPatternCommand]  # Victory
HandPatternCommandControlTransferToNear = [[1, 1, 1, 1, 1], _HandControlPatternCommand]  # Open Hand

_HandMediaPatternCommand = [0, 1, 1, 0, 0]
HandMediaPatternCommandSwitch = [_HandPatternAnyFinger, _HandMediaPatternCommand]  # Media Pattern Switch
HandPatternCommandDroneTakePicture = [[0, 1, 1, 0, 0], _HandMediaPatternCommand]  # Index and Middle up
HandPatternCommandDroneStartRecording = [[0, 1, 1, 1, 0], _HandMediaPatternCommand]  # Index, Middle, and Ring up
HandPatternCommandDroneStopRecording = [[0, 1, 1, 1, 1], _HandMediaPatternCommand]  # Index, Middle, Ring, and small up


class DroneControl:
    def __init__(self, drone, joystick: Joystick, droneEnabled, droneControlEnabled):
        self.drone = drone
        self.joystick = joystick

        self.HUDColorScheme = Element.COLOR_DAY

        self.droneEnabled = droneEnabled
        self.droneControlEnabled = droneControlEnabled
        self.droneVideoRecording = False
        self.fb = 0
        self.yv = 0
        self.ud = 0
        self.lr = 0

        # List all the possible controls
        self.droneInputs = {
            "SystemStatusLoopTime": DronePropertyTime.DronePropertyTime(0, True, 0, DronePropertyTime.OPERATION_AVERAGE, 300),
            "StatusDroneEnabled": DroneProperty.DroneProperty(self.droneEnabled, self.droneEnabled, False),
            "StatusDroneControlEnabled": DroneProperty.DroneProperty(self.droneControlEnabled, self.droneControlEnabled,
                                                                     False),
            "CommandHUDSwitch": DroneProperty.DroneProperty(False, True, False),
            "CommandHUDJoystickEnable": DroneProperty.DroneProperty(False, False, False),
            "CommandHUDJoystickDisable": DroneProperty.DroneProperty(False, False, False),
            "StatusHUDJoystickEnabled": DroneProperty.DroneProperty(False, True, False),
            "StatusHUDJoystickCenters": DroneProperty.DroneProperty(False, False, False),

            "CommandHUDColorSchemeDay": DroneProperty.DroneProperty(False, False, False),
            "CommandHUDColorSchemeNight": DroneProperty.DroneProperty(False, False, False),
            "StatusHUDColorScheme": DroneProperty.DroneProperty(Element.COLOR_DAY, True, 0),

            "CommandDroneMovementSwitch": DroneProperty.DroneProperty(False, True, False),
            "CommandDroneLand": DroneProperty.DroneProperty(False, False, False),
            "CommandDroneTakeoff": DroneProperty.DroneProperty(False, False, False),
            "StatusDroneFlying": DroneProperty.DroneProperty(False, False, False),

            "CommandControlTransferSwitch": DroneProperty.DroneProperty(False, True, False),
            "CommandControlTransferToFar": DroneProperty.DroneProperty(False, False, False),
            "CommandControlTransferToNear": DroneProperty.DroneProperty(False, False, False),

            "CommandDroneMediaSwitch": DroneProperty.DroneProperty(False, True, False),
            "CommandDroneTakePicture": DroneProperty.DroneProperty(False, False, False),
            "CommandDroneVideoRecording": DroneProperty.DroneProperty(False, False, False),
            "CommandDroneStartRecording": DroneProperty.DroneProperty(False, False, False),
            "CommandDroneStopRecording": DroneProperty.DroneProperty(False, False, False),

            "StatusDroneBattery": DroneProperty.DroneProperty(0, False, "N/A"),
            "StatusDroneTemperature": DroneProperty.DroneProperty(0, False, "N/A"),
            "StatusDroneHeight": DroneProperty.DroneProperty(0, False, "N/A"),
            "StatusDroneFlightTime": DroneProperty.DroneProperty(0, False, "N/A"),
            "StatusDroneBarometer": DroneProperty.DroneProperty(0, False, "N/A"),
            "StatusDronePitch": DroneProperty.DroneProperty(0, False, 0),
            "StatusDroneRoll": DroneProperty.DroneProperty(0, False, 0),
            "StatusDroneYaw": DroneProperty.DroneProperty(0, False, 0),
            "StatusDroneSpeedX": DroneProperty.DroneProperty(0, False, 0),
            "StatusDroneSpeedY": DroneProperty.DroneProperty(0, False, 0),
            "StatusDroneSpeedZ": DroneProperty.DroneProperty(0, False, 0),
        }

        self.hud = Value()

    def setInputs(self, hands, detector):
        commandHUDSwitch = Hands.matchesFingerPattern(detector, hands, HandPatternHUDCommandSwitch)
        self.droneInputs["CommandHUDSwitch"].setValue(commandHUDSwitch)
        if commandHUDSwitch:
            # Joystick set and remove
            joystickEnable = Hands.matchesFingerPattern(detector, hands, HandPatternHUDJoystickInit)
            joystickDisable = Hands.matchesFingerPattern(detector, hands, HandPatternHUDJoystickTerminate)
            self.droneInputs["CommandHUDJoystickEnable"].setValue(joystickEnable)
            self.droneInputs["CommandHUDJoystickEnable"].isEnabled = not self.joystick.isJoystickEnabled()
            self.droneInputs["CommandHUDJoystickDisable"].setValue(joystickDisable)
            self.droneInputs["CommandHUDJoystickDisable"].isEnabled = self.joystick.isJoystickEnabled()
            self.droneInputs["StatusHUDJoystickEnabled"].setValue(self.joystick.isJoystickEnabled())

            self.droneInputs["StatusHUDJoystickCenters"].setValue(Hands.getHandsCenters(hands))
            self.droneInputs["StatusHUDJoystickCenters"].isEnabled = True

            commandColorSchemeDay = Hands.matchesFingerPattern(detector, hands, HandPatternHUDSchemeDay)
            commandColorSchemeNight = Hands.matchesFingerPattern(detector, hands, HandPatternHUDSchemeNight)
            commandColorSchemeDayEnabled = self.droneInputs["StatusHUDColorScheme"].getValue() != Element.COLOR_DAY
            commandColorSchemeNightEnabled = self.droneInputs["StatusHUDColorScheme"].getValue() != Element.COLOR_NIGHT

            self.droneInputs["CommandHUDColorSchemeDay"].setValue(commandColorSchemeDay)
            self.droneInputs["CommandHUDColorSchemeDay"].isEnabled = commandColorSchemeDayEnabled
            self.droneInputs["CommandHUDColorSchemeNight"].setValue(commandColorSchemeNight)
            self.droneInputs["CommandHUDColorSchemeNight"].isEnabled = commandColorSchemeNightEnabled
            if commandColorSchemeDay:
                self.droneInputs["StatusHUDColorScheme"].setValue(Element.COLOR_DAY)
            elif commandColorSchemeNight:
                self.droneInputs["StatusHUDColorScheme"].setValue(Element.COLOR_NIGHT)

        # Command Switch
        commandControlTransferSwitch = Hands.matchesFingerPattern(
            detector,
            hands,
            HandPatternCommandControlTransferSwitch
        )
        self.droneInputs["CommandControlTransferSwitch"].setValue(commandControlTransferSwitch)
        if commandControlTransferSwitch:
            commandControlTransferToFar = Hands.matchesFingerPattern(
                detector,
                hands,
                HandPatternCommandControlTransferToFar
            )
            commandControlTransferToNear = Hands.matchesFingerPattern(
                detector,
                hands,
                HandPatternCommandControlTransferToNear
            )

            self.droneInputs["CommandControlTransferToFar"].setValue(commandControlTransferToFar)
            self.droneInputs["CommandControlTransferToFar"].isEnabled = commandControlTransferToNear
            self.droneInputs["CommandControlTransferToNear"].setValue(commandControlTransferToNear)
            self.droneInputs["CommandControlTransferToNear"].isEnabled = commandControlTransferToFar

        # Media
        mediaCommandSwitch = Hands.matchesFingerPattern(detector, hands, HandMediaPatternCommandSwitch)
        self.droneInputs["CommandDroneMediaSwitch"].setValue(mediaCommandSwitch)
        if mediaCommandSwitch:
            self.droneInputs["CommandDroneMediaSwitch"].isEnabled = self.droneEnabled
            writeImage = Hands.matchesFingerPattern(detector, hands, HandPatternCommandDroneTakePicture)
            self.droneInputs["CommandDroneTakePicture"].setValue(writeImage)
            self.droneInputs["CommandDroneTakePicture"].isEnabled = self.droneEnabled

            self.droneInputs["CommandDroneVideoRecording"].setValue(self.droneVideoRecording)
            self.droneInputs["CommandDroneVideoRecording"].isEnabled = self.droneEnabled and not self.droneVideoRecording
            startRecording = Hands.matchesFingerPattern(detector, hands, HandPatternCommandDroneStartRecording)
            self.droneInputs["CommandDroneStartRecording"].setValue(startRecording)
            self.droneInputs["CommandDroneStartRecording"].isEnabled = self.droneEnabled and not self.droneVideoRecording
            stopRecording = Hands.matchesFingerPattern(detector, hands, HandPatternCommandDroneStopRecording)
            self.droneInputs["CommandDroneStopRecording"].setValue(stopRecording)
            self.droneInputs["CommandDroneStopRecording"].isEnabled = self.droneEnabled and self.droneVideoRecording

        # Movement Controls
        droneControlsEnabled = self.droneEnabled and self.droneControlEnabled
        flightCommandSwitch = Hands.matchesFingerPattern(detector, hands, HandFlightPatternCommandSwitch)
        self.droneInputs["CommandDroneMovementSwitch"].setValue(flightCommandSwitch)
        if flightCommandSwitch:
            landing = Hands.matchesFingerPattern(detector, hands, HandPatternCommandDroneLand)
            self.droneInputs["CommandDroneLand"].setValue(landing)
            self.droneInputs["CommandDroneLand"].isEnabled = droneControlsEnabled and self.joystick.isJoystickEnabled()
            takeoff = Hands.matchesFingerPattern(detector, hands, HandPatternCommandDroneTakeOff)
            self.droneInputs["CommandDroneTakeoff"].setValue(takeoff)
            self.droneInputs["CommandDroneTakeoff"].isEnabled = droneControlsEnabled and self.drone.is_flying
            droneLanded = self.droneEnabled and self.drone.is_flying
            self.droneInputs["StatusDroneFlying"].setValue(droneLanded)
            self.droneInputs["StatusDroneFlying"].isEnabled = droneControlsEnabled

        # Drone Properties
        if self.droneEnabled:
            self.droneInputs["StatusDroneBattery"].setValue(self.drone.get_battery())
            self.droneInputs["StatusDroneBattery"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDroneTemperature"].setValue(self.drone.get_temperature())
            self.droneInputs["StatusDroneTemperature"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDroneHeight"].setValue(self.drone.get_ground_altitude())
            self.droneInputs["StatusDroneHeight"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDroneFlightTime"].setValue(self.drone.get_flight_time())
            self.droneInputs["StatusDroneFlightTime"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDroneBarometer"].setValue(self.drone.get_barometer_altitude())
            self.droneInputs["StatusDroneBarometer"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDronePitch"].setValue(self.drone.get_pitch())
            self.droneInputs["StatusDronePitch"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDroneRoll"].setValue(self.drone.get_roll())
            self.droneInputs["StatusDroneRoll"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDroneYaw"].setValue(self.drone.get_yaw())
            self.droneInputs["StatusDroneYaw"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDroneSpeedX"].setValue(self.drone.get_speed_x())
            self.droneInputs["StatusDroneSpeedX"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDroneSpeedY"].setValue(self.drone.get_speed_y())
            self.droneInputs["StatusDroneSpeedY"].isEnabled = self.droneEnabled

            self.droneInputs["StatusDroneSpeedZ"].setValue(self.drone.get_speed_z())
            self.droneInputs["StatusDroneSpeedZ"].isEnabled = self.droneEnabled

    def applyInputs(self, image, inputs):
        # Get the drone properties
        if self.droneEnabled:
            if self.droneInputs["StatusDroneBattery"].getValue(): self.drone.get_battery()
            if self.droneInputs["StatusDroneHeight"].getValue(): self.drone.get_ground_altitude()
            if self.droneInputs["StatusDroneFlightTime"].getValue(): self.drone.get_flight_time()
            # Capture image
            if self.droneInputs["CommandDroneTakePicture"].getValue(): cv2.imwrite(
                f'Resources/Images/{time.time()}.jpg',
                image)
            if self.droneInputs["CommandDroneStartRecording"].getValue():
                self.droneVideoRecording = True
            if self.droneInputs["CommandDroneStopRecording"].getValue():
                self.droneVideoRecording = False

        # Apply drone controls
        if self.droneEnabled and self.droneControlEnabled:
            # Take off and landing
            if self.droneInputs["CommandDroneTakeoff"].getValue(): self.drone.takeoff()
            if self.droneInputs["CommandDroneLand"].getValue(): self.drone.land()

            # Map the controls
            self.fb = inputs[0][1]
            self.yv = inputs[0][0]
            self.ud = inputs[0][1]
            self.lr = inputs[0][0]

            if self.droneControlEnabled:
                self.drone.send_rc_control(self.lr, self.fb, self.ud, self.yv)
