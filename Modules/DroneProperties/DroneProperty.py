# A drone input has an internal value and also the ability to be disabled

class DroneProperty:
    def __init__(self, value, isEnabled: bool = False, disabledValue="N/A"):
        self._internalValue = None
        self.isEnabled = isEnabled
        self._disabledValue = disabledValue
        self.setValue(value)

    def getValue(self):
        if self.isEnabled:
            return self._internalValue
        else:
            return self._disabledValue

    def getInternalValue(self):
        return self._internalValue

    def setValue(self, value):
        self._internalValue = value
