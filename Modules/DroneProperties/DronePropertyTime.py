# A drone input that returns the same value for a period of time
# It recalculates the value based of a function
import time
from Modules.DroneProperties.DroneProperty import DroneProperty

OPERATION_AVERAGE = 1


class DronePropertyTime(DroneProperty):
    _rawValue: list = []
    _rawValueEarliestTime: int = 0
    _operation: int = 0
    _operationMsTime: int = 0

    def __init__(
        self,
        value,
        isEnabled: bool = False,
        disabledValue="N/A",
        operation: int = OPERATION_AVERAGE,
        operationMsTime: int = 300
    ):
        # Initialize the value
        super(DronePropertyTime, self).__init__(value, isEnabled, disabledValue)
        self._rawValue: list = []
        self._rawValueEarliestTime: int = time.time()
        self._operation: int = operation
        self._operationMsTime: int = operationMsTime

        self.resetRawValue()

    def setValue(self, value):
        setTime = time.time()
        timeDifference = (setTime - self._rawValueEarliestTime) * 1000

        # If we pass the operation time, we compile and save the internal value, then reset the raw values
        if timeDifference > self._operationMsTime:
            if self._operation == OPERATION_AVERAGE:
                super(DronePropertyTime, self).setValue(round(sum(self._rawValue) / len(self._rawValue), 0))
                self._rawValueEarliestTime = setTime
                self.resetRawValue()

        # Finally, add the new value to _rawValue
        self._rawValue.append(value)

    def resetRawValue(self):
        self._rawValue = []
