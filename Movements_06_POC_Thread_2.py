import logging
import time

import cv2
import keyboard

from ModulesV2.Drones import DroneFactory
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image import ImageFetcherFactory
from ModulesV2.Loggers import Logger

# Declare the initial variables
# drone_ref = DroneFactory.DJI_TELLO
drone_ref = DroneFactory.STUB
computer_camera_index = 1  # -1 for a black image loader, >=0 to use a real connected camera
camera_ref = ImageFetcherFactory.FETCHER_CAMERA_COMPUTER if computer_camera_index >= 0 else ImageFetcherFactory.FETCHER_STUB_BLACK
working_image_size = Point2D(1000, 500)
logging_level = logging.DEBUG
# logging_level = main_logger.info

# Init the logger
logging.basicConfig(format=Logger.MESSAGE_FORMAT_TAB, level=logging_level, datefmt=Logger.TIME_FORMAT)
main_logger = logging.getLogger(Logger.LOGGER_MAIN)

# Defines the drone and connect to it
main_logger.info('Instance the Drone')
drone = DroneFactory.create(drone_ref)
main_logger.info('Drone instance: {}'.format(drone.__class__))
drone.connect()

# Defines the image fetchers to gather images
main_logger.info('Instance Image Fetchers')
image_fetcher_factory = ImageFetcherFactory.ImageFetcherFactory()
image_fetcher_factory.drone = drone
image_fetcher_factory.camera_index = computer_camera_index
drone_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_CAMERA_DRONE)
# hud_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_STUB_BLACK)
camera_image_fetcher = image_fetcher_factory.create(camera_ref)
# status_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_STUB_BLACK)
drone.image_fetcher = drone_image_fetcher

main_logger.info('Start Image Fetcher Threads')

main_loop_sleep_time = 0.01

main_logger.info('Start Main Loop')
while True:
    # Sets the time start of the loop
    logging.debug('Loop Start')

    logging.debug('Fetch Images')
    camera_image_fetcher.fetch_image()
    drone_image_fetcher.fetch_image()

    drone_image = drone_image_fetcher.get_image()
    camera_image = camera_image_fetcher.get_image()
    if drone_image is not None:
        cv2.imshow('Drone', drone_image)
        logging.debug('Drone Image Displayed')
    else:
        logging.debug('Drone Image Not Available')

    if camera_image is not None:
        cv2.imshow('Camera', camera_image)
        logging.debug('Camera Image Displayed')
    else:
        logging.debug('Camera Image Not Available')

    # Keyboard implementation --> Doesn't work with Mac due to security constraints
    if keyboard.read_key() == "q":
        print("You pressed q")
        break

    time.sleep(main_loop_sleep_time)

    logging.debug('Loop End')

main_logger.info('End')
