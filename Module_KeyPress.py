import pygame

def init():
    pygame.init()
    window = pygame.display.set_mode((400, 400))

def getKey(keyName):
    keyPressed = False

    for event in pygame.event.get(): pass
    keyInput = pygame.key.get_pressed()
    myKey = getattr(pygame, 'K_{}'.format(keyName))

    if keyInput[myKey]:
        keyPressed = True
    pygame.display.update()

    return keyPressed

def main():
    if getKey("LEFT"):
        print('LEFT')
    if getKey('RIGHT'):
        print('RIGHT')


if __name__ == '__main__':
    init()
    while True:
        main()