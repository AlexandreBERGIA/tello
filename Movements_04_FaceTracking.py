import time
import cv2
import numpy as np
from djitellopy import tello
import Module_KeyPress as keyPress
import numpy

longitRange = [8000, 9000]
maxSpeed = [-30, 30]
workingImageSize = [360, 240]
pid = [0.4, 0.4, 0]
pError = (0, 0)

droneEnabled = True
droneControlEnabled = True

if droneEnabled:
    drone = tello.Tello()
    drone.connect()
    drone.streamon()
    image = drone.get_frame_read().frame
    time.sleep(5)

if droneControlEnabled:
    drone.takeoff()


def findFaces(image):
    faceCascade = cv2.CascadeClassifier("Resources/haarcascade_frontalface_default.xml")
    workingImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return faceCascade.detectMultiScale(workingImage, 1.2, 8)

def drawFaces(image, faces, mainFaceIndex):
    i = 0
    # Paint the faces on the image
    for (x, y, w, h) in faces:
        # Determine the important points
        cx = x + w // 2
        cy = y + h // 2
        area = w * h

        # Draw on the image
        borderWidth = 2
        if i == mainFaceIndex: borderWidth = 4
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), borderWidth)
        cv2.circle(image, (cx, cy), 3, (0, 255, 0), borderWidth)

        i=i+1
    return image

def getMainFace(faces):
    faceCenters = []
    faceAreas = []

    for (x, y, w, h) in faces:
        # Determine the important points
        cx = x + w // 2
        cy = y + h // 2
        area = w * h

        # Add the face points to the arrays
        faceCenters.append((cx, cy))
        faceAreas.append(area)

    if len(faceCenters) != 0:
        mainFaceIndex = faceAreas.index(max(faceAreas))
        return faceCenters[mainFaceIndex], faceAreas[mainFaceIndex], mainFaceIndex
    else:
        return (0, 0), 0, 0

def trackFace(faceCenter, faceArea, imageSize, pid, pError):
    lr, fb, ud, yv = 0, 0, 0, 0
    error = (faceCenter[0] - imageSize[0] // 2, faceCenter[1] - imageSize[1] // 2)

    yv = pid[0] * error[0] + pid[1] * (error[0] - pError[0])
    yv = int(numpy.clip(yv, maxSpeed[0], maxSpeed[1]))

    ud = pid[0] * error[1] + pid[1] * (error[1] - pError[1])
    ud = int(numpy.clip(-ud, maxSpeed[0], maxSpeed[1]))

    if faceArea > longitRange[1]:
        fb = maxSpeed[0]
    elif faceArea < longitRange[0] and faceArea != 0:
        fb = maxSpeed[1]

    # Failsafe when the faceCenter is 0
    if faceCenter[0] == 0:
        lr, fb, ud, yv = 0, 0, 0, 0
        error = (0, 0)


    return [lr, fb, ud, yv], error

if not droneEnabled:
    cap = cv2.VideoCapture(0)

while True:
    if droneEnabled:
        image = drone.get_frame_read().frame
        batteryCharge = drone.get_battery()
    else:
        _, image = cap.read()
        batteryCharge = -1

    # Get the image tracking information and controls
    workingImage = cv2.resize(image, (workingImageSize[0], workingImageSize[1]))
    faces = findFaces(workingImage)
    mainFaceCenter, mainFaceArea, mainFaceIndex = getMainFace(faces)
    workingImage = drawFaces(workingImage, faces, mainFaceIndex)
    controls, pError = trackFace(mainFaceCenter, mainFaceArea, workingImageSize, pid, pError)

    if droneEnabled:
        drone.send_rc_control(controls[0], controls[1], controls[2], controls[3])

    # Display the image and control outputs
    print(batteryCharge, mainFaceCenter, mainFaceArea, controls)
    cv2.imshow("Output", workingImage)
    if cv2.waitKey(1) & 0xFF == ord('q') & droneEnabled:
        drone.land()
        break

