MIT License

Copyright (c) 2023 AlexandreBERGIA

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



Class Diagram

```plantuml
class TelloPY {}
class DroneController {
    - TelloPY drone
    + Boolean isFlying()
    + Boolean isVideoRecording()
    + Object droneGetters()
    + void droneSetters()
}


abstract HUD {
    + DroneController controller
    + {abstract} void display()
}
JoystickHUD o-- DroneController
DroneHUD o-- DroneController
ControlsHUD o-- DroneController
HUD <|-- JoystickHUD
HUD <|-- DroneHUD
HUD <|-- ControlsHUD

class JoystickHUD {
    - JoystickController controller
}
class DroneHUD {}

class ControlsHUD {}


class HUDController {
    - JoystickHUD joystick
    - DroneHUD drone
    - ControlsHUD controls
}
HUDController o-- JoystickHUD
HUDController o-- DroneHUD
HUDController o-- ControlsHUD

class Joystick {}
class JoystickController {
    - Joystick joystick
    - HandController handController
    + Object joystickGetters()
    + void joystickSetters()
}

class HandDetector {}
class HandController {
    - HandDetector detector
    + Object detectorGetters()
    + void detectorSetters()
}


class FlightRecorder {
    - DroneController controller
}
class XlsFlightRecorder {
    - FlightRecorder recorder
}
XlsFlightRecorder *-- FlightRecorder


class MainLoop {
    - DroneController droneController
    - HUDController hudController
    - XlsFlightRecorder recorder
}
MainLoop o-- DroneController
MainLoop o-- HUDController
MainLoop o-- XlsFlightRecorder


TelloPY *-- DroneController




JoystickController *-- Joystick
HandController o-- JoystickController
JoystickHUD o-- JoystickController

HandController *-- HandDetector

FlightRecorder o-- DroneController

```
