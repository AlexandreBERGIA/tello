import logging

from cvzone.HandTrackingModule import HandDetector

from ModulesV2.Converter.FingersUpToGestureConverter import FingersUpToGestureConverter
from ModulesV2.Converter.HandsToFingersUpConverter import HandsToFingersUpConverter
from ModulesV2.Converter.ImageToHandsConverter import ImageToHandsConverter
from ModulesV2.Drones import DroneFactory
from ModulesV2.Formatters.Formats.NumberRoundFormat import NumberRoundFormat
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Controllers.MainLoopHUDController import MainLoopHUDController
from ModulesV2.HUD.Controllers.StatusHUDController import StatusHUDController
from ModulesV2.HUD.Elements.Slots.HUDElementSlotFactory import HUDElementSlotFactory
from ModulesV2.HUD.Types import JoystickHUD
from ModulesV2.HUD.Types.DroneHUD import DroneHUD
from ModulesV2.HUD.Types.StatusHUD import StatusHUD
from ModulesV2.Hands.HandsController import HandsController
from ModulesV2.Hands.HandsGestureController import HandsGestureController
from ModulesV2.Image import ImageFetcherFactory
from ModulesV2.Joystick.JoystickController import JoystickController
from ModulesV2.Loggers import Logger
from ModulesV2.Mappers.CommandMappers.CommandMapperController import CommandMapperController
from ModulesV2.Mappers.CommandMappers.DroneLand import DroneLand
from ModulesV2.Mappers.CommandMappers.DroneLowBattery import DroneLowBattery
from ModulesV2.Mappers.CommandMappers.DroneMotorDisable import DroneMotorDisable
from ModulesV2.Mappers.CommandMappers.DroneMotorEnable import DroneMotorEnable
from ModulesV2.Mappers.CommandMappers.DroneTakeoff import DroneTakeoff
from ModulesV2.Mappers.CommandMappers.JoystickHide import JoystickHide
from ModulesV2.Mappers.CommandMappers.JoystickSetInputs import JoystickSetInputs
from ModulesV2.Mappers.CommandMappers.JoystickShow import JoystickShow
from ModulesV2.Mappers.CommandMappers.KeyboardMapperController import KeyboardMapperController
from ModulesV2.Mappers.CommandMappers.KeyboardMappers import AbstractKeyboardMapper
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneLandTakeoff import DroneLandTakeoff
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneMotorEnableDisable import DroneMotorEnableDisable
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneMoveBackward import DroneMoveBackward
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneMoveDown import DroneMoveDown
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneMoveForward import DroneMoveForward
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneMoveLeft import DroneMoveLeft
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneMoveRight import DroneMoveRight
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneMoveUp import DroneMoveUp
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneRotateLeft import DroneRotateLeft
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.DroneRotateRight import DroneRotateRight
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.ExitProgram import ExitProgram
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.HandsDetectOnOff import HandsDetectOnOff
from ModulesV2.Mappers.HandsToDualJoystickMapper import HandsToDualJoystickMapper
from ModulesV2.Time.Chronometer import Chronometer

# Declare the initial variables
# drone_ref = DroneFactory.DJI_TELLO
drone_ref = DroneFactory.STUB
computer_camera_index = 1  # -1 for a black image loader, >=0 to use a real connected camera
camera_ref = ImageFetcherFactory.FETCHER_CAMERA_COMPUTER if computer_camera_index >= 0 else ImageFetcherFactory.FETCHER_STUB_BLACK
working_image_size = Point2D(1000, 800)

# Declare the different loggers
main_logger = logging.getLogger(Logger.LOGGER_MAIN)
main_logger_formatter = logging.Formatter(Logger.MESSAGE_FORMAT_TAB)
main_logger_formatter.datefmt = Logger.TIME_FORMAT
main_logger_handler = logging.StreamHandler()
main_logger_handler.setFormatter(main_logger_formatter)
main_logger.addHandler(main_logger_handler)
main_logger.setLevel(logging.DEBUG)
main_logger.propagate = False
#
statistics_logger = logging.getLogger(Logger.LOGGER_STATISTICS)
statistics_logger_formatter = logging.Formatter(Logger.MESSAGE_FORMAT_STATISTICS)
statistics_logger_formatter.datefmt = Logger.TIME_FORMAT
statistics_logger_handler = logging.StreamHandler()
statistics_logger_handler.setFormatter(statistics_logger_formatter)
statistics_logger.addHandler(statistics_logger_handler)
statistics_logger.setLevel(logging.DEBUG)
statistics_logger.propagate = False

main_logger.info('Init,Started')

# Defines the drone and connect to it
drone = DroneFactory.create(drone_ref)
drone.connect()
main_logger.info('Init,Drone Instanced')

# Defines the image fetchers to gather images
image_fetcher_factory = ImageFetcherFactory.ImageFetcherFactory()
image_fetcher_factory.drone = drone
image_fetcher_factory.camera_index = computer_camera_index
drone_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_CAMERA_DRONE)
hud_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_STUB_BLACK)
camera_image_fetcher = image_fetcher_factory.create(camera_ref)
status_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_STUB_BLACK)
drone.image_fetcher = drone_image_fetcher
main_logger.info('Init,Image Fetchers Instanced')

# Defines the converter that will extract the hands information form an image
hand_detector = HandDetector(detectionCon=0.8, maxHands=2)
fingers_up_to_gesture_converter = FingersUpToGestureConverter()
hands_gesture_controller = HandsGestureController(fingers_up_to_gesture_converter)
hands_to_fingers_converter = HandsToFingersUpConverter(hand_detector)
hands_controller = HandsController(hands_to_fingers_converter, hands_gesture_controller)
image_to_hands_converter = ImageToHandsConverter(camera_image_fetcher, hand_detector)
main_logger.info('Init,Hands Controller and Converters Instanced')

left_joystick_controller = JoystickController()
right_joystick_controller = JoystickController()
main_logger.info('Init,Joystick Controllers Instanced')

# Defines the HUD instances
main_loop_chronometer = Chronometer()
format_list = {
    JoystickHUD.FORMATTER_VALUE_PERCENT: [NumberRoundFormat(0)]
}
left_joystick_hud = JoystickHUD.JoystickHUD(camera_image_fetcher, left_joystick_controller, format_list)
right_joystick_hud = JoystickHUD.JoystickHUD(camera_image_fetcher, right_joystick_controller, format_list)
drone_video_hud = DroneHUD(drone_image_fetcher, drone)
element_factory = HUDElementSlotFactory(status_image_fetcher, drone, hands_gesture_controller,
                                        main_loop_chronometer)
status_controller = StatusHUDController(element_factory)
status_hud = StatusHUD(status_image_fetcher, status_controller)
main_logger.info('Init,HUDs Instanced')

# Defines the joystick controllers and their converter
hand_to_dual_joystick_mapper = HandsToDualJoystickMapper(
    hands_controller,
    left_joystick_hud,
    right_joystick_hud,
    left_joystick_controller,
    right_joystick_controller
)
exit_program_command = ExitProgram(AbstractKeyboardMapper.KEYBOARD_Q, drone)
keyboard_mapper_list = [
    exit_program_command,
    DroneLandTakeoff(AbstractKeyboardMapper.KEYBOARD_SPACE, drone),
    DroneMoveForward(AbstractKeyboardMapper.KEYBOARD_W, drone),
    DroneMoveBackward(AbstractKeyboardMapper.KEYBOARD_S, drone),
    DroneRotateRight(AbstractKeyboardMapper.KEYBOARD_D, drone),
    DroneRotateLeft(AbstractKeyboardMapper.KEYBOARD_A, drone),
    DroneMoveUp(AbstractKeyboardMapper.KEYBOARD_ARROW_UP, drone),
    DroneMoveDown(AbstractKeyboardMapper.KEYBOARD_ARROW_DOWN, drone),
    DroneMoveRight(AbstractKeyboardMapper.KEYBOARD_ARROW_RIGHT, drone),
    DroneMoveLeft(AbstractKeyboardMapper.KEYBOARD_ARROW_LEFT, drone),
    HandsDetectOnOff(AbstractKeyboardMapper.KEYBOARD_1, image_to_hands_converter),
    DroneMotorEnableDisable(AbstractKeyboardMapper.KEYBOARD_2, drone),

]
keyboard_mapper_controller = KeyboardMapperController(keyboard_mapper_list)
mapper_list = [
    # Normal mappings go first
    JoystickShow('preflight', 'joystick_show', hand_to_dual_joystick_mapper, hands_controller),
    DroneMotorEnable('preflight', 'drone_motor_enable', hands_controller, drone),
    DroneTakeoff('preflight', 'drone_takeoff', hands_controller, drone),
    JoystickShow('joystick', 'joystick_show', hand_to_dual_joystick_mapper, hands_controller),
    JoystickHide('joystick', 'joystick_hide', hand_to_dual_joystick_mapper, hands_controller),
    JoystickSetInputs(hand_to_dual_joystick_mapper, drone),
    DroneTakeoff('drone', 'drone_takeoff', hands_controller, drone),
    DroneLand('drone', 'drone_land', hands_controller, drone),
    DroneMotorEnable('drone', 'drone_motor_enable', hands_controller, drone),
    DroneMotorDisable('drone', 'drone_motor_disable', hands_controller, drone),

    # Keyboard mappings go after the normal one to prevent overrides
    keyboard_mapper_controller,

    # Emergency mappings go last to prevent any override
    DroneLowBattery(drone),
]
mappers_controller = CommandMapperController(mapper_list)
main_logger.info('Init,Mappers Instanced')

main_loop_hud_controller = MainLoopHUDController(
    [left_joystick_hud, right_joystick_hud, drone_video_hud, status_hud],
    hand_to_dual_joystick_mapper,
    image_fetcher_factory.create(ImageFetcherFactory.FETCHER_STUB_LOADING)
)
main_logger.info('Init,Main HUD Instanced')

program_chronometer = Chronometer()
program_chronometer.start()
main_logger.info('Init,Main Loop Started')
while True:
    # Sets the time start of the loop
    main_loop_chronometer.start()

    # Capture the drone data over time
    drone.capture_data(program_chronometer)

    main_loop_hud_controller.fetch_images()

    # Get the hands, finger up and gesture information
    hands_controller.set_hands(image_to_hands_converter.convert())

    # Convert the gesture information in input commands and set them to the commands controller
    mappers_controller.apply_inputs()

    # Display the HUDs
    main_loop_hud_controller.display()

    if exit_program_command.is_apply_allowed():
        break

    # Sets the end time of the loop
    main_loop_chronometer.save_elapsed_time()
    main_loop_chronometer.stop()

main_logger.info('End,Loop Ended')
main_loop_hud_controller.terminate()
drone.end()
program_chronometer.stop()

main_logger.info('End,Terminated')
