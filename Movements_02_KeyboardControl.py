from djitellopy import tello
from time import sleep
import Module_KeyPress as keyPress

keyPress.init()
drone = tello.Tello()
drone.connect()

print(drone.get_battery())

def getKeyboardInput():
    lr, fb, ud, yv = 0, 0, 0, 0
    to, ld = False, False
    speed = 50

    if(keyPress.getKey("LEFT")): lr = -speed
    elif (keyPress.getKey("RIGHT")): lr = speed

    if (keyPress.getKey("UP")): fb = speed
    elif (keyPress.getKey("DOWN")): fb = -speed

    if (keyPress.getKey("w")): ud = speed
    elif (keyPress.getKey("s")): ud = -speed

    if (keyPress.getKey("a")): yv = -speed
    elif (keyPress.getKey("d")): yv = speed

    if (keyPress.getKey("q")): ld = True
    if (keyPress.getKey("e")): to = True

    return [lr, fb, ud, yv, ld, to]

while True:
    # Get the keyboard inputs
    keyboardInput = getKeyboardInput()
    print(drone.get_battery(), keyboardInput)

    # Uses the keyboad controls
    drone.send_rc_control(keyboardInput[0], keyboardInput[1], keyboardInput[2], keyboardInput[3])
    if keyboardInput[4]: drone.land()
    if keyboardInput[5]: drone.takeoff()

    # Limit the loops per second
    sleep(0.05)
