from djitellopy import tello
from time import sleep

drone = tello.Tello()
drone.connect()

drone.takeoff()
sleep(2)
drone.move_forward(4)
sleep(2)
drone.move_back(4)
sleep(2)
drone.land()