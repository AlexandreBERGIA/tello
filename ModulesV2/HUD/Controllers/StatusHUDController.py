import time

from ModulesV2.Drones import AbstractDrone
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Graphs.Graph import Graph
from ModulesV2.HUD.Elements.Graphs.GraphValues.DroneGraphValues import DroneGraphValues
from ModulesV2.HUD.Elements.Slots.AbstractHUDElementSlot import AbstractHUDElementSlot
from ModulesV2.HUD.Elements.Slots.HUDElementSlotFactory import HUDElementSlotFactory


class StatusHUDController:
    def __init__(self, element_factory: HUDElementSlotFactory):
        self._element_factory = element_factory

        self._elements = []
        self._element_index = 0
        self._loop_start_time = time.time()
        self._loop_time_ms = 0

        self._init_elements()

    def _init_elements(self) -> None:
        """
        Initialize all the elements
        """

        # First Column
        elements = [
            # Add the loops time
            self._element_factory.create('loop.loop_time'),
            # Add the Drone Properties
            self._element_factory.create('drone.get_battery'),
            self._element_factory.create('drone.get_temperature'),

            # Add flight information
            self._element_factory.create('generic.spacer'),
            self._element_factory.create('drone.get_flight_time'),
            self._element_factory.create('drone.get_is_motor_enabled'),
            self._element_factory.create('drone.get_is_flying'),
        ]
        main_element_container = self._element_factory.create('generic.container')
        main_element_container.set_elements(elements)
        main_element_container.set_rendering_padding(Point2D(-0.9, 0.9))
        self._elements.append(main_element_container)

        # Second Column
        elements = [
            # Adds the gestures
            self._element_factory.create('gesture.preflight'),
            self._element_factory.create('gesture.joystick'),
            self._element_factory.create('gesture.drone'),
        ]
        gestures_element_container = self._element_factory.create('generic.container')
        gestures_element_container.set_elements(elements)
        gestures_element_container.set_rendering_padding(Point2D(0, 0.9))
        self._elements.append(gestures_element_container)

        graph_values = DroneGraphValues(self._element_factory.drone, AbstractDrone.CAPTURED_DATA_GROUND_ALTITUDE)
        graph_values.range_max_x = 20000
        graph_values.range_max_y = self._element_factory.drone.get_max_ground_altitude()
        height_graph = Graph(self._element_factory.image_fetcher, Point2D(-1, -1), graph_values)
        height_graph.size = Point2D(2, 0.3)
        height_graph.step_size_x = 5000  # Draw a step every 5 seconds
        height_graph.step_size_y = 5  # Draw a step every 5 meters
        self._elements.append(height_graph)

    def get_elements(self) -> [AbstractHUDElementSlot]:
        """
        Returns the list of elements to be rendered
        """
        return self._elements
