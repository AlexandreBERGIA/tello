from abc import abstractmethod

from ModulesV2.HUD.AbstractHUD import AbstractHUD


class AbstractHUDController:
    """
    This controller is in charge of managing all the HUDs and displaying them
    """

    def __init__(
            self,
            hud_list: [AbstractHUD]
    ):
        self._hud_list = hud_list

    def _paint(self):
        """
        Paint all the HUDs
        """
        for hud in self._hud_list:
            hud.paint()

    @abstractmethod
    def _compose(self) -> None:
        """
        Compose the final image based on all the HUD images
        """
        pass

    def display(self) -> None:
        """
        The public call that paints all HUDs and compose the final image to display
        """
        self._paint()
        self._compose()

    @abstractmethod
    def fetch_inputs(self) -> None:
        """
        Fetches the input values
        """
        pass

    @abstractmethod
    def terminate(self) -> None:
        """
        Terminate the HUD Controller and all its components
        """
        pass
