from abc import ABC

import cv2
import numpy

from ModulesV2.HUD.AbstractHUD import AbstractHUD
from ModulesV2.HUD.Controllers.AbstractHUDController import AbstractHUDController
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher
from ModulesV2.Loggers import Logger
from ModulesV2.Mappers.HandsToDualJoystickMapper import HandsToDualJoystickMapper


class MainLoopHUDController(AbstractHUDController, ABC):
    """
    The HUD Controller that displays all the required HUD types
    """

    def __init__(
            self,
            hud_list: [AbstractHUD],
            converter: HandsToDualJoystickMapper,
            loading_image_fetcher: AbstractImageFetcher
    ):
        super(MainLoopHUDController, self).__init__(hud_list)
        self._converter = converter

        self._hud_window_name = "Drone Hud"
        self._loading_image_fetcher = loading_image_fetcher
        self._loading_image_fetcher.fetch_image()

    def _compose(self):
        """
        Composes the final image with
        """
        image_list = []
        for hud in self._hud_list:
            image = hud.get_image_fetcher().get_image()

            # If the image is missing, it is replaced by a placeholder image, assuming it is available
            if image is None:
                image = self._loading_image_fetcher.get_image()

            # At that point, if we are missing one image, we skip the composition
            if image is None:
                fetcher_name = type(hud.get_image_fetcher()).__name__
                Logger.main_logger.warn('Display skipped, Empty Image from fetcher: {}'.format(fetcher_name))
                return

            image_list.append(image)
        working_image = numpy.concatenate([image_list[1], image_list[2], image_list[3]], 1)
        cv2.imshow(self._hud_window_name, working_image)

    def fetch_images(self) -> None:
        """
        Starts the image capture in all the declared image fetchers
        """
        for hud in self._hud_list:
            hud.get_image_fetcher().fetch_image()

    def terminate(self) -> None:
        """
        Destroy the HUD windows and also terminate the image fetchers
        """
        cv2.destroyWindow(self._hud_window_name)
