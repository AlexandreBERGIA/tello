from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Formatters.Formats.NumberRoundFormat import NumberRoundFormat
from ModulesV2.Formatters.Formats.TextRJust import TextRJust
from ModulesV2.HUD.AbstractHUD import AbstractHUD
from ModulesV2.HUD.Elements.AltimeterHUDElement import AltimeterHUDElement
from ModulesV2.HUD.Elements.AltimeterScaleHUDElement import AltimeterScaleHUDElement
from ModulesV2.HUD.Elements.ArtificialHorizonHUDElement import ArtificialHorizonHUDElement
from ModulesV2.HUD.Elements.CompassHUDElement import CompassHUDElement
from ModulesV2.HUD.Elements.GridHUDElement import GridHUDElement
from ModulesV2.HUD.Elements.HorizontalSpeedHUDElement import HorizontalSpeedHUDElement
from ModulesV2.HUD.Elements.RollHUDElement import RollHUDElement
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class DroneHUD(AbstractHUD, ABC):
    """
    Renders the drone HUD with the image coming from the drone and add all the HUD data on top of it
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, drone: AbstractDrone):
        super(DroneHUD, self).__init__(image_fetcher)

        # Format the values
        altimeter_format = [NumberRoundFormat(0), TextRJust(5, ' ')]
        horizontal_speed_format = [NumberRoundFormat(0), TextRJust(5, ' ')]
        current_heading_format = [NumberRoundFormat(0), TextRJust(3, '0')]
        compass_heading_format = [NumberRoundFormat(0), TextRJust(2, '0')]
        horizon_format = {
            ArtificialHorizonHUDElement.FORMAT_RIGHT_PITCH_TEXT: [],
            ArtificialHorizonHUDElement.FORMAT_LEFT_PITCH_TEXT: [TextRJust(3, ' ')],
        }

        self._elements = [
            GridHUDElement(image_fetcher),
            AltimeterHUDElement(image_fetcher, drone, altimeter_format),
            AltimeterScaleHUDElement(image_fetcher, drone),
            RollHUDElement(image_fetcher, drone),
            ArtificialHorizonHUDElement(image_fetcher, drone, horizon_format),
            HorizontalSpeedHUDElement(image_fetcher, drone, horizontal_speed_format),
            CompassHUDElement(image_fetcher, drone, current_heading_format, compass_heading_format)
        ]

    def _paint(self) -> None:
        for element in self._elements:
            element.paint()
