from abc import ABC

from ModulesV2.HUD.AbstractHUD import AbstractHUD
from ModulesV2.HUD.Controllers.StatusHUDController import StatusHUDController
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class StatusHUD(AbstractHUD, ABC):
    """
    Renders the drone HUD with the image coming from the drone and add all the HUD data on top of it
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, status_controller: StatusHUDController):
        super(StatusHUD, self).__init__(image_fetcher)
        self._status_controller = status_controller

    def _paint(self) -> None:
        for element in self._status_controller.get_elements():
            element.paint()
