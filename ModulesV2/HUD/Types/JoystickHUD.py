from abc import ABC
from typing import Dict, List

import cv2

from ModulesV2.Formatters import Formatter
from ModulesV2.Formatters.Formats.AbstractFormat import AbstractFormat
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.AbstractHUD import AbstractHUD
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher
from ModulesV2.Joystick.JoystickController import JoystickController

joystick_min_thickness = 4
joystick_min_radius = 35
joystick_max_thickness = 4
joystick_max_radius = 170
joystick_pointer_radius = 4
joystick_pointer_thickness = 6
joystick_percent_value_font = cv2.FONT_HERSHEY_SIMPLEX
joystick_percent_value_size = 0.7
joystick_percent_value_thickness = 4
joystick_percent_value_size_text_coeff = 3

FORMATTER_VALUE_PERCENT = 'value_percent'


class JoystickHUD(AbstractHUD, ABC):
    """
    Renders one joystick
    """

    def __init__(
            self,
            image_fetcher: AbstractImageFetcher,
            joystick_controller: JoystickController,
            formatter_list: Dict[str, List[AbstractFormat]]
    ):
        super(JoystickHUD, self).__init__(image_fetcher)

        self._joystick_controller = joystick_controller
        self._formatter_list: Dict[str, List[AbstractFormat]] = formatter_list
        self._center = Point2D(0, 0)

        self.joystick_min_thickness = joystick_min_thickness
        self.joystick_min_radius = joystick_min_radius
        self.joystick_max_thickness = joystick_max_thickness
        self.joystick_max_radius = joystick_max_radius
        self.joystick_pointer_radius = joystick_pointer_radius
        self.joystick_pointer_thickness = joystick_pointer_thickness
        self.joystick_percent_value_font = cv2.FONT_HERSHEY_SIMPLEX
        self.joystick_percent_value_size = joystick_percent_value_size
        self.joystick_percent_value_thickness = joystick_percent_value_thickness

        self._is_visible = False

    def _paint(self) -> None:
        self.display_min_max_delimitations()
        self.display_input()
        self.display_percent_values()

    def display_min_max_delimitations(self) -> None:
        """
        Display two circles that represent the Min and Max possible locations for the joystick
        """
        # Min Radius
        cv2.circle(
            self._image_fetcher.get_image(),
            self._center.point_int,
            int(self.joystick_min_radius),
            self.get_display_color(),
            int(self.joystick_min_thickness)
        )
        # Draw the Max Radius
        cv2.circle(
            self._image_fetcher.get_image(),
            self._center.point_int,
            int(self.joystick_max_radius),
            self.get_display_color(),
            int(self.joystick_max_thickness)
        )

    def display_input(self) -> None:
        """
        Display the position of the joystick input based on the joystick value
        """
        x = self._center.x + self._joystick_controller.get_joystick_xy().x * self.joystick_max_radius / 100
        y = self._center.y - self._joystick_controller.get_joystick_xy().y * self.joystick_max_radius / 100
        point = Point2D(x, y)

        cv2.circle(
            self._image_fetcher.get_image(),
            point.point_int,
            int(self.joystick_pointer_radius),
            self.get_display_color(),
            int(self.joystick_pointer_thickness)
        )

    def display_percent_values(self) -> None:
        """
        Display the joystick values in percent outside the max radius
        """
        coordinates = Point2D(self._center.x + self.joystick_max_radius + 5, self._center.y)
        value = Formatter.format_value(self._joystick_controller.get_joystick_xy().x,
                                       self._formatter_list[FORMATTER_VALUE_PERCENT])
        cv2.putText(
            self._image_fetcher.get_image(),
            value + "%",
            coordinates.point_int,
            self.joystick_percent_value_font,
            self.joystick_percent_value_size * joystick_percent_value_size_text_coeff,
            self.get_display_color(),
            int(self.joystick_percent_value_thickness)
        )

        coordinates = Point2D(self._center.x, self._center.y - self.joystick_max_radius - 5)
        value = Formatter.format_value(self._joystick_controller.get_joystick_xy().y,
                                       self._formatter_list[FORMATTER_VALUE_PERCENT])
        cv2.putText(
            self._image_fetcher.get_image(),
            value + "%",
            coordinates.point_int,
            self.joystick_percent_value_font,
            self.joystick_percent_value_size * joystick_percent_value_size_text_coeff,
            self.get_display_color(),
            int(self.joystick_percent_value_thickness)
        )

    def get_center(self) -> Point2D:
        """
        Returns the joystick HUD center
        """
        return self._center

    def set_center(self, center: Point2D) -> None:
        """
        Sets the center point of the Joystick HUD from where all will be drawn
        """
        self._center = center

    def set_outer_radius_size(self, size: float) -> None:
        """
        Sets the size of the outer radius and determines the size of all the other elements relative to it
        """
        size_factor = size / joystick_max_radius
        self.joystick_max_radius = size
        self.joystick_min_thickness = max(int(joystick_min_thickness * size_factor), 1)
        self.joystick_min_radius = joystick_min_radius * size_factor
        self.joystick_max_thickness = max(joystick_max_thickness * size_factor, 1)
        self.joystick_pointer_radius = joystick_pointer_radius * size_factor
        self.joystick_pointer_thickness = max(joystick_pointer_thickness * size_factor, 1)
        self.joystick_percent_value_size = joystick_percent_value_size * size_factor
        self.joystick_percent_value_thickness = max(joystick_percent_value_thickness * size_factor, 1)
