from abc import ABC

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Slots.ContainerHUDElementSlot import ContainerHUDElementSlot
from ModulesV2.HUD.Elements.Slots.HandPatternHUDElementSlot import HandPatternHUDElementSlot
from ModulesV2.Hands.HandGesture import HandGesture
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class HandGestureHUDElementSlot(ContainerHUDElementSlot, ABC):
    """
    Renders a HandGesture in the HUD
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D, label: str,
                 gesture: HandGesture):
        super().__init__(image_fetcher, screen_position)

        self._elements.append(HandPatternHUDElementSlot(
            image_fetcher,
            screen_position,
            label,
            gesture.get_left_hand_pattern()
        ))

        for right_hand_label, hand_pattern in gesture.get_right_hand_pattern_list().items():
            self._elements.append(HandPatternHUDElementSlot(
                image_fetcher,
                screen_position,
                right_hand_label,
                hand_pattern
            ))

    def _preprocess_paint(self) -> None:
        """
        All the elements after the index [0] should be hidden if the first HandPattern doesn't match
        """
        super()._preprocess_paint()
        right_hand_rendering_padding = Point2D(0.1, 0)

        element_index = 0
        is_visible = False
        for element in self._elements:
            # Get the match value of the first element to determine whether the other elements are visible or not
            if element_index == 0:
                is_visible = element.get_hand_pattern().is_match()
            else:
                element.set_is_visible(is_visible)
                element.screen_position = element.screen_position + right_hand_rendering_padding

            element_index += 1

    def get_slot_height(self) -> int:
        """
        Returns 1 + the quantity HandPatterns when the left hand matches, else 1
        """
        if self._elements[0].get_hand_pattern().is_match():
            return len(self._elements)
        else:
            return 1
