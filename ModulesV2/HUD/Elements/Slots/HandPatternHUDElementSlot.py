from abc import ABC

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Slots.LabelHUDElementSlot import LabelHUDElementSlot
from ModulesV2.Hands.HandPattern import HandPattern
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class HandPatternHUDElementSlot(LabelHUDElementSlot, ABC):
    def __init__(
            self,
            image_fetcher: AbstractImageFetcher,
            screen_position: Point2D,
            label: str,
            hand_pattern: HandPattern
    ):
        super().__init__(image_fetcher, screen_position, label)
        self._hand_pattern = hand_pattern

    def get_finger_gesture(self) -> str:
        """
        Returns the finger pattern for the gesture
        """
        label_pattern = []
        for finger_pattern in self._hand_pattern.get_fingers_pattern():
            if finger_pattern == -1:
                label_pattern.append('*')
            elif finger_pattern == 0:
                label_pattern.append('-')
            elif finger_pattern == 1:
                label_pattern.append('|')
            else:
                raise ValueError("The finger patter value is not supported: {}".format(finger_pattern))

        return ''.join(label_pattern)

    def get_hand_pattern(self) -> HandPattern:
        """
        Returns the HandPattern used in that element
        """
        return self._hand_pattern

    def get_label(self) -> str:
        """
        Overrides the parent method to also display the gesture information
        """
        is_match_string = 'T' if self._hand_pattern.is_match() else 'F'
        return '{0}: {1} ({2})'.format(self._label, self.get_finger_gesture(), is_match_string)
