from abc import ABC

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Slots.AbstractHUDElementSlot import AbstractHUDElementSlot
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class SpacerHUDElementSlot(AbstractHUDElementSlot, ABC):
    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D, slot_height: int = 1):
        super().__init__(image_fetcher, screen_position)
        self._slot_height = slot_height
