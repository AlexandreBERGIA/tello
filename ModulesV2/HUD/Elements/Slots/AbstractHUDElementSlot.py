from abc import ABC

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.AbstractHUDElement import AbstractHUDElement
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class AbstractHUDElementSlot(AbstractHUDElement, ABC):
    """
    This is a basic HUD element that can be rendered on an image based on a point
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D):
        super().__init__(image_fetcher, screen_position)

        self._slot_height = 1  # This helps the rendering process by declaring how high is the element

    def get_slot_height(self) -> int:
        """
        Returns how high is the element. 1 is the default high. It can be greater when there are additional elements
        also rendered
        """
        return self._slot_height
