from abc import ABC

from ModulesV2.Formatters import Formatter
from ModulesV2.Formatters.Formats.AbstractFormat import AbstractFormat
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Slots.MathAverageHUDElementSlot import MathAverageHUDElementSlot
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher
from ModulesV2.Time.Chronometer import Chronometer


class LoopAverageTimeHUDElementSlot(MathAverageHUDElementSlot, ABC):
    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D, label: str,
                 values_stack_size: int, main_loop_chronometer: Chronometer, suffix: str = '',
                 format_list: [AbstractFormat] = []):
        super().__init__(image_fetcher, screen_position, label, values_stack_size, suffix)
        self._main_loop_chronometer = main_loop_chronometer
        self._format_list = format_list

    def assign_value(self) -> None:
        """
        Assign a new value to the stack
        """
        self.add_stack_value(self._main_loop_chronometer.get_saved_elapsed_time())

    def get_value(self) -> str:
        """
        Assign the new value and get the updated label
        """
        self.assign_value()
        value = super().get_value()
        return Formatter.format_value(value, self._format_list)
