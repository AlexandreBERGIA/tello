from abc import ABC

import cv2

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Slots.AbstractHUDElementSlot import AbstractHUDElementSlot
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class LabelHUDElementSlot(AbstractHUDElementSlot, ABC):
    """
    Renders a label based on a rendering point and a text
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D, label: str):
        super().__init__(image_fetcher, screen_position)
        self._label = label

    def _paint(self) -> None:
        self._paint_text()

    def _paint_text(self) -> None:
        # Draw
        cv2.putText(self.get_image_fetcher().get_image(), self.get_label(),
                    self.screenspace_pos_to_px(self._screen_position), self.TEXT_FONT_DEFAULT, self.TEXT_SIZE_DEFAULT,
                    self.get_display_color(), self.TEXT_THICKNESS_DEFAULT)

    def get_label(self) -> str:
        """
        Returns the label of the element
        """
        return self._label
