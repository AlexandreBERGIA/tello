from abc import ABC

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Slots.LabelHUDElementSlot import LabelHUDElementSlot
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class MathAverageHUDElementSlot(LabelHUDElementSlot, ABC):
    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D, label: str,
                 values_stack_size: int,
                 suffix: str = ''):
        super().__init__(image_fetcher, screen_position, label)
        self._values_stack_size = values_stack_size
        self._suffix = suffix

        self._values = []

    def add_stack_value(self, value: float) -> None:
        """
        Adds the provided value and ensure the value stack remains under the stack size by removing the first entry
        """
        self._values.append(value)
        if len(self._values) > self._values_stack_size:
            self._values.pop(0)

    def get_label(self) -> str:
        """
        Overrides the parent method to get a specific property
        """
        return self._label + ': ' + str(self.get_value()) + self._suffix

    def get_value(self) -> float:
        """
        Returns the average value from the value stack
        """
        return sum(self._values) / len(self._values)
