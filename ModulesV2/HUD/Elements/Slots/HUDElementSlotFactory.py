from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Formatters.Formats.NumberRoundFormat import NumberRoundFormat
from ModulesV2.Formatters.Formats.TextRJust import TextRJust
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Slots.AbstractHUDElementSlot import AbstractHUDElementSlot
from ModulesV2.HUD.Elements.Slots.ContainerHUDElementSlot import ContainerHUDElementSlot
from ModulesV2.HUD.Elements.Slots.DronePropertyHUDElementSlot import DronePropertyHUDElementSlot
from ModulesV2.HUD.Elements.Slots.HandGestureHUDElementSlot import HandGestureHUDElementSlot
from ModulesV2.HUD.Elements.Slots.LoopAverageTimeHUDElementSlot import LoopAverageTimeHUDElementSlot
from ModulesV2.HUD.Elements.Slots.SpacerHUDElementSlot import SpacerHUDElementSlot
from ModulesV2.Hands.HandGesture import HandGesture
from ModulesV2.Hands.HandsGestureController import HandsGestureController
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher
from ModulesV2.Time.Chronometer import Chronometer


class HUDElementSlotFactory:
    """
    Generates DroneProperty objects
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, drone: AbstractDrone,
                 gesture_controller: HandsGestureController, main_loop_chronometer: Chronometer):
        self.image_fetcher = image_fetcher
        self._drone = drone
        self._gesture_controller = gesture_controller
        self._main_loop_chronometer = main_loop_chronometer

    def create(self, element_name: str) -> AbstractHUDElementSlot:
        """
        Creates and return a new DroneProperty based on the provided property name
        """
        default_coordinates = Point2D(0, 0)

        # Creates a Container for ElementSlot elements
        if element_name == 'generic.container':
            return ContainerHUDElementSlot(self.image_fetcher, default_coordinates)

        # Creates an empty / spacer element
        elif element_name == 'generic.spacer':
            return SpacerHUDElementSlot(self.image_fetcher, default_coordinates, 1)

        # Creates the DroneProperty elements
        elif element_name == 'drone.get_is_motor_enabled':
            return DronePropertyHUDElementSlot(self.image_fetcher, default_coordinates, self._drone,
                                               'Motor Enabled', 'get_is_motor_enabled')
        elif element_name == 'drone.get_ground_altitude':
            formatter_list = [NumberRoundFormat(0), TextRJust(5, ' ')]
            return DronePropertyHUDElementSlot(self.image_fetcher, default_coordinates, self._drone,
                                               'Ground Altitude', 'get_ground_altitude', 'm', formatter_list)
        elif element_name == 'drone.get_battery':
            formatter_list = [NumberRoundFormat(0), TextRJust(3, ' ')]
            return DronePropertyHUDElementSlot(self.image_fetcher, default_coordinates, self._drone,
                                               'Battery', 'get_battery', '%', formatter_list)
        elif element_name == 'drone.get_is_flying':
            return DronePropertyHUDElementSlot(self.image_fetcher, default_coordinates, self._drone,
                                               'Is Flying', 'get_is_flying')
        elif element_name == 'drone.get_flight_time':
            formatter_list = [NumberRoundFormat(1)]
            return DronePropertyHUDElementSlot(self.image_fetcher, default_coordinates, self._drone,
                                               'Flight Time', 'get_flight_time', 's', formatter_list)
        elif element_name == 'drone.get_temperature':
            return DronePropertyHUDElementSlot(self.image_fetcher, default_coordinates, self._drone,
                                               'Temperature', 'get_temperature', 'C')

        # Creates the HandGesture
        elif element_name == 'gesture.joystick':
            return HandGestureHUDElementSlot(self.image_fetcher, default_coordinates, 'Joystick',
                                             self.get_gesture('joystick'))
        
        elif element_name == 'gesture.drone':
            return HandGestureHUDElementSlot(self.image_fetcher, default_coordinates, 'Drone',
                                             self.get_gesture('drone'))
        elif element_name == 'gesture.preflight':
            return HandGestureHUDElementSlot(self.image_fetcher, default_coordinates, 'Preflight',
                                             self.get_gesture('preflight'))

        # Creates the loop elements
        elif element_name == 'loop.loop_time':
            formatter_list = [NumberRoundFormat(0), TextRJust(5, ' ')]
            return LoopAverageTimeHUDElementSlot(self.image_fetcher, default_coordinates, 'Loop Time', 4,
                                                 self._main_loop_chronometer, 'ms', formatter_list)

        # If not element can be created, raises an exception
        else:
            raise ValueError("The provided property is not supported: {}".format(element_name))

    def get_gesture(self, element_name) -> HandGesture:
        """
        Returns the gesture based on the provided element name
        """
        if element_name not in self._gesture_controller.get_gestures():
            raise ValueError("The provided Gesture name does not exist: {}".format(element_name))

        return self._gesture_controller.get_gestures()[element_name]

    @property
    def drone(self):
        return self._drone
