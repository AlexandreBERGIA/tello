from abc import ABC

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Slots.AbstractHUDElementSlot import AbstractHUDElementSlot
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class ContainerHUDElementSlot(AbstractHUDElementSlot, ABC):
    """
    This class allows rendering multiple HUDElement and other ContainerHUDElement providing the rendering coordinates
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D):
        super().__init__(image_fetcher, screen_position)

        self._elements = []

        self._rendering_padding = Point2D(0, 0)
        self._slot_height_coeff = 0.09

    def _paint(self) -> None:
        """
        Paints all the contained elements
        """
        for element in self._elements:
            element.paint()

    def _preprocess_paint(self) -> None:
        """
        Determines the rendering coordinates of each element based on their slot index
        """
        element_slot_index = 0
        for element in self._elements:
            self._set_element_rendering_coordinates(element, element_slot_index)
            element_slot_index += element.get_slot_height()

    def _set_element_rendering_coordinates(self, element: AbstractHUDElementSlot, element_slot_index: int) -> None:
        """
        Sets the rendering coordinates based on where the element is rendered, the element index, different paddings
        and interspace
        """
        element.screen_position = self._screen_position + self._rendering_padding
        element.screen_position.y += - self._slot_height_coeff * element_slot_index

    def get_slot_height(self) -> int:
        """
        Returns the sum of all the slot height of the contained elements
        """
        slot_height = 0
        for element in self._elements:
            slot_height += element.get_slot_height()

        return slot_height

    def set_elements(self, elements: [AbstractHUDElementSlot]) -> None:
        """
        Sets the elements part of this ElementContainer
        """
        self._elements = elements

    def set_rendering_padding(self, padding_px: Point2D):
        """
        Sets the padding that should be applied to all the rendered elements within that container
        """
        self._rendering_padding = padding_px
