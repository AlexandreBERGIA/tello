from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Formatters import Formatter
from ModulesV2.Formatters.Formats.AbstractFormat import AbstractFormat
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Slots.LabelHUDElementSlot import LabelHUDElementSlot
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class DronePropertyHUDElementSlot(LabelHUDElementSlot, ABC):
    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D, drone: AbstractDrone,
                 label: str, property_getter: str, suffix: str = '', format_list: [AbstractFormat] = []):
        super().__init__(image_fetcher, screen_position, label)
        self._property_getter = property_getter
        self._suffix = suffix
        self._drone = drone
        self._format_list = format_list

    def get_label(self) -> str:
        """
        Overrides the parent method to get a specific property
        """
        return self._label + ': ' + str(self.get_property_value()) + self._suffix

    def get_property_value(self) -> str:
        """
        Returns the expected property value as string. This value is concatenated to the label when getting the label
        """
        # Get the drone property value, then format it
        value = self._drone.get_property_value(self._property_getter)
        value = Formatter.format_value(value, self._format_list)
        return value
