from abc import ABC

import cv2

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Formatters import Formatter
from ModulesV2.Formatters.Formats.AbstractFormat import AbstractFormat
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.AbstractHUDElement import AbstractHUDElement
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class AltimeterHUDElement(AbstractHUDElement, ABC):
    """
    Paint the Altimeter on the image. The rendering point is the top left corner of the box
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, drone: AbstractDrone, format_list: [AbstractFormat] = []):
        super().__init__(image_fetcher, Point2D(0.5, 0.2))
        self._drone = drone
        self._format_list = format_list
        self._box_size = Point2D(0.25, 0.15)
        self._text_offset = Point2D(0.03, 0.03)

        self._box_coordinates_1 = Point2D(0, 0)
        self._box_coordinates_2 = self._box_coordinates_1 + self._box_size
        self._text_coordinates = self._box_coordinates_1 + self._text_offset

    def _paint(self) -> None:
        super(AltimeterHUDElement, self)._paint()

        # Paint the box around the values. All the values are relative to the rendering coordinates
        cv2.rectangle(self._image_fetcher.get_image(), self.rel_to_px(self._box_coordinates_1),
                      self.rel_to_px(self._box_coordinates_2), self.get_display_color(),
                      self.LINE_THICKNESS_DEFAULT)

        # Paint the altimeter value
        altitude = Formatter.format_value(self._drone.get_ground_altitude(), self._format_list)
        cv2.putText(self._image_fetcher.get_image(), altitude, self.rel_to_px(self._text_coordinates),
                    self.TEXT_FONT_DEFAULT, self.TEXT_SIZE_DEFAULT, self.get_display_color(),
                    self.TEXT_THICKNESS_DEFAULT)
