import math
from abc import ABC

import cv2

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.AbstractHUDElement import AbstractHUDElement
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class RollHUDElement(AbstractHUDElement, ABC):
    """
    Paint the Roll scale on the image.
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, drone: AbstractDrone):
        super().__init__(image_fetcher, Point2D(0, 0.3))  # The rendering coordinates correspond to the wheel center
        self._drone = drone

        self._wheel_inner_radius = 0.44
        self._wheel_outer_radius = 0.48
        self._zero_additional_length = 0.04
        self._current_roll_indicator_offset = Point2D(0, -0.12)  # Offset of the current indicator after the bottom of
        # the wheel
        self._triangle_indicator_height = 0.04
        self._triangle_base_width = 0.04
        self._display_amplitude_deg = 55

        # Determine the triangle points for the current rolls indicator
        self._triangle_a_point = Point2D(0, -self._wheel_inner_radius) + self._current_roll_indicator_offset
        self._triangle_b_point = Point2D(-self._triangle_base_width / 2, -self._triangle_indicator_height) \
                                 + self._triangle_a_point
        self._triangle_c_point = Point2D(self._triangle_base_width / 2, -self._triangle_indicator_height) \
                                 + self._triangle_a_point

    def _paint(self) -> None:
        super(RollHUDElement, self)._paint()
        self._paint_current_indicator()
        self._paint_roll()

    def _paint_current_indicator(self) -> None:
        """
        Paint the static indicator that points to the current roll
        """
        # Paint the triangle that points to the current roll
        cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(self._triangle_a_point),
                 self.rel_to_px(self._triangle_b_point), self.get_display_color(), self.LINE_THICKNESS_DEFAULT)
        cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(self._triangle_b_point),
                 self.rel_to_px(self._triangle_c_point), self.get_display_color(), self.LINE_THICKNESS_DEFAULT)
        cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(self._triangle_c_point),
                 self.rel_to_px(self._triangle_a_point), self.get_display_color(), self.LINE_THICKNESS_DEFAULT)

    def _paint_roll(self) -> None:
        """
        Paints the static part of the element with the scale starting from 0 up to the max altitude the drone can fly
        """
        scale_offset_deg = 90  # Adds 90 deg to the roll scale to render the 0 at the bottom
        current_roll_angle_deg = self._drone.get_roll()

        min_rendering_angle_deg = scale_offset_deg - self._display_amplitude_deg
        max_rendering_angle_deg = scale_offset_deg + self._display_amplitude_deg

        # Pain the lines that represent every 10 deg
        for scale_angle_deg in range(0, 360, 10):
            # Define the rendering angles in degree and gradients
            painted_angle_deg = (scale_angle_deg - current_roll_angle_deg + scale_offset_deg) % 360
            painted_angle_rad = painted_angle_deg * math.pi / 180

            # Skips the paining process if we are outside the allowed amplitude
            if not min_rendering_angle_deg < painted_angle_deg < max_rendering_angle_deg:
                continue

            # Determine line points to render
            inner_coordinates_x = self._wheel_inner_radius * math.cos(painted_angle_rad)
            inner_coordinates_y = - self._wheel_inner_radius * math.sin(painted_angle_rad)
            inner_coordinates = Point2D(inner_coordinates_x, inner_coordinates_y)

            # Makes the down (0 deg) line longer
            length = self._wheel_outer_radius
            line_thickness = self.LINE_THICKNESS_DEFAULT
            if scale_angle_deg == 0:
                length = self._wheel_outer_radius + self._zero_additional_length
                line_thickness = self.LINE_THICKNESS_BOLD

            outer_coordinates_x = length * math.cos(painted_angle_rad)
            outer_coordinates_y = - length * math.sin(painted_angle_rad)
            outer_coordinates = Point2D(outer_coordinates_x, outer_coordinates_y)

            cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(inner_coordinates),
                     self.rel_to_px(outer_coordinates), self.get_display_color(), line_thickness)
