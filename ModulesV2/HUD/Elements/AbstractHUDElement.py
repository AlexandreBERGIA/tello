from abc import ABC
from typing import Tuple

import cv2

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.AbstractHUD import AbstractHUD
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class AbstractHUDElement(AbstractHUD, ABC):
    """
    This is a basic HUD element that can be rendered on an image based on a point
    """

    GRAPHIC_DEBUG = False

    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D):
        super(AbstractHUDElement, self).__init__(image_fetcher)
        self._screen_position = screen_position

    def _paint(self) -> None:
        super(AbstractHUDElement, self)._paint()
        if self.GRAPHIC_DEBUG:
            cv2.circle(self.get_image_fetcher().get_image(), self.screenspace_pos_to_px(self._screen_position),
                       self.screen_size_to_px(0.005), self.COLOR_DEBUG_PURPLE, 7)

    @property
    def image_position(self) -> Tuple[int, int]:
        """
        Returns the image position in pixels
        """
        return self.screenspace_pos_to_px(self._screen_position)

    def rel_to_px(self, point: Point2D) -> Tuple[int, int]:
        """
        Returns the point position offset by the element screen position and converter to image position
        """
        return self.screenspace_pos_to_px(point + self.screen_position)

    @property
    def screen_position(self) -> Point2D:
        """
        Returns the screen position of the element
        """
        return self._screen_position

    @screen_position.setter
    def screen_position(self, screen_position: Point2D):
        """
        Sets the rendering coordinates of the element
        """
        self._screen_position = screen_position
