import logging
from abc import abstractmethod

from ModulesV2.Geometry.Point2D import Point2D


class AbstractGraphValues:
    def __init__(self):
        self._values = [Point2D]

        self._range_min_x: float = 0
        self._range_max_x: float = 0
        self._range_min_y: float = 0
        self._range_max_y: float = 0

        self.range_min_x = 0
        self.range_max_x = 1000
        self.range_min_y = 0
        self.range_max_y = 1000

    def fetch_values(self) -> None:
        """
        Fetches the new values
        """
        values = self._fetch()
        self._values = self.trim_range(values)

    @abstractmethod
    def _fetch(self) -> [Point2D]:
        """
        Fetches the new values
        """

    def is_point_in_range(self, values: [Point2D], index: int) -> bool:
        """
        Returns True when the point at the provided index is within the rendering range, else False
        """
        try:
            point = values[index]
        except KeyError:
            logging.warning('The point at the request index does not exist: {}'.format(index))
            return False

        # Ensure the point can be rendered withing the boundaries
        if point.x < self._range_min_x or point.x > self._range_max_x:
            return False
        if point.y < self._range_min_y or point.y > self._range_max_y:
            return False

        return True

    def trim_range(self, values: [Point2D]) -> [Point2D]:
        """
        Return the list of points that are in range for painting
        """
        in_range = []
        for index in range(len(values)):
            if self.is_point_in_range(values, index):
                in_range.append(values[index])

        return in_range

    @property
    def range_min_x(self) -> float:
        return self._range_min_x

    @range_min_x.setter
    def range_min_x(self, range_min_x: float) -> None:
        self._range_min_x = range_min_x

    @property
    def range_max_x(self) -> float:
        return self._range_max_x

    @range_max_x.setter
    def range_max_x(self, range_max_x: float) -> None:
        self._range_max_x = range_max_x

    @property
    def range_min_y(self) -> float:
        return self._range_min_y

    @range_min_y.setter
    def range_min_y(self, range_min_y: float) -> None:
        self._range_min_y = range_min_y

    @property
    def range_max_y(self) -> float:
        return self._range_max_y

    @range_max_y.setter
    def range_max_y(self, range_max_y: float) -> None:
        self._range_max_y = range_max_y

    @property
    def range_x(self) -> float:
        return self._range_max_x - self._range_min_x

    @property
    def range_y(self) -> float:
        return self._range_max_y - self._range_min_y

    @property
    def values(self) -> [Point2D]:
        """
        Returns the fetched values
        """
        return self._values
