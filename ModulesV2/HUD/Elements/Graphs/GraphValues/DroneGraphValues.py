from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.Graphs.GraphValues.AbstractGraphValues import AbstractGraphValues


class DroneGraphValues(AbstractGraphValues, ABC):
    def __init__(self, drone: AbstractDrone, value_name: str):
        super().__init__()
        self._drone = drone
        self._value_name = value_name

    def _fetch(self) -> [Point2D]:
        # Update the min and max x range to display the last ms
        values = self._drone.get_captured_data()[self._value_name]
        values_end_ms = values[len(values) - 1].x

        # If the data hasn't passed the range, return all the data without updating the range
        if values_end_ms <= self.range_max_x:
            return values

        # Determine the lowest value of the window and update the range
        range_start_ms = values_end_ms - self.range_x
        self.range_min_x = range_start_ms
        self.range_max_x = values_end_ms
        return values
