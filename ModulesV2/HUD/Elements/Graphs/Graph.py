from abc import ABC

import cv2

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.AbstractHUDElement import AbstractHUDElement
from ModulesV2.HUD.Elements.Graphs.GraphValues.AbstractGraphValues import AbstractGraphValues
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class Graph(AbstractHUDElement, ABC):
    """
    Renders a graph
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, screen_position: Point2D,
                 values_fetcher: AbstractGraphValues):
        super().__init__(image_fetcher, screen_position)
        self._values_fetcher = values_fetcher

        self._size: Point2D = Point2D(1, 0.3)  # The rendering size in pixels

        self.step_size_x: int = 10  # Display a step on the x-axis every n quantity of units
        self.step_size_y: int = 10  # Display a step on the y-axis every n quantity of units
        self.step_length: float = 0.01  # The length of a step line
        self.is_paint_frame: bool = False
        self.is_paint_step_x: bool = True
        self.is_paint_step_y: bool = True

    def _paint(self) -> None:
        """
        Paint the graph
        """
        # Get the new values
        self._values_fetcher.fetch_values()

        # Paint the graph components
        self._paint_frame()
        self._paint_axis()
        self._paint_graph()

    def _paint_axis(self) -> None:
        """
        Paint the axis of the graph
        """
        pt_y_max = Point2D(0, self.size.y)
        pt_x_max = Point2D(self._size.x, 0)
        pt_zero = Point2D(0, 0)

        cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(pt_y_max), self.rel_to_px(pt_zero),
                 self.get_display_color(), self.LINE_THICKNESS_DEFAULT)
        cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(pt_zero), self.rel_to_px(pt_x_max),
                 self.get_display_color(), self.LINE_THICKNESS_DEFAULT)

        self._paint_axis_steps()

    def _paint_axis_steps(self) -> None:
        """
        Paint the axis steps
        """
        # Paint the x-axis steps
        pt1_y = 0
        pt2_y = self.step_length
        for step_value in range(int(self._values_fetcher.range_min_x), int(self._values_fetcher.range_max_x),
                                self.step_size_x):
            pt_x = step_value * self.scale_x
            pt1 = Point2D(pt_x, pt1_y)
            pt2 = Point2D(pt_x, pt2_y)
            cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(pt1), self.rel_to_px(pt2),
                     self.get_display_color(), self.LINE_THICKNESS_DEFAULT)

        # Paint the y-axis steps
        pt1_x = 0
        pt2_x = self.step_length
        for step_value in range(int(self._values_fetcher.range_min_y), int(self._values_fetcher.range_max_y),
                                self.step_size_y):
            pt_y = step_value * self.scale_y
            pt1 = Point2D(pt1_x, pt_y)
            pt2 = Point2D(pt2_x, pt_y)
            cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(pt1), self.rel_to_px(pt2),
                     self.get_display_color(), self.LINE_THICKNESS_DEFAULT)

    def _paint_graph(self) -> None:
        """
        Pain the points of the graph
        """
        values = self._values_fetcher.values
        for point in values:
            point_coordinates = self.get_point_coordinates(point)

            # Render the point
            cv2.circle(self.get_image_fetcher().get_image(), self.rel_to_px(point_coordinates), 2,
                       self.get_display_color(), 2)

    def _paint_frame(self) -> None:
        """
        Paints the frame around the graph when the paint_frame property is True
        """
        if not self.is_paint_frame:
            return

        pt1 = Point2D(0, 0)
        pt2 = Point2D(self._size.x, self._size.y)
        cv2.rectangle(self.get_image_fetcher().get_image(), self.rel_to_px(pt1), self.rel_to_px(pt2),
                      self.get_display_color(), self.LINE_THICKNESS_DEFAULT)

    def get_point_coordinates(self, point: Point2D) -> Point2D:
        """
        Returns the pixel coordinates of the provided point
        """
        # Determines the x and y coordinates
        point_x = (point.x - self._values_fetcher.range_min_x) * self.scale_x
        point_y = (point.y - self._values_fetcher.range_min_y) * self.scale_y
        return Point2D(point_x, point_y)

    @property
    def scale_x(self) -> float:
        return self.size.x / self._values_fetcher.range_x

    @property
    def scale_y(self) -> float:
        return self.size.y / self._values_fetcher.range_y

    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, size: Point2D) -> None:
        self._size = size
