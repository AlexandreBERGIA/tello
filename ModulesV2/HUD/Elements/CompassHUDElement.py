from abc import ABC
from typing import List

import cv2

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Formatters import Formatter
from ModulesV2.Formatters.Formats.AbstractFormat import AbstractFormat
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.AbstractHUDElement import AbstractHUDElement
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class CompassHUDElement(AbstractHUDElement, ABC):
    """
    Paint a compass on the image. The rending coordinates correspond to the center of the compass and top/middle portion
     of the current indicator box
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, drone: AbstractDrone,
                 current_heading_format: [AbstractFormat] = [], compass_heading_format: [AbstractFormat] = []):
        super().__init__(image_fetcher, Point2D(0, 0.5))  # The rendering coordinates correspond to the wheel center
        self._drone = drone
        self._current_heading_format = current_heading_format
        self._compass_heading_format = compass_heading_format

        # Define the variables to pain the current heading box
        self._current_heading_box_size = Point2D(0.25, 0.15)
        self._current_heading_box_point_a = Point2D(-self._current_heading_box_size.x / 2, 0)
        self._current_heading_box_point_b = Point2D(
            self._current_heading_box_point_a.x + self._current_heading_box_size.x,
            self._current_heading_box_point_a.y + self._current_heading_box_size.y)
        self._current_heading_text_padding = Point2D(0.07, 0.05)
        self._current_heading_text_point = self._current_heading_box_point_a + self._current_heading_text_padding

        # Compass variables
        self._compass_offset = Point2D(0, 0.2)  # Offset from the rendering point to the top portion of the indexes
        self._compass_text_offset = Point2D(-0.05, 0.35)  # Offset from the rendering point
        self._compass_step_degree = 5  # Renders a compass every x degrees
        self._compass_main_step_deg = 10  # Main steps display the angle as text
        self._compass_rendering_amplitude = 90  # How wide the compass will render in degrees
        self._compass_deg_coeff = 0.035  # How many screen space represent one degree
        self._compass_main_index_length = 0.12
        self._compass_secondary_index_length = 0.06

    def _paint(self) -> None:
        super(CompassHUDElement, self)._paint()
        self._paint_current_indicator()
        self._paint_compass()

    def _paint_current_indicator(self) -> None:
        """
        Paints the box that contains the value of the current heading
        """
        # Get the heading
        heading = Formatter.format_value(self.get_wrapped_angle(self._drone.get_heading()),
                                         self._current_heading_format)

        # Paint the box and the text
        cv2.rectangle(self.get_image_fetcher().get_image(), self.rel_to_px(self._current_heading_box_point_a),
                      self.rel_to_px(self._current_heading_box_point_b), self.get_display_color(),
                      self.LINE_THICKNESS_DEFAULT)
        cv2.putText(self.get_image_fetcher().get_image(), str(heading),
                    self.rel_to_px(self._current_heading_text_point), self.TEXT_FONT_DEFAULT, self.TEXT_SIZE_DEFAULT,
                    self.get_display_color(), self.TEXT_THICKNESS_DEFAULT)

    def _paint_compass(self) -> None:
        """
        Paints 360 degree compass
        """
        compass_index_list = self.get_compass_index_list()

        for index, heading in enumerate(compass_index_list):
            is_main_index = heading % self._compass_main_step_deg == 0
            relative_heading_deg = heading - self._drone.get_heading()
            relative_heading_pos_x = relative_heading_deg * self._compass_deg_coeff

            heading_line_length = self._compass_main_index_length if is_main_index else self._compass_secondary_index_length
            compass_heading_bottom_point = Point2D(relative_heading_pos_x, 0) + self._compass_offset
            compass_heading_top_point = compass_heading_bottom_point + Point2D(0, heading_line_length)
            compass_text = Point2D(relative_heading_pos_x, 0) + self._compass_text_offset
            cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(compass_heading_bottom_point),
                     self.rel_to_px(compass_heading_top_point), self.get_display_color(),
                     self.LINE_THICKNESS_DEFAULT)

            # Skip the text for non-main indexes
            if not is_main_index:
                continue
            wrapped_heading = int(self.get_wrapped_angle(heading) / 10)
            wrapped_heading = Formatter.format_value(wrapped_heading, self._compass_heading_format)
            cv2.putText(self.get_image_fetcher().get_image(), str(wrapped_heading),
                        self.rel_to_px(compass_text),
                        self.TEXT_FONT_DEFAULT, self.TEXT_SIZE_1, self.get_display_color(), self.TEXT_THICKNESS_DEFAULT)

    def get_compass_index_list(self) -> List[int]:
        """
        Returns the list of indexes to be rendered for the compass
        """
        current_heading = self._drone.get_heading()

        # Generate the list of indexes before the current heading
        left_max_index = int((current_heading - self._compass_rendering_amplitude / 2) / self._compass_step_degree) * \
                         self._compass_step_degree
        right_max_index = int((current_heading + self._compass_rendering_amplitude / 2) / self._compass_step_degree) * \
                          self._compass_step_degree
        return [*range(left_max_index, right_max_index + 1, self._compass_step_degree)]

    def get_wrapped_angle(self, angle: float) -> float:
        """
        Wrap any angle lower than 0 and higher than 360 around
        """
        if angle < 0:
            return 360 + angle
        elif angle > 360:
            return angle - 360
        else:
            return angle
