from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.AbstractHUDElement import AbstractHUDElement
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class AbstractDronePropertyHUDElement(AbstractHUDElement, ABC):
    def __init__(
            self,
            image_fetcher: AbstractImageFetcher,
            screen_position: Point2D,
            drone: AbstractDrone,
            property_getter: str,
    ):
        super().__init__(image_fetcher, screen_position)
        self._property_getter = property_getter
        self._drone = drone

    def get_property_value(self):
        """
        Returns the expected property value as string.
        """
        return self._drone.get_property_value(self._property_getter)
