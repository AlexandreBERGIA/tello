import math
from abc import ABC
from typing import List, Dict

import cv2

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Formatters import Formatter
from ModulesV2.Formatters.Formats.AbstractFormat import AbstractFormat
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.AbstractHUDElement import AbstractHUDElement
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class ArtificialHorizonHUDElement(AbstractHUDElement, ABC):
    """
    Paint the artificial horizon. This class assumes the image is 1000 x 800
    """

    FORMAT_LEFT_PITCH_TEXT = 'left_pitch_text'
    FORMAT_RIGHT_PITCH_TEXT = 'right_pitch_text'

    _empty_formats = {
        FORMAT_LEFT_PITCH_TEXT: [],
        FORMAT_RIGHT_PITCH_TEXT: [],
    }

    def __init__(self, image_fetcher: AbstractImageFetcher, drone: AbstractDrone,
                 formats: Dict[str, AbstractFormat] = _empty_formats):
        rendering_coordinates = Point2D(0, 0)  # The rendering coordinates correspond to the screen center
        super().__init__(image_fetcher, rendering_coordinates)
        self._drone = drone
        self._formats = formats

        self._vertical_length = 0.05  # The length of the ending vertical lines
        self._horizontal_primary_length = 1  # The length of the primary horizontal line
        self._horizontal_secondary_length = 0.6  # The length of a secondary horizontal line
        self._horizon_center_space_length = 0.2  # The empty space in the 0 deg horizontal line
        self._horizontal_lines_degree_step = 5  # Draw an horizontal line every x degrees
        self._degree_to_screen_position_coeff = 0.05  # The coefficient to convert an angle to a screen position
        self._horizontal_lines_display_quantity = 5  # The humber of horizontal lines to display
        self._step_increment_angle = 5  # Displays a line every x angle
        self._text_offset_right = Point2D(0.05, -0.02)  # Offset for text displayed to the right of the angle line
        self._text_offset_left = Point2D(-0.20, -0.02)  # Offset for text displayed to the left of the angle line

        # Calculates the camera offset y to apply due to it pitch angle
        self._camera_offset_y = -self._drone.get_camera_pitch_angle() * self._degree_to_screen_position_coeff

    def _paint(self) -> None:
        image = self.get_image_fetcher().get_image()
        drone_roll_angle_rad = self._drone.get_roll() * math.pi / 180

        # Calculate the y offset of the camera
        lines_offset_y = (-self._drone.get_pitch() * self._degree_to_screen_position_coeff) + self._camera_offset_y

        # Draw the horizon lines
        for horizon_index in self.get_horizon_indexes():
            # Set the multiplier that determines whether the vertical line points up or down
            vertical_position_multiplier = 1 if horizon_index > 0 else -1

            # Set the horizontal line length
            horizontal_length = self._horizontal_secondary_length if horizon_index != 0 else self._horizontal_primary_length
            # Set the points for the horizontal lines
            a_x = - horizontal_length / 2
            b_x = - self._horizon_center_space_length / 2
            c_x = self._horizon_center_space_length / 2
            d_x = horizontal_length / 2
            horizon_y = self._degree_to_screen_position_coeff * horizon_index + lines_offset_y
            vertical_y = horizon_y + self._vertical_length * vertical_position_multiplier

            # Create the point objects to draw the lines
            horizon_a = Point2D(a_x, horizon_y)
            horizon_b = Point2D(b_x, horizon_y)
            horizon_c = Point2D(c_x, horizon_y)
            horizon_d = Point2D(d_x, horizon_y)
            vertical_1 = Point2D(a_x, vertical_y)
            vertical_2 = Point2D(d_x, vertical_y)
            txt_a = horizon_a + self._text_offset_left
            txt_b = horizon_d + self._text_offset_right

            # Rotate the points based on the roll
            horizon_a.rotate(self.screen_position.point, drone_roll_angle_rad)
            horizon_b.rotate(self.screen_position.point, drone_roll_angle_rad)
            horizon_c.rotate(self.screen_position.point, drone_roll_angle_rad)
            horizon_d.rotate(self.screen_position.point, drone_roll_angle_rad)
            vertical_1.rotate(self.screen_position.point, drone_roll_angle_rad)
            vertical_2.rotate(self.screen_position.point, drone_roll_angle_rad)
            txt_a.rotate(self.screen_position.point, drone_roll_angle_rad)
            txt_b.rotate(self.screen_position.point, drone_roll_angle_rad)

            # Draw the lines
            cv2.line(image, self.rel_to_px(horizon_a), self.rel_to_px(horizon_b), self.get_display_color(),
                     self.LINE_THICKNESS_DEFAULT)
            cv2.line(image, self.rel_to_px(horizon_c), self.rel_to_px(horizon_d), self.get_display_color(),
                     self.LINE_THICKNESS_DEFAULT)
            # Draw on all angles except 0
            if horizon_index != 0:
                # Draw the vertical lines
                cv2.line(image, self.rel_to_px(horizon_a), self.rel_to_px(vertical_1), self.get_display_color(),
                         self.LINE_THICKNESS_DEFAULT)
                cv2.line(image, self.rel_to_px(horizon_d), self.rel_to_px(vertical_2), self.get_display_color(),
                         self.LINE_THICKNESS_DEFAULT)

                # Draw the numbers

                number_a = Formatter.format_value(horizon_index, self._formats[self.FORMAT_LEFT_PITCH_TEXT])
                number_b = Formatter.format_value(horizon_index, self._formats[self.FORMAT_RIGHT_PITCH_TEXT])
                cv2.putText(image, str(number_a), self.rel_to_px(txt_a), self.TEXT_FONT_DEFAULT, self.TEXT_SIZE_DEFAULT,
                            self.get_display_color(), self.TEXT_THICKNESS_DEFAULT)
                cv2.putText(image, str(number_b), self.rel_to_px(txt_b), self.TEXT_FONT_DEFAULT, self.TEXT_SIZE_DEFAULT,
                            self.get_display_color(), self.TEXT_THICKNESS_DEFAULT)

    def get_horizon_indexes(self) -> List[int]:
        """
        Gets the horizon line index based on the pitch
        """
        # Calculate where the center of the view points to
        view_center_pitch_deg = self._drone.get_pitch() + self._drone.get_camera_pitch_angle()

        # Determine the closest angle index to the horizon line and adds all the extra indexes
        center_horizon_line_closest_index = int(
            view_center_pitch_deg / self._horizontal_lines_degree_step) * self._horizontal_lines_degree_step
        bottom_index = center_horizon_line_closest_index - (
                self._horizontal_lines_display_quantity * self._horizontal_lines_degree_step)
        top_index = center_horizon_line_closest_index + (
                self._horizontal_lines_display_quantity * self._horizontal_lines_degree_step)
        index_list = [*range(bottom_index, top_index, self._horizontal_lines_degree_step)]

        # Trim the values above and below +90 and -90 since the horizon will shift of the other side
        index_list[:] = [x for x in index_list if -90 <= x <= 90]

        return index_list
