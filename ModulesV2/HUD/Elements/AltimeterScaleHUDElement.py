from abc import ABC

import cv2

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.AbstractHUDElement import AbstractHUDElement
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class AltimeterScaleHUDElement(AbstractHUDElement, ABC):
    """
    Paint the Altimeter scale on the image. The rendering point is the bottom point of the scale
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, drone: AbstractDrone):
        super().__init__(image_fetcher, Point2D(0.8, -0.8))

        self._drone = drone
        self._vertical_line_length = 0.9
        self._vertical_line_quantity = 12  # 10 + the min and max
        self._horizontal_lines_length = 0.02
        self._horizontal_line_interspace = self._vertical_line_length / (self._vertical_line_quantity - 2)
        self._min_ground_altitude = 0
        self._max_ground_altitude = self._drone.get_max_ground_altitude()
        self._current_altitude_overflow_percent = 0.05
        self._text_offset = Point2D(0.002, -0.02)

        # Generate static values based on the configuration
        self._max_altitude_overflow_percent = 1 + self._current_altitude_overflow_percent
        self._min_altitude_overflow_percent = 0 - self._current_altitude_overflow_percent
        self._vertical_line_top_point = Point2D(0, self._vertical_line_length)
        self._vertical_line_bottom_point = Point2D(0, 0)

        self._horizontal_lines = []
        # Define the horizontal lines
        for index in range(0, self._vertical_line_quantity - 1):
            horizontal_line_y_offset = index * self._horizontal_line_interspace
            horizontal_line_start_point = Point2D(0, horizontal_line_y_offset)
            horizontal_line_end_point = Point2D(self._horizontal_lines_length, horizontal_line_y_offset)
            self._horizontal_lines.append([horizontal_line_start_point, horizontal_line_end_point])

    def _paint(self) -> None:
        super(AltimeterScaleHUDElement, self)._paint()
        self._paint_scale()
        self._paint_current_altitude()

    def _paint_scale(self) -> None:
        """
        Paints the static part of the element with the scale starting from 0 up to the max altitude the drone can fly
        """
        # Paint the main vertical line

        cv2.line(self._image_fetcher.get_image(), self.rel_to_px(self._vertical_line_bottom_point),
                 self.rel_to_px(self._vertical_line_top_point), self.get_display_color(),
                 self.LINE_THICKNESS_DEFAULT)

        # Pain the horizontal lines
        for line in self._horizontal_lines:
            cv2.line(self._image_fetcher.get_image(), self.rel_to_px(line[0]), self.rel_to_px(line[1]),
                     self.get_display_color(), self.LINE_THICKNESS_DEFAULT)

        # Paint the numbers
        min_altitude_text_point = self._vertical_line_bottom_point + Point2D(self._horizontal_lines_length,
                                                                             0) + self._text_offset
        max_altitude_text_point = self._vertical_line_top_point + Point2D(self._horizontal_lines_length,
                                                                          0) + self._text_offset
        cv2.putText(self._image_fetcher.get_image(), str(self._min_ground_altitude),
                    self.rel_to_px(min_altitude_text_point), self.TEXT_FONT_DEFAULT, self.TEXT_SIZE_DEFAULT,
                    self.get_display_color(), self.TEXT_THICKNESS_DEFAULT)
        cv2.putText(self._image_fetcher.get_image(), str(self._max_ground_altitude),
                    self.rel_to_px(max_altitude_text_point), self.TEXT_FONT_DEFAULT, self.TEXT_SIZE_DEFAULT,
                    self.get_display_color(), self.TEXT_THICKNESS_DEFAULT)

    def _paint_current_altitude(self) -> None:
        """
        Paints the current altitude on the scale
        """
        current_altitude = self._drone.get_ground_altitude()

        # Ensure the altitude percent doesn't pass the allowed buffer
        current_altitude_percent = current_altitude / self._max_ground_altitude
        if current_altitude_percent > self._max_altitude_overflow_percent:
            current_altitude_percent = self._max_altitude_overflow_percent
        elif current_altitude_percent < self._min_altitude_overflow_percent:
            current_altitude_percent = self._min_altitude_overflow_percent

        triangle_a_x = (self._horizontal_lines_length / 2) - 0.03
        triangle_a_y = current_altitude_percent * self._vertical_line_length

        triangle_a = Point2D(triangle_a_x, triangle_a_y) + self._screen_position
        triangle_b = triangle_a + Point2D(-0.08, -0.04)
        triangle_c = triangle_a + Point2D(-0.08, 0.04)
        cv2.line(self.get_image_fetcher().get_image(), self.screenspace_pos_to_px(triangle_a),
                 self.screenspace_pos_to_px(triangle_b), self.get_display_color(), self.LINE_THICKNESS_DEFAULT)
        cv2.line(self.get_image_fetcher().get_image(), self.screenspace_pos_to_px(triangle_b),
                 self.screenspace_pos_to_px(triangle_c), self.get_display_color(), self.LINE_THICKNESS_DEFAULT)
        cv2.line(self.get_image_fetcher().get_image(), self.screenspace_pos_to_px(triangle_c),
                 self.screenspace_pos_to_px(triangle_a), self.get_display_color(), self.LINE_THICKNESS_DEFAULT)
