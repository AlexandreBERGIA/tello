from abc import ABC

import cv2

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Elements.AbstractHUDElement import AbstractHUDElement
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class GridHUDElement(AbstractHUDElement, ABC):
    """
    Renders a grid on the image to display key positions. The rendering point is the center of the grid.
    Renders the grid only in debut mode
    """

    def __init__(self, image_fetcher: AbstractImageFetcher):
        screen_position = Point2D(0, 0)
        super().__init__(image_fetcher, screen_position)

        self._text_offset = Point2D(-0.06, 0.03)

    def _paint(self) -> None:
        super(GridHUDElement, self)._paint()

        # If not in debug mode, return right away
        if not self.GRAPHIC_DEBUG:
            return

        # Paint the horizontal lines
        for x in range(-8, 9, 2):  # Skip the first and last lines that should have been at -10 and 10
            x = x / 10
            top = Point2D(x, -1)
            bottom = Point2D(x, 1)
            cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(top), self.rel_to_px(bottom),
                     self.get_display_color(), 1)

            # Paint the point coordinates
            for y in range(-8, 9, 2):
                y = y / 10
                text_point = Point2D(x, y)
                cv2.putText(self.get_image_fetcher().get_image(), '{},{}'.format(text_point.x, text_point.y),
                            self.rel_to_px(text_point + self._text_offset), self.TEXT_FONT_DEFAULT,
                            self.TEXT_SIZE_DEFAULT * 0.4, self.get_display_color())

        # Paint the horizontal lines
        for y in range(-8, 9, 2):  # Skip the first and last lines that should have been at -10 and 10
            y = y / 10
            left = Point2D(-1, y)
            right = Point2D(1, y)
            cv2.line(self.get_image_fetcher().get_image(), self.rel_to_px(left), self.rel_to_px(right),
                     self.get_display_color(), 1)
