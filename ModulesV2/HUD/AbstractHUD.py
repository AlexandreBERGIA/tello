from abc import abstractmethod
from typing import Tuple

import cv2

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class AbstractHUD:
    # Declares the different colors for the graphics and text
    COLORSCHEME_DAY = 0
    COLORSCHEME_NIGHT = 1

    LINE_THICKNESS_DEFAULT = 2  # Defines the standard line thickness for HUD elements
    LINE_THICKNESS_BOLD = 3  # Defines the bold thickness for the HUD elements
    TEXT_FONT_DEFAULT = cv2.FONT_HERSHEY_SIMPLEX
    TEXT_THICKNESS_DEFAULT = 2
    TEXT_SIZE_DEFAULT = 1
    TEXT_SIZE_1 = 1.2
    COLOR_WHITE = [255, 255, 255]
    COLOR_GREEN = [255, 0, 255]
    COLOR_DEBUG_PURPLE = [255, 0, 255]

    _color_schemes = [
        COLOR_WHITE,
        COLOR_GREEN
    ]

    def __init__(self, image_fetcher: AbstractImageFetcher):
        self._selected_color_scheme = self.COLORSCHEME_DAY
        self._is_visible = True
        self._image_fetcher = image_fetcher

    @abstractmethod
    def _paint(self) -> None:
        """
        The actual actions that paints the element if the element is visible
        """
        pass

    @abstractmethod
    def _preprocess_paint(self) -> None:
        """
        Perform any preprocessing required before painting the element if the element is visible
        """
        pass

    def get_display_color(self) -> list:
        """
        Returns the display color in RGB
        """
        return self.get_display_color_schemes()[self._selected_color_scheme]

    def get_display_color_schemes(self) -> list:
        """
        Returns the list of color schemes
        """
        return self._color_schemes

    def get_image_fetcher(self) -> AbstractImageFetcher:
        """
        Returns the image fetcher responsible for fetching images in that HUD
        """
        return self._image_fetcher

    def get_is_visible(self) -> bool:
        """
        Returns True when the HUD element is visible
        """
        return self._is_visible

    @property
    def image_size(self) -> Point2D:
        """
        Returns the image size from the image fetcher
        """
        return self._image_fetcher.image_size

    def paint(self) -> None:
        """
        Will be called for each HUD element to render itself
        """
        if self.get_is_visible():
            self._preprocess_paint()
            self._paint()

    def screenspace_pos_to_px(self, screen_space_point: Point2D) -> Tuple[int, int]:
        """
        Takes a point in screen space coordinates and converts it to a tuple with pixel indexes for an image \n
        -1,1________1,1 \n
        _____0,0_____ \n
        -1,-1______1,-1
        """
        # Convert the screen space to a space from 0 to 1
        coeff_x = (screen_space_point.x + 1) / 2
        coeff_y = 1 - (screen_space_point.y + 1) / 2
        return int(coeff_x * self.image_size.x), int(coeff_y * self.image_size.y)

    def screen_size_to_px(self, size: float) -> int:
        """
        Returns the screen size of a screen space length based on the image width (x)
        """
        return int(size * self.image_size.x)

    def set_is_visible(self, is_visible: bool) -> None:
        """
        Sets the visibility of the HUD element
        """
        self._is_visible = is_visible
