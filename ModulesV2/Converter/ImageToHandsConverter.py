import threading
from abc import ABC
from queue import Queue

from cvzone.HandTrackingModule import HandDetector

from ModulesV2.Converter.Converter import Converter
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher
from ModulesV2.Loggers import Logger


class ImageToHandsConverter(Converter, ABC):
    """
    Converts an image from the provided image fetcher to hands
    """

    def __init__(self, image_fetcher: AbstractImageFetcher, detector: HandDetector):
        self._image_fetcher = image_fetcher
        self._detector = detector
        self._detected_hands_queue = Queue(1)
        kwargs = {
            'image_fetcher': image_fetcher
        }
        self._thread = threading.Thread(target=self._hands_detect_thread, name='Hands Detection', kwargs=kwargs)
        self._thread.daemon = True

        self.hands_quantity = 2
        self._right_hand_index, self._left_hand_index = 1, 0
        self._is_detect_hands: bool = True

    def convert(self) -> []:
        """
        Extract the hands from the hand detector and save them into the hand controller.
        [0] Left
        [1] Right
        """
        hands = []

        # Skip the hands detection and return an empty array when the hands detection is turned off
        if not self.is_detect_hands:
            return hands

        # Get the image
        image = self._image_fetcher.get_image()

        # Skips the hands detection and return an empty array when there is no image available
        if image is None:
            return hands

        # Now that we have a valid image, we detect the hands
        # Starts the convert thread if not already started
        if not self._thread.isAlive():
            self._thread.start()

        # Returns the empty hands if there are no detected hands yet
        if self._detected_hands_queue.empty():
            return hands

        detected_hands = self._detected_hands_queue.get()

        # If no hands are detected, the function ends
        if detected_hands is None or len(detected_hands) != self.hands_quantity:
            return hands

        # Sets the hands into their respective indexes
        hands = [None] * len(detected_hands)
        if detected_hands[0]['type'] == 'Right':
            hands[self._right_hand_index] = detected_hands[0]
            hands[self._left_hand_index] = detected_hands[1]
        else:
            hands[self._left_hand_index] = detected_hands[0]
            hands[self._right_hand_index] = detected_hands[1]

        return hands

    @property
    def is_detect_hands(self) -> bool:
        """
        Returns True when the detector is set to detect hands
        """
        return self._is_detect_hands

    @is_detect_hands.setter
    def is_detect_hands(self, is_detect_hands: bool) -> None:
        """
        Sets the ability to detect hands
        """
        self._is_detect_hands = is_detect_hands

    def _hands_detect_thread(self, image_fetcher: AbstractImageFetcher) -> None:
        """
        Detect the hands from the image coming from the image fetcher
        """
        Logger.main_logger.info(type(self).__name__ + ',Start Detecting Hands')
        while True:
            detected_hands, image = self._detector.findHands(image_fetcher.get_image(), True, False)
            self._detected_hands_queue.put(detected_hands)
