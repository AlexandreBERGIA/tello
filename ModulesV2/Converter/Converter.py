from abc import abstractmethod


class Converter:
    """
    Base class for the converters. If exposes only the convert function
    """

    @abstractmethod
    def convert(self) -> any:
        """
        Perform the required conversion and returns its value
        """
        pass
