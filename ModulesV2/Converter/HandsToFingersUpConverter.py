from abc import ABC

from cvzone.HandTrackingModule import HandDetector

from ModulesV2.Converter.Converter import Converter


def _apply_fingers_up_correction(fingers_up: [int]) -> [int]:
    """
    Apply any correction that is necessary to the detected fingers up
    """
    for finger_index, finger_up in enumerate(fingers_up):
        # Reverse the thumb up information
        if finger_index == 0:
            fingers_up[finger_index] = _apply_thumb_up_correction(finger_up)

    return fingers_up


def _apply_thumb_up_correction(thumb_up_value: int) -> int:
    """
    Reverses the thumb up information
    """
    if thumb_up_value == 0:
        return 1
    else:
        return 0


class HandsToFingersUpConverter(Converter, ABC):
    """
    Converts Hands data to finger up data
    """

    def __init__(self, finger_detector: HandDetector) -> None:
        super(HandsToFingersUpConverter, self).__init__()

        self._finger_detector = finger_detector

        self._hands = []
        self._fingers_up = []

    def convert(self) -> []:
        self._fingers_up = []
        for hand in self._hands:
            fingers_up = self._finger_detector.fingersUp(hand)
            fingers_up = _apply_fingers_up_correction(fingers_up)
            self._fingers_up.append(fingers_up)

        return self._fingers_up

    def set_hands(self, hands: []) -> None:
        """
        Sets the hands the converter will use to determine the fingers up
        """
        self._hands = hands
