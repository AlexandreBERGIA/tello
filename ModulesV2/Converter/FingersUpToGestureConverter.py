from abc import ABC
from typing import Dict

from ModulesV2.Converter.Converter import Converter
from ModulesV2.Hands.HandGesture import HandGesture


class FingersUpToGestureConverter(Converter, ABC):
    """
    Converts Fingers up to Gestures
    """
    def __init__(self):
        self._fingers_up = []
        self._gestures = {}

    def convert(self) -> Dict[str, HandGesture]:
        """
        Converts the fingers up information into a list of Gestures
        """
        # If we don't have two hands, we reset the match info and skip the conversion
        if len(self._fingers_up) != 2:
            for hand_gesture in self._gestures.values():
                hand_gesture.reset_match()
            return self._gestures

        for gesture_key, hand_gesture in self._gestures.items():
            hand_gesture.fetch_match(self._fingers_up[0], self._fingers_up[1])

        return self._gestures

    def set_gestures(self, gestures:Dict[str, HandGesture]):
        """
        Sets the gestures that will use the fingers up information
        """
        self._gestures = gestures

    def set_fingers_up(self, fingers_up: []) -> None:
        """
        Sets the fingers up information that will be converted
        """
        self._fingers_up = fingers_up
