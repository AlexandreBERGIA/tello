from abc import ABC

from ModulesV2.Formatters.Formats.AbstractFormat import AbstractFormat


class TextRJust(AbstractFormat, ABC):
    """
    Fill the left portion of the provided text with the provided character until reaching the defined length
    """

    def __init__(self, length: int, char: chr(1)):
        self._length = length
        self._char = char

    def format(self, value: str):
        """
        Fill the left portion of the provided text with the provided character until reaching the defined length
        """
        return str(value).rjust(self._length, self._char)
