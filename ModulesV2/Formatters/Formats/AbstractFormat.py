from abc import abstractmethod


class AbstractFormat:
    @abstractmethod
    def format(self, value):
        """
        Format and return the provided value
        """
