import logging
import math

from ModulesV2.Formatters.Formats.AbstractFormat import AbstractFormat


class NumberRoundFormat(AbstractFormat):
    def __init__(self, round_digits: int):
        self._round_digits = round_digits

    def format(self, value) -> str:
        if value is None or math.isnan(value):
            logging.warning('Formatter expected a number value, actual: {}'.format(value))
            value = 0
        return '{:.{trailing_zeros}f}'.format(round(value, self._round_digits), trailing_zeros=self._round_digits)
