from ModulesV2.Formatters.Formats.AbstractFormat import AbstractFormat


def format_value(value, formatter_list: [AbstractFormat] = []) -> str:
    """
    Format a value with the list of specified formats
    """
    for value_format in formatter_list:
        value = value_format.format(value)
    return value
