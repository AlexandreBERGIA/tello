import math
import statistics

import numpy

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.HUD.Types.JoystickHUD import JoystickHUD
from ModulesV2.Hands.HandsController import HandsController
from ModulesV2.Joystick.JoystickController import JoystickController


def _get_joystick_xy(hand_center: Point2D, joystick_hud: JoystickHUD) -> Point2D:
    """
    Calculates the joystick XY values based on the joystick center coordinates and the current hand center
    coordinates.
    The values will be calculated between -100 and 100

    Returns the XY values as a Point2D
    """

    joystick_xy = Point2D(0, 0)

    # If the hand center is 0, 0, we skip all the calculation
    if hand_center.x == 0 and hand_center.y == 0:
        return joystick_xy

    # Calculates raw length in pixels
    raw_x = hand_center.x - joystick_hud.get_center().x
    raw_y = -(hand_center.y - joystick_hud.get_center().y)
    raw_length = math.hypot(raw_x, raw_y)

    # Use the max and min radius to define the max lengths and the dead zone in the center
    raw_length = numpy.clip(raw_length, 0, joystick_hud.joystick_max_radius)
    dead_zone_length = numpy.clip(raw_length, 0, joystick_hud.joystick_min_radius)
    raw_length -= dead_zone_length

    # Transform the raw length in percentage
    joystick_angle = math.atan2(raw_y, raw_x)
    raw_x = raw_length * math.cos(joystick_angle)
    raw_y = raw_length * math.sin(joystick_angle)
    joystick_xy.x = raw_x / (joystick_hud.joystick_max_radius - joystick_hud.joystick_min_radius) * 100
    joystick_xy.y = raw_y / (joystick_hud.joystick_max_radius - joystick_hud.joystick_min_radius) * 100

    return joystick_xy


class HandsToDualJoystickMapper:
    """
    Gets an image as input and extract from the detected hands two joystick controllers
    """

    def __init__(
            self,
            hands_controller: HandsController,
            left_joystick_hud: JoystickHUD,
            right_joystick_hud: JoystickHUD,
            left_joystick_controller: JoystickController,
            right_joystick_controller: JoystickController

    ):
        self._hands_controller = hands_controller
        self._right_joystick_hud = left_joystick_hud
        self._left_joystick_hud = right_joystick_hud
        self._right_joystick_controller = left_joystick_controller
        self._left_joystick_controller = right_joystick_controller

    def assign_hud_centers(self) -> None:
        """
        Sets the centers of the two joystick HUDs based on the current hands center
        """
        # This process only works when we have both hands
        hand_centers = self._hands_controller.get_hands_centers()
        if len(hand_centers) != 2:
            return

        self._left_joystick_hud.set_center(hand_centers[0])
        self._right_joystick_hud.set_center(hand_centers[1])

    def assign_joysticks_values(self) -> None:
        """
        Sets the two joystick HUD coordinates based on the saved HUD center and current hands center
        """
        # This process only works when we have both hands
        hand_centers = self._hands_controller.get_hands_centers()
        zero_joystick_center = Point2D(0, 0)
        if len(hand_centers) != 2:
            self._left_joystick_controller.set_joystick_xy(zero_joystick_center)
            self._right_joystick_controller.set_joystick_xy(zero_joystick_center)
            return

        # If the joysticks are hidden, we reset the values
        if not self._left_joystick_hud.get_is_visible():
            self._left_joystick_controller.set_joystick_xy(zero_joystick_center)
        if not self._right_joystick_hud.get_is_visible():
            self._right_joystick_controller.set_joystick_xy(zero_joystick_center)
        if not self._right_joystick_hud.get_is_visible() or not self._left_joystick_hud.get_is_visible():
            return

        left_hand_center = hand_centers[0]
        right_hand_center = hand_centers[1]

        left_joystick_xy = _get_joystick_xy(
            left_hand_center,
            self._left_joystick_hud
        )
        right_joystick_xy = _get_joystick_xy(
            right_hand_center,
            self._right_joystick_hud
        )

        self._left_joystick_controller.set_joystick_xy(left_joystick_xy)
        self._right_joystick_controller.set_joystick_xy(right_joystick_xy)

    def assign_size(self) -> None:
        """
        Define and sets the size of the joysticks depending on the hand sizes in the image
        """
        hand_to_joystick_factor = 0.8  # Defines how bigger the outer joystick radius is relative the hands width

        hand_size_left = self._hands_controller.get_hands_size(0)
        hand_size_right = self._hands_controller.get_hands_size(1)

        hand_size_average = statistics.mean([hand_size_left.x, hand_size_left.y, hand_size_right.x, hand_size_right.y])
        joystick_outer_radius = hand_size_average * hand_to_joystick_factor

        self._left_joystick_hud.set_outer_radius_size(joystick_outer_radius)
        self._right_joystick_hud.set_outer_radius_size(joystick_outer_radius)

    def get_joysticks_is_visible(self) -> bool:
        """
        Returns True when both joysticks are visible
        """
        return self._left_joystick_hud.get_is_visible() and self._right_joystick_hud.get_is_visible()

    def get_joystick_values(self) -> [Point2D]:
        """
        Returns the joystick values
        [0]: Left joystick
        [1]: Right joystick
        """
        return [
            self._left_joystick_controller.get_joystick_xy(),
            self._right_joystick_controller.get_joystick_xy(),
        ]

    def set_joysticks_is_visible(self, is_visible: bool) -> None:
        """
        Sets the visibility of the joystick huds
        """
        self._left_joystick_hud.set_is_visible(is_visible)
        self._right_joystick_hud.set_is_visible(is_visible)
