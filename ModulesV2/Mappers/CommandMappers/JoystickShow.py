from abc import ABC

from ModulesV2.Hands.HandsController import HandsController
from ModulesV2.Mappers.CommandMappers.AbstractGestureToCommandMapper import AbstractGestureToCommandMapper
from ModulesV2.Mappers.HandsToDualJoystickMapper import HandsToDualJoystickMapper


class JoystickShow(AbstractGestureToCommandMapper, ABC):
    """
    In charge of showing and setting the Joystick centers
    """

    def __init__(self, gesture_name: str, pattern_name: str, hands_to_joystick_mapper: HandsToDualJoystickMapper,
                 hands_controller: HandsController):
        super().__init__(gesture_name, pattern_name, hands_controller)
        self._hands_to_joystick_mapper = hands_to_joystick_mapper

    def _apply(self) -> None:
        self._hands_to_joystick_mapper.set_joysticks_is_visible(True)
        self._hands_to_joystick_mapper.assign_size()
        self._hands_to_joystick_mapper.assign_hud_centers()
        self._hands_to_joystick_mapper.assign_joysticks_values()
