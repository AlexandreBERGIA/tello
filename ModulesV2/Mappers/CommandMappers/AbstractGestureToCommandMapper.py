import logging
from abc import ABC

from ModulesV2.Hands.HandsController import HandsController
from ModulesV2.Mappers.CommandMappers.AbstractCommandMapper import AbstractCommandMapper


class AbstractGestureToCommandMapper(AbstractCommandMapper, ABC):
    """
    Transforms a gesture information into an applied input
    """

    def __init__(self, gesture_name: str, pattern_name: str, hands_controller: HandsController) -> None:
        super(AbstractGestureToCommandMapper, self).__init__()
        self._gesture_name = gesture_name
        self._pattern_name = pattern_name
        self._hands_controller = hands_controller

    def is_apply_allowed(self) -> bool:
        """
        Returns True only when the corresponding gesture matches the provided patterns
        """
        gesture_list = self._hands_controller.get_gestures()
        is_allowed = False
        try:
            is_allowed = gesture_list[self.get_gesture_name()].is_match(self.get_pattern_name())
        except KeyError:
            logging.warning(
                'The gesture does not exist: {}.{}'.format(self.get_gesture_name(), self.get_pattern_name()))
        return is_allowed

    def get_gesture_name(self) -> str:
        """
        Returns the gesture name
        """
        return self._gesture_name

    def get_pattern_name(self) -> str:
        """
        Returns the pattern name
        """
        return self._pattern_name
