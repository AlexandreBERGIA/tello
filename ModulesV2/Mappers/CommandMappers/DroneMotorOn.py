from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Hands.HandsController import HandsController
from ModulesV2.Mappers.CommandMappers.AbstractGestureToCommandMapper import AbstractGestureToCommandMapper


class DroneMotorOn(AbstractGestureToCommandMapper, ABC):
    """
    In charge of turning on the motor
    """

    def __init__(self, gesture_name: str, pattern_name: str, hands_controller: HandsController, drone: AbstractDrone):
        super().__init__(gesture_name, pattern_name, hands_controller)
        self._drone = drone

    def _apply(self) -> None:
        self._drone.turn_motor_on()
