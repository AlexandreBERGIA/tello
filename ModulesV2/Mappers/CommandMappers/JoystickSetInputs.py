from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.AbstractCommandMapper import AbstractCommandMapper
from ModulesV2.Mappers.HandsToDualJoystickMapper import HandsToDualJoystickMapper


class JoystickSetInputs(AbstractCommandMapper, ABC):
    """
    In charge of showing and setting the Joystick centers
    """

    def __init__(self, hands_to_joystick_mapper: HandsToDualJoystickMapper, drone: AbstractDrone):
        super(JoystickSetInputs, self).__init__()
        self._hands_to_joystick_mapper = hands_to_joystick_mapper
        self._drone = drone

    def is_apply_allowed(self) -> bool:
        return self._hands_to_joystick_mapper.get_joysticks_is_visible()

    def _apply(self) -> None:
        # Sets the joystick values to the HUD and the Joystick Controllers
        self._hands_to_joystick_mapper.assign_joysticks_values()

        # Then applies the controls to the drone
        joystick_values = self._hands_to_joystick_mapper.get_joystick_values()
        left_right = joystick_values[0].x
        forward_backward = joystick_values[1].y
        up_down = joystick_values[0].y
        yaw = joystick_values[1].x
        self._drone.send_rc_controls(int(left_right), int(forward_backward), int(up_down), int(yaw))
