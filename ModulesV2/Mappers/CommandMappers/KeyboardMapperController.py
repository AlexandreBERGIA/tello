from abc import ABC

import cv2

from ModulesV2.Mappers.CommandMappers.AbstractCommandMapper import AbstractCommandMapper
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractKeyboardMapper import AbstractKeyboardMapper


class KeyboardMapperController(AbstractCommandMapper, ABC):
    """
    Perform commands based on keyboard inputs
    """

    def __init__(self, keyboard_mapper_list: [AbstractKeyboardMapper]):
        self._keyboard_mapper_list = keyboard_mapper_list

    def is_apply_allowed(self) -> bool:
        """
        Returns always True so the _apply method is executed
        """
        return True

    def _apply(self) -> None:
        """
        Get the pressed key only once and compare it with all the keyboard mappers. Calls the apply method to all the
        mappers with the corresponding key
        """
        pressed_key = cv2.waitKey(1) & 0xFF

        for keyboard_mapper in self._keyboard_mapper_list:
            keyboard_mapper.pressed_key_index = pressed_key
            keyboard_mapper.apply()
