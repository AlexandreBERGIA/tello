import logging
from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.AbstractCommandMapper import AbstractCommandMapper


class DroneLowBattery(AbstractCommandMapper, ABC):
    """
    Lands the drone automatically on low battery
    """
    _low_battery_level = 5

    def __init__(self, drone: AbstractDrone):
        super(DroneLowBattery, self).__init__()

        self._drone = drone

    def _apply(self) -> None:
        """
        Lands the drone
        """
        self._drone.land()

    def is_apply_allowed(self) -> bool:
        """
        Allow applying the command when the battery reaches a certain level
        """
        is_critical = self._drone.get_is_battery_level_critical()
        is_warning = self._drone.get_is_battery_level_warning()
        if is_critical:
            logging.warning('Drone Battery Critical')
        elif is_warning:
            logging.warning('Drone Battery Warning')
        return is_critical
