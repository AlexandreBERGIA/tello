from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractKeyboardMapper import AbstractKeyboardMapper


class DroneRotateRight(AbstractKeyboardMapper, ABC):
    """
    Make the drone rotate right
    """

    def __init__(self, mapper_key_index: int, drone: AbstractDrone):
        super(DroneRotateRight, self).__init__(mapper_key_index)
        self._drone = drone

    def _apply(self) -> None:
        """
        Make the drone rotate right
        """
        self._drone.send_rc_controls(0, 0, 0, 100)
