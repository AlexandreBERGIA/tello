from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractKeyboardMapper import AbstractKeyboardMapper


class DroneMoveDown(AbstractKeyboardMapper, ABC):
    """
    Make the drone move down
    """

    def __init__(self, mapper_key_index: int, drone: AbstractDrone):
        super(DroneMoveDown, self).__init__(mapper_key_index)
        self._drone = drone

    def _apply(self) -> None:
        """
        Make the drone move down
        """
        self._drone.send_rc_controls(0, 0, -100, 0)
