from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractToggleKeyboardMapper import AbstractToggleKeyboardMapper


class DroneLandTakeoff(AbstractToggleKeyboardMapper, ABC):
    """
    Make the drone land or takeoff depending on the flight status
    """

    def __init__(self, mapper_key_index: int, drone: AbstractDrone):
        super(DroneLandTakeoff, self).__init__(mapper_key_index)
        self._drone = drone

    def _apply(self) -> None:
        """
        Land or takeoff the drone
        """
        if self._drone.get_is_flying():
            self._drone.land()
        else:
            self._drone.takeoff()
