from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractKeyboardMapper import AbstractKeyboardMapper


class DroneMoveBackward(AbstractKeyboardMapper, ABC):
    """
    Make the drone move backward
    """

    def __init__(self, mapper_key_index: int, drone: AbstractDrone):
        super(DroneMoveBackward, self).__init__(mapper_key_index)
        self._drone = drone

    def _apply(self) -> None:
        """
        Make the drone move backward
        """
        self._drone.send_rc_controls(0, -100, 0, 0)
