from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractKeyboardMapper import AbstractKeyboardMapper


class ExitProgram(AbstractKeyboardMapper, ABC):
    """
    Allow exiting the program
    """

    def __init__(self, mapper_key_index: int, drone: AbstractDrone):
        super(ExitProgram, self).__init__(mapper_key_index)
        self._drone = drone

    def _apply(self) -> None:
        """
        Nothing is really done here since use the is_apply_allowed() method to determine when we exit the program
        """
        pass
