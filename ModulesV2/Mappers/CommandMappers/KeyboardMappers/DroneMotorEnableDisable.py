from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractToggleKeyboardMapper import AbstractToggleKeyboardMapper


class DroneMotorEnableDisable(AbstractToggleKeyboardMapper, ABC):
    """
    Make the drone motors enabled or disabled
    """

    def __init__(self, mapper_key_index: int, drone: AbstractDrone):
        super(DroneMotorEnableDisable, self).__init__(mapper_key_index)
        self._drone = drone

    def _apply(self) -> None:
        """
        Make the drone motors enabled or disabled
        """
        self._drone.set_is_motor_enabled(not self._drone.get_is_motor_enabled())
