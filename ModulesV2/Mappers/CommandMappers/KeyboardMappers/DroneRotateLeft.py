from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractKeyboardMapper import AbstractKeyboardMapper


class DroneRotateLeft(AbstractKeyboardMapper, ABC):
    """
    Make the drone rotate left
    """

    def __init__(self, mapper_key_index: int, drone: AbstractDrone):
        super(DroneRotateLeft, self).__init__(mapper_key_index)
        self._drone = drone

    def _apply(self) -> None:
        """
        Make the drone rotate left
        """
        self._drone.send_rc_controls(0, 0, 0, -100)
