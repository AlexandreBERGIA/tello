from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractKeyboardMapper import AbstractKeyboardMapper


class DroneMoveLeft(AbstractKeyboardMapper, ABC):
    """
    Make the drone move right
    """

    def __init__(self, mapper_key_index: int, drone: AbstractDrone):
        super(DroneMoveLeft, self).__init__(mapper_key_index)
        self._drone = drone

    def _apply(self) -> None:
        """
        Make the drone move right
        """
        self._drone.send_rc_controls(100, 0, 0, 0)
