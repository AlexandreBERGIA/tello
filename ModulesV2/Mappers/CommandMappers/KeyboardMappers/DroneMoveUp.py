from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractKeyboardMapper import AbstractKeyboardMapper


class DroneMoveUp(AbstractKeyboardMapper, ABC):
    """
    Make the drone move up
    """

    def __init__(self, mapper_key_index: int, drone: AbstractDrone):
        super(DroneMoveUp, self).__init__(mapper_key_index)
        self._drone = drone

    def _apply(self) -> None:
        """
        Make the drone move up
        """
        self._drone.send_rc_controls(0, 0, 100, 0)
