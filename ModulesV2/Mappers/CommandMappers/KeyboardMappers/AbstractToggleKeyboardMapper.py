import time
from abc import ABC

from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractKeyboardMapper import AbstractKeyboardMapper


class AbstractToggleKeyboardMapper(AbstractKeyboardMapper, ABC):
    """
    An individual keyboard mapper that defines what do to when a certain key is pressed.
    It triggers the action only once, even though the key remain pressed.
    It also triggers the action only once, even
    """

    def __init__(self, mapper_key_index: int) -> None:
        super().__init__(mapper_key_index)

        self.toggle_time: int = 430  # The toggle sticky time in ms

        self._pressed_time = 0

    def is_apply_allowed(self) -> bool:
        """
        Returns True only when the corresponding key is pressed after the toggle time passed
        """
        # Determines whether we correct key is pressed and whether we are inside or outside the toggle time
        is_key_match = super().is_apply_allowed()
        is_inside_toggle_time = self._pressed_time > 0 and time.time() - self._pressed_time < self.toggle_time / 1000

        # When there is a match, we reset the pressed_time
        if is_key_match:
            self._pressed_time = time.time()

        # When inside the toggle, we return False, otherwise return the is_key_match value
        if is_inside_toggle_time:
            return False
        else:
            return is_key_match
