from abc import ABC

from ModulesV2.Converter.ImageToHandsConverter import ImageToHandsConverter
from ModulesV2.Mappers.CommandMappers.KeyboardMappers.AbstractToggleKeyboardMapper import AbstractToggleKeyboardMapper


class HandsDetectOnOff(AbstractToggleKeyboardMapper, ABC):
    """
    Enable or disable the Hands detection
    """

    def __init__(self, mapper_key_index: int, image_to_hands_converter: ImageToHandsConverter):
        super(HandsDetectOnOff, self).__init__(mapper_key_index)
        self._image_to_hands_converter = image_to_hands_converter

    def _apply(self) -> None:
        """
        Enable or disable the Hands detection
        """
        new_value = not self._image_to_hands_converter.is_detect_hands
        self._image_to_hands_converter.is_detect_hands = new_value
