from abc import ABC

from ModulesV2.Mappers.CommandMappers.AbstractCommandMapper import AbstractCommandMapper

KEYBOARD_1 = 49
KEYBOARD_2 = 50
KEYBOARD_3 = 51
KEYBOARD_4 = 52
KEYBOARD_A = 97
KEYBOARD_D = 100
KEYBOARD_Q = 113
KEYBOARD_S = 115
KEYBOARD_W = 119
KEYBOARD_SPACE = 32
KEYBOARD_ARROW_DOWN = 1
KEYBOARD_ARROW_UP = 0
KEYBOARD_ARROW_LEFT = 2
KEYBOARD_ARROW_RIGHT = 3
KEYBOARD_NOTHING = 255


class AbstractKeyboardMapper(AbstractCommandMapper, ABC):
    """
    An individual keyboard mapper that defines what do to when a certain key is pressed
    """

    def __init__(self, mapper_key_index: int) -> None:
        self._mapper_key_index = mapper_key_index  # What the mapper is listening to
        self._pressed_key_index = -1  # The key that is currently pressed

    def is_apply_allowed(self) -> bool:
        """
        Returns True only when the corresponding key is pressed
        """
        return self._pressed_key_index == self._mapper_key_index

    @property
    def pressed_key_index(self) -> int:
        """
        Returns the currently pressed key index
        """
        return self._pressed_key_index

    @pressed_key_index.setter
    def pressed_key_index(self, pressed_key_index: int) -> None:
        """
        Sets the currently pressed key index
        """
        self._pressed_key_index = pressed_key_index

    @property
    def mapper_key_index(self) -> chr(1):
        """
        Returns the key index the mapper listens to
        """
        return self._mapper_key_index

    @mapper_key_index.setter
    def mapper_key_index(self, mapper_key_index: int) -> None:
        """
        Sets the key index the mapper listens to
        """
        self._mapper_key_index = mapper_key_index
