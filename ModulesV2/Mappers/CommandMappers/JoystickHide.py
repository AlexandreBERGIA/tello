from abc import ABC

from ModulesV2.Hands.HandsController import HandsController
from ModulesV2.Mappers.CommandMappers.AbstractGestureToCommandMapper import AbstractGestureToCommandMapper
from ModulesV2.Mappers.HandsToDualJoystickMapper import HandsToDualJoystickMapper


class JoystickHide(AbstractGestureToCommandMapper, ABC):
    """
    In charge of showing and setting the Joystick centers
    """

    def __init__(self, gesture_name: str, pattern_name: str, hands_to_joystick_mapper: HandsToDualJoystickMapper,
                 hands_controller: HandsController):
        super().__init__(gesture_name, pattern_name, hands_controller)
        self._hands_to_joystick_mapper = hands_to_joystick_mapper

    def is_apply_allowed(self) -> bool:
        is_allowed = super().is_apply_allowed()
        if not is_allowed:
            return is_allowed

        return self._hands_to_joystick_mapper.get_joysticks_is_visible()

    def _apply(self) -> None:
        self._hands_to_joystick_mapper.set_joysticks_is_visible(False)
        self._hands_to_joystick_mapper.assign_joysticks_values()
