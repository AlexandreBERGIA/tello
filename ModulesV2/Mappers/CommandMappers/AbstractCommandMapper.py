from abc import abstractmethod


class AbstractCommandMapper:
    """
    Maps an input and allow applying it
    """

    def apply(self) -> None:
        """
        Applies the input
        """
        if self.is_apply_allowed():
            self._apply()

    @abstractmethod
    def is_apply_allowed(self) -> bool:
        """
        The expected implementation of the method that returns True when the _apply method can be called
        """
        return False

    @abstractmethod
    def _apply(self) -> None:
        """
        The expected implementation of the apply method from the child class
        """
        pass
