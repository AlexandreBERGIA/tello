from ModulesV2.Mappers.CommandMappers.AbstractCommandMapper import AbstractCommandMapper


class CommandMapperController:
    """
    Get all the inputs and map them to the different objects requiring inputs
    """
    def __init__(self, _gesture_to_input_mapper_list: [AbstractCommandMapper]):
        self._gesture_to_input_mapper_list = _gesture_to_input_mapper_list

    def apply_inputs(self) -> None:
        """
        Sets the different inputs
        """
        for input_mapper in self._gesture_to_input_mapper_list:
            input_mapper.apply()
