from abc import ABC

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Hands.HandsController import HandsController
from ModulesV2.Mappers.CommandMappers.AbstractGestureToCommandMapper import AbstractGestureToCommandMapper


class DroneMotorDisable(AbstractGestureToCommandMapper, ABC):
    """
    Makes the drone motor disabled
    """

    def __init__(self, gesture_name: str, pattern_name: str, hands_controller: HandsController, drone: AbstractDrone):
        super().__init__(gesture_name, pattern_name, hands_controller)
        self._drone = drone

    def _apply(self) -> None:
        self._drone.set_is_motor_enabled(False)
