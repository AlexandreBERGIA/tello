from abc import abstractmethod

import cv2
from numpy import ndarray

from ModulesV2.Geometry.Point2D import Point2D


class AbstractImageFetcher:

    def __init__(self, image_size: Point2D):
        self._image_size = image_size
        self._image = None

    def fetch_image(self) -> None:
        """
        Fetch a new image and set it for future use of get_image()
        """
        image = self._fetch()
        image = self._post_process(image)
        self._image = self._resize(image)

    def get_image(self) -> ndarray:
        """
        Returns an image that was previously fetched
        """
        return self._image

    @abstractmethod
    def _fetch(self) -> ndarray:
        """
        Fetch a new image from the drone
        """
        pass

    def _post_process(self, image: ndarray) -> ndarray:
        """
        Do the post-processing of the image
        """
        return image

    def _resize(self, image: ndarray) -> ndarray:
        """
        Resizes the image to the desired resolution
        """
        if image is None:
            return image
        return cv2.resize(image, self._image_size.point_int)

    @property
    def image_size(self) -> Point2D:
        """
        Return the image size
        """
        return self._image_size
