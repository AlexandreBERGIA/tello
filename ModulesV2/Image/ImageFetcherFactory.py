from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Drones.StubDrone import StubDrone
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher
from ModulesV2.Image.BlackImageFetcher import BlackImageFetcher
from ModulesV2.Image.CameraImageFetcher import CameraImageFetcher
from ModulesV2.Image.DJITelloDroneImageFetcher import DJITelloDroneImageFetcher
from ModulesV2.Image.LoadingImageFetcher import LoadingImageFetcher

FETCHER_STUB_BLACK = 'stub.black'
FETCHER_STUB_LOADING = 'stub.loading'
FETCHER_CAMERA_COMPUTER = 'camera.computer'
FETCHER_CAMERA_DRONE = 'camera.drone'


class ImageFetcherFactory:
    """
    Creates a new image fetcher for a drone
    """

    def __init__(self):
        self._drone = None
        self._working_image_size = Point2D(1000, 800)
        self._camera_index = -1
        self._thread_index = 0

    def create(self, fetcher_type: str) -> AbstractImageFetcher:
        """
        Creates a new image fetcher for a drone
        """
        if fetcher_type == FETCHER_CAMERA_DRONE and isinstance(self._drone, StubDrone):
            # When we use the StubDrone
            return BlackImageFetcher(self._working_image_size)
        elif fetcher_type == FETCHER_STUB_BLACK:
            return BlackImageFetcher(self._working_image_size)
        elif fetcher_type == FETCHER_CAMERA_DRONE:
            return DJITelloDroneImageFetcher(self._working_image_size, self.get_thread_name(), self._drone)
        elif fetcher_type == FETCHER_CAMERA_COMPUTER:
            return CameraImageFetcher(self._working_image_size, self.get_thread_name(), self._camera_index)
        elif fetcher_type == FETCHER_STUB_LOADING:
            return LoadingImageFetcher(self._working_image_size)
        else:
            raise ValueError("The provided ImageFetcher type is not supported: {}".format(fetcher_type))

    @property
    def camera_index(self) -> int:
        return self._camera_index

    @camera_index.setter
    def camera_index(self, camera_index: int) -> None:
        self._camera_index = camera_index

    @property
    def drone(self) -> AbstractDrone:
        return self._drone

    @drone.setter
    def drone(self, drone: AbstractDrone) -> None:
        self._drone = drone

    def get_thread_name(self) -> str:
        name = 'Image Capture {}'.format(self._thread_index)
        self._thread_index += 1
        return name
