from abc import ABC

import numpy as np
from numpy import ndarray

from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher


class BlackImageFetcher(AbstractImageFetcher, ABC):
    def __init__(self, image_size):
        super(BlackImageFetcher, self).__init__(image_size)

    def _fetch(self) -> ndarray:
        return np.zeros([self._image_size.x, self._image_size.y, 3], dtype=np.uint8)
