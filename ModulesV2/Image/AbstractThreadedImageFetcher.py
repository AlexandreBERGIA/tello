import threading
from abc import abstractmethod
from queue import Queue

from numpy import ndarray

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher
from ModulesV2.Loggers import Logger


class AbstractThreadedImageFetcher(AbstractImageFetcher):

    def __init__(self, image_size: Point2D, capture_thread_name: str):
        super(AbstractThreadedImageFetcher, self).__init__(image_size)
        self._image_queue = Queue(1)
        self._thread = threading.Thread(target=self._image_capture_thread, name=capture_thread_name)
        self._thread.daemon = True

    def fetch_image(self) -> None:
        """
        Fetch a new image from the queue, to get the image, use get_image()
        """
        # Start the capture thread if it is not alive
        if not self._thread.isAlive():
            self._thread.start()

        # Keep the previous image if the queue is empty
        if self._image_queue.empty():
            return

        self._image = self._image_queue.get()

    @abstractmethod
    def _fetch(self) -> ndarray:
        """
        Fetch a new image from the drone
        """
        pass

    def _image_capture_thread(self) -> None:
        """
        Capture images and places them into the image queue
        """
        Logger.main_logger.info(type(self).__name__ + ',Start Capturing')
        while True:
            image = self._fetch()
            image = self._post_process(image)
            image = self._resize(image)
            self._image_queue.put(image)
