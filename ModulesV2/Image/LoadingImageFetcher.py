from abc import ABC

import cv2
from numpy import ndarray

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image.BlackImageFetcher import BlackImageFetcher


class LoadingImageFetcher(BlackImageFetcher, ABC):
    def __init__(self, image_size):
        super(LoadingImageFetcher, self).__init__(image_size)
        self._text = 'Loading ...'
        self._text_size = 3
        self._text_size_coeff = 12

    def _fetch(self) -> ndarray:
        # Get the black image from the parent class and add a text
        image = super(LoadingImageFetcher, self)._fetch()
        text_position = self.image_size / Point2D(2, 2)
        text_position.x -= len(self._text) * self._text_size * self._text_size_coeff
        cv2.putText(image, self._text, text_position.point_int, cv2.FONT_HERSHEY_SIMPLEX, self._text_size,
                    (255, 255, 255))
        return image
