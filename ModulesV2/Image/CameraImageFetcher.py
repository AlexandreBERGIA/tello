from abc import ABC

import cv2
from numpy import ndarray

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image.AbstractThreadedImageFetcher import AbstractThreadedImageFetcher


class CameraImageFetcher(AbstractThreadedImageFetcher, ABC):
    def __init__(self, image_size: Point2D, capture_thread_name: str, camera_index: int = 0):
        super(CameraImageFetcher, self).__init__(image_size, capture_thread_name)

        self._camera_index = camera_index
        self._camera_capture = cv2.VideoCapture(self._camera_index)

    def _fetch(self) -> ndarray:
        """
        Fetches the image for the webcam at the provided index
        """
        _, image = self._camera_capture.read()
        return image

    def _post_process(self, image: ndarray) -> ndarray:
        """
        Flip the image so it acts like a mirror
        """
        return cv2.flip(image, 1)
