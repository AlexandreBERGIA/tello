from abc import ABC

from numpy import ndarray

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image.AbstractThreadedImageFetcher import AbstractThreadedImageFetcher


class DJITelloDroneImageFetcher(AbstractThreadedImageFetcher, ABC):
    def __init__(self, image_size: Point2D, capture_thread_name: str, drone: AbstractDrone):
        super(DJITelloDroneImageFetcher, self).__init__(image_size, capture_thread_name)

        self._drone = drone
        self._drone.streamon()

    def _fetch(self) -> ndarray:
        return self._drone.get_drone_source().get_frame_read().frame
