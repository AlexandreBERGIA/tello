def _is_pattern_value_valid(value: int) -> bool:
    """
    Returns True when the pattern value is either -1. 0. or 1
    """
    return 1 >= value >= -1


def _validate_finger_pattern_value(value: int) -> None:
    """
    Raise an exception if the pattern value is invalid
    """
    if not _is_pattern_value_valid(value):
        raise "The Hand pattern value must be -1, 0, or 1"


def _validate_hand_pattern_values(values: [int]) -> None:
    """
    Validates all the provided pattern values and raise an exception if the value is invalid
    """
    for value in values:
        _validate_finger_pattern_value(value)


def _matches_finger(finger_pattern: int, provided_finger: int) -> bool:
    """
    Returns True if
    - The finger pattern = -1
    - The finger pattern = the provided finger pattern
    """
    return finger_pattern == -1 or finger_pattern == provided_finger


class HandPattern:
    """
    Manage the hand patterns for one hand
    """
    def __init__(self, hand_pattern: [int]):
        self._hand_size = 5
        self._is_match = False

        if len(hand_pattern) != self._hand_size:
            raise "Hand Patterns must contain 5 fingers"

        _validate_hand_pattern_values(hand_pattern)
        self._hand_pattern = hand_pattern

    def fetch_match(self, hand_pattern: [int]) -> None:
        """
        Fetch the pattern match
        """
        self.reset_match()

        if len(hand_pattern) != len(self._hand_pattern):
            return

        for finger_index in range(self._hand_size):
            match = _matches_finger(self._hand_pattern[finger_index], hand_pattern[finger_index])
            if not match:
                return

        self._is_match = True

    def get_fingers_pattern(self) -> [int]:
        """
        Return the list of finger patters
        """
        return self._hand_pattern

    def is_match(self) -> bool:
        """
        Returns True when the provided pattern matches the Hand Pattern
        """
        return self._is_match

    def reset_match(self) -> None:
        """
        Set the match value to False
        """
        self._is_match = False
