from ModulesV2.Converter.HandsToFingersUpConverter import HandsToFingersUpConverter
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Hands.HandGesture import HandGesture
from ModulesV2.Hands.HandsGestureController import HandsGestureController


class HandsController:
    """
    A controller class that encapsulates the Hand detector.
    """

    def __init__(
            self,
            hands_to_fingers_converter: HandsToFingersUpConverter,
            gesture_controller: HandsGestureController
    ):
        self._hands = []
        self._fingers_up = []

        self._hands_to_fingers_converter = hands_to_fingers_converter
        self._gesture_controller = gesture_controller

    def get_hands(self) -> list:
        """
        Returns the hands
        """
        return self._hands

    def get_hands_centers(self) -> [Point2D]:
        """
        Returns the list of hand centers that are at the index [9]
        """
        centers = []
        for hand in self._hands:
            centers.append(Point2D(hand['lmList'][9][0], hand['lmList'][9][1]))

        return centers

    def get_hands_size(self, hand_index: int) -> Point2D:
        """
        Returns the size of the hand at the provided index
        """
        return Point2D(self._hands[hand_index]['bbox'][2], self._hands[hand_index]['bbox'][3])

    def get_fingers_up(self) -> list:
        """
        Returns the fingers up
        """
        return self._fingers_up

    def set_hands(self, hands: []) -> None:
        """
        Sets the hands information, updates the finger up information, and update the gestures
        """
        self._hands = hands
        self._hands_to_fingers_converter.set_hands(self._hands)
        self._fingers_up = self._hands_to_fingers_converter.convert()
        self._gesture_controller.set_fingers_up(self._fingers_up)

    def get_gestures(self) -> [HandGesture]:
        """
        Returns the gesture list corresponding to the current hand information
        """
        return self._gesture_controller.get_gestures()
