from typing import Dict

from ModulesV2.Converter.FingersUpToGestureConverter import FingersUpToGestureConverter
from ModulesV2.Hands.HandGesture import HandGesture
from ModulesV2.Hands.HandPattern import HandPattern

# Declare the preflight checks
pattern_preflight_switch = HandPattern([0, 1, 1, 0, 0])
pattern_preflight_joystick_show = HandPattern([1, 0, 0, 0, 0])
pattern_preflight_motor_enable = HandPattern([1, 1, 0, 0, 0])
pattern_preflight_drone_takeoff = HandPattern([1, 1, 1, 0, 0])
pattern_preflight_list = {
    'joystick_show': pattern_preflight_joystick_show,
    'drone_motor_enable': pattern_preflight_motor_enable,
    'drone_takeoff': pattern_preflight_drone_takeoff,
}
gesture_preflight = HandGesture(pattern_preflight_switch, pattern_preflight_list)

# Declare the Joystick gestures
pattern_joystick_switch = HandPattern([1, 0, 0, 0, 0])
pattern_joystick_show = HandPattern([1, 0, 0, 0, 0])
pattern_joystick_hide = HandPattern([0, 0, 0, 0, 1])
pattern_joystick_list = {
    'joystick_show': pattern_joystick_show,
    'joystick_hide': pattern_joystick_hide
}
gesture_joystick = HandGesture(pattern_joystick_switch, pattern_joystick_list)

# Declare the Drone Flight controls
pattern_drone_switch = HandPattern([0, 1, 0, 0, 1])
pattern_motor_enable = HandPattern([0, 1, 1, 0, 0])
pattern_motor_disable = HandPattern([0, 1, 1, 1, 0])
pattern_drone_takeoff = HandPattern([1, 0, 0, 0, 0])
pattern_drone_land = HandPattern([0, 0, 0, 0, 1])
pattern_drone_list = {
    'drone_motor_enable': pattern_motor_enable,
    'drone_motor_disable': pattern_motor_disable,
    'drone_takeoff': pattern_drone_takeoff,
    'drone_land': pattern_drone_land,
}
gesture_drone = HandGesture(pattern_drone_switch, pattern_drone_list)


class HandsGestureController:
    """
    A controller that has all the gestures corresponding to the provided finger up information
    """

    def __init__(self, converter: FingersUpToGestureConverter):
        self._fingers_up = []
        self._gestures = {
            'preflight': gesture_preflight,
            'joystick': gesture_joystick,
            'drone': gesture_drone,
        }

        self._converter = converter

    def set_fingers_up(self, fingers_up: []) -> None:
        """
        Sets the fingers up information in order to update the gestures
        """
        self._fingers_up = fingers_up
        self._converter.set_gestures(self._gestures)
        self._converter.set_fingers_up(self._fingers_up)
        self._gestures = self._converter.convert()

    def get_gestures(self) -> Dict[str, HandGesture]:
        """
        Returns the list of hand gestures associated with the provided fingers up information
        """
        return self._gestures
