import logging
from typing import Dict

from ModulesV2.Hands.HandPattern import HandPattern


class HandGesture:
    def __init__(self, left_hand: HandPattern, right_hand: Dict[str, HandPattern]):
        self._left_hand = left_hand
        self._right_hand = right_hand

    def fetch_match(self, provided_left_hand_pattern: HandPattern, provided_right_hand_pattern: HandPattern) -> None:
        self._left_hand.fetch_match(provided_left_hand_pattern)

        # If there is no match on the left hand, all the right hand values are reset
        for right_hand in self._right_hand.values():
            if not self._left_hand.is_match():
                right_hand.reset_match()
            else:
                right_hand.fetch_match(provided_right_hand_pattern)

    def get_left_hand_pattern(self) -> HandPattern:
        """
        Returns the Left Hand HandPatter
        """
        return self._left_hand

    def get_right_hand_pattern_list(self) -> Dict[str, HandPattern]:
        """
        Returns the list of HandPattern from the right hand
        """
        return self._right_hand

    def is_match(self, pattern_name: str) -> bool:
        """
        Returns True when the gesture is match with the provided pattern during the fetch
        """
        # If the left hands is not a match, return false
        if not self._left_hand.is_match():
            return False

        # Return the right hand match if available
        try:
            return self._right_hand[pattern_name].is_match()
        except KeyError:
            logging.warning('The right hand pattern doesn\'t exist: {}'.format(pattern_name))

        return False

    def reset_match(self) -> None:
        """
        Resets the match information
        """
        self._left_hand.reset_match()
        for right_hand in self._right_hand.values():
            right_hand.reset_match()
