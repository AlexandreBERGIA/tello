import logging
import time


class Chronometer:
    """
    Simple Chronometer that allows starting counting the time and stop. Upon stopping, it returns the elapsed time in ms
    """

    def __init__(self):
        self._start_time = 0
        self._stop_time = 0
        self._saved_elapsed_time = 0
        self._is_started = False

    def get_elapsed_time(self) -> int:
        """
        Returns the elapsed time in ms.
        When the Chronometer is not started, returns 0
        When the Chronometer is running, returns the elapsed time since the start
        When the Chronometer is stopped, returns the elapsed time between the start and the stop
        """
        if self._start_time == 0:  # The Chronometer is not started
            return 0
        elif self._stop_time == 0:  # The Chronometer is still running
            return int((time.time() - self._start_time) * 1000)
        else:  # The Chronometer was stopped
            return int((self._stop_time - self._start_time) * 1000)

    def get_saved_elapsed_time(self) -> int:
        """
        Returns the saved elapsed time, or 0 if it was not set
        """
        return self._saved_elapsed_time

    @property
    def is_started(self) -> bool:
        """
        Returns True when the Chronometer is started, else False
        """
        return self._is_started

    def save_elapsed_time(self) -> None:
        """
        Saves the elapsed time
        """
        if not self._is_started:
            logging.warning('The Chronometer was not started')
        self._saved_elapsed_time = self.get_elapsed_time()

    def start(self) -> None:
        """
        Starts the chronometer
        """
        self._is_started = True
        self._start_time = time.time()
        self._stop_time = 0

    def stop(self) -> None:
        """
        Stops the chronometer and save the stop time only if it was already started
        """
        if self._is_started:
            self._stop_time = time.time()

        self._is_started = False
