from abc import ABC

from djitellopy import tello as Drone

from ModulesV2.Drones.AbstractDrone import AbstractDrone


class DJITelloDrone(AbstractDrone, ABC):
    """
    The object that represents the DJI Tello drone, its properties and all methods
    """

    def __init__(self, drone: Drone):
        super().__init__(drone)

        self._max_barometer_altitude = 1000.0  # Overrides the max barometer altitude
        self._max_ground_altitude = 10.0  # Overrides the max ground altitude
        self._camera_pitch_angle = -5  # The camera has a natural pitch angle pointing downward

    def _connect(self) -> None:
        """
        Connects to the drone
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.connect
        """
        self._drone.connect()

    def _emergency(self) -> None:
        """
        Stops all motors
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.emergency
        """
        self._drone.emergency()

    def _end(self) -> None:
        """
        Terminates the drone object
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.end
        """
        self._drone.end()

    def _get_barometer_altitude(self) -> float:
        """
        Returns the barometer height in m
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_barometer
        """
        return self._drone.get_barometer()

    def _get_battery(self) -> int:
        """
        Returns the battery state between 0 and 100
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_battery
        """
        return self._drone.get_battery()

    def _get_current_state(self) -> dict:
        """
        Returns a dictionary of all the drone states
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_current_state
        """
        return self._drone.get_current_state()

    def _get_flight_time(self) -> int:
        """
        Returns the flight time in s
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_flight_time
        """
        return self._drone.get_flight_time()

    def _get_ground_altitude(self) -> float:
        """
        Returns the drone height in m
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_height
        """
        return self._drone.get_height() / 100

    def _get_heading(self) -> float:
        return 0

    def _get_is_flying(self) -> bool:
        """
        Returns the is_flying state of the drone
        """
        return self._drone.is_flying

    def _get_pitch(self) -> int:
        """
        Returns the drone pitch in degree
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_pitch
        """
        return self._drone.get_pitch()

    def _get_roll(self) -> int:
        """
        Returns the drone roll in degree
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_roll
        """
        return self._drone.get_roll()

    def _get_speed_x(self) -> int:
        """
        Returns the drone x speed
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_speed_x
        """
        return self._drone.get_speed_x()

    def _get_speed_y(self) -> int:
        """
        Returns the drone y speed
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_speed_y
        """
        return self._drone.get_speed_y()

    def _get_speed_z(self) -> int:
        """
        Returns the drone z speed
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_speed_z
        """
        return self._drone.get_speed_z()

    def _get_temperature(self) -> int:
        """
        Returns the drone temperature in C
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_temperature
        """
        return self._drone.get_temperature()

    def _get_yaw(self) -> int:
        """
        Returns the drone yaw
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.get_yaw
        """
        return self._drone.get_yaw()

    def _land(self) -> None:
        """
        Lands the drone
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.land
        """
        self._drone.land()

    def _send_rc_controls(
            self,
            left_right_velocity: int,
            forward_backward_velocity: int,
            up_down_velocity: int,
            yaw_velocity: int
    ) -> None:
        """
        Sends RC Controls to the drone
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.send_rc_control
        """
        self._drone.send_rc_control(left_right_velocity, forward_backward_velocity, up_down_velocity, yaw_velocity)

    def _streamoff(self) -> None:
        """
        Disable the image stream
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.streamoff
        """
        self._drone.streamoff()

    def _streamon(self) -> None:
        """
        Enables the image stream for later image capture
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.streamon
        """
        self._drone.streamon()

    def _takeoff(self) -> None:
        """
        Makes the drone take off
        https://djitellopy.readthedocs.io/en/latest/tello/#djitellopy.tello.Tello.takeoff
        """
        self._drone.takeoff()

    def _turn_motor_off(self) -> None:
        """
        Turn the drone motors off (Cooling mode off). This functionality is not supported in the current drone.
        """
        pass

    def _turn_motor_on(self) -> None:
        """
        Turn the drone motors on (Cooling mode on). This functionality is not supported in the current drone.
        """
        pass
