import logging
import math
from abc import abstractmethod
from typing import Dict, List

from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image.AbstractImageFetcher import AbstractImageFetcher
from ModulesV2.Loggers import Logger
from ModulesV2.Time.Chronometer import Chronometer

CAPTURED_DATA_GROUND_ALTITUDE = 'ground_altitude'  # The ground altitude entry for the captured data


class AbstractDrone:
    """
    The object that represents a generic drone, its properties and all methods
    """

    def __init__(self, drone):
        self._drone = drone

        self._max_barometer_altitude = 10000.0
        self._max_ground_altitude = 1000
        self._battery_level_critical = 5
        self._battery_level_warning = 10
        self._camera_pitch_angle = 0
        self._is_image_enabled = False
        self._is_motor_enabled = False

        # Define the public properties
        self.image_fetcher: AbstractImageFetcher = None

        # Define the captured data
        self._captured_data = {
            CAPTURED_DATA_GROUND_ALTITUDE: [],
        }

    def capture_data(self, chronometer: Chronometer):
        """
        Capture drone data with a time
        """
        self._captured_data[CAPTURED_DATA_GROUND_ALTITUDE].append(
            Point2D(chronometer.get_elapsed_time(), self.get_ground_altitude()))

    def connect(self) -> None:
        """
        Connects to the drone
        """
        Logger.main_logger.info('Drone Connect')
        self._connect()

    def emergency(self) -> None:
        """
        Stops all motors
        """
        logging.warning('Drone Emergency')
        self.set_is_motor_enabled(False)
        self._emergency()

    def end(self) -> None:
        """
        Terminates the drone object
        """
        Logger.main_logger.info('Drone End')
        self._end()

    def get_barometer_altitude(self) -> float:
        """
        Returns the barometer height in m
        """
        value = self._get_barometer_altitude()
        if value is None or math.isnan(value):
            logging.warning('Invalid Barometer Altitude value: {}'.format(value))
            value = 0
        return value

    def get_battery(self) -> int:
        """
        Returns the battery state between 0 and 100
        """
        value = self._get_battery()
        if value is None or math.isnan(value):
            logging.warning('Invalid Battery value: {}'.format(value))
            value = 0
        return value

    def get_camera_pitch_angle(self) -> float:
        """
        Returns the pitch angle of the camera in deg
        """
        value = self._camera_pitch_angle
        if value is None or math.isnan(value):
            logging.warning('Invalid Camera Pitch Angle value: {}'.format(value))
            value = 0
        return value

    def get_captured_data(self) -> Dict[str, List[Point2D]]:
        """
        Returns the captured data
        """
        return self._captured_data

    def get_current_state(self) -> dict:
        """
        Returns a dictionary of all the drone states
        """
        return self._get_current_state()

    def get_drone_source(self):
        """
        Returns the internal drone object
        """
        return self._drone

    def get_flight_time(self) -> int:
        """
        Returns the flight time in s
        """
        value = self._get_flight_time()
        if value is None or math.isnan(value):
            logging.warning('Invalid Flight Time value: {}'.format(value))
            value = 0
        return value

    def get_ground_altitude(self) -> float:
        """
        Returns the drone height in m
        """
        value = self._get_ground_altitude()
        if value is None or math.isnan(value):
            logging.warning('Invalid Ground Altitude value: {}'.format(value))
            value = 0
        return value

    def get_heading(self) -> float:
        """
        Returns the heading of the drone in deg. From 0 to 360
        """
        value = self._get_heading()
        if value is None or math.isnan(value):
            logging.warning('Invalid Heading value: {}'.format(value))
            value = 0
        return value

    # TODO Alex: Add the return type
    def get_image_frame(self):
        """
        Returns an image frame from the drone using the provided image fetcher
        """
        return self.image_fetcher.get_image()

    def get_is_battery_level_critical(self) -> bool:
        """
        Returns True when the battery reaches critical level
        """
        return self.get_battery() < self._battery_level_critical

    def get_is_battery_level_warning(self) -> bool:
        """
        Returns True when the battery reaches warning level
        """
        return self.get_battery() < self._battery_level_warning

    def get_is_flying(self) -> bool:
        """
        Returns the is_flying state of the drone
        """
        return self._get_is_flying()

    def get_is_motor_enabled(self) -> bool:
        """
        Returns True when the drone is allowed to fly
        """
        return self._is_motor_enabled

    def get_max_barometer_altitude(self) -> float:
        """
        Returns the maximum barometer altitude the drone can fly in m
        """
        return self._max_barometer_altitude

    def get_max_ground_altitude(self) -> float:
        """
        Returns the maximum ground altitude the drone can fly in m
        """
        return self._max_ground_altitude

    def get_pitch(self) -> int:
        """
        Returns the drone pitch in degree
        """
        value = self._get_pitch()
        if value is None or math.isnan(value):
            logging.warning('Invalid Drone Pitch value: {}'.format(value))
            value = 0
        return value

    def get_property_value(self, get_property: str):
        """
        Returns the provided property value as string
        """
        try:
            function = getattr(self, get_property)
            return function()
        except AttributeError:
            return 'Error...'

    def get_roll(self) -> int:
        """
        Returns the drone roll in degree
        """
        value = self._get_roll()
        if value is None or math.isnan(value):
            logging.warning('Invalid Drone Roll value: {}'.format(value))
            value = 0
        return value

    def get_speed_x(self) -> int:
        """
        Returns the drone x speed
        """
        value = self._get_speed_x()
        if value is None or math.isnan(value):
            logging.warning('Invalid Drone Speed X value: {}'.format(value))
            value = 0
        return value

    def get_speed_y(self) -> int:
        """
        Returns the drone y speed
        """
        value = self._get_speed_y()
        if value is None or math.isnan(value):
            logging.warning('Invalid Drone Speed Y value: {}'.format(value))
            value = 0
        return value

    def get_speed_z(self) -> int:
        """
        Returns the drone z speed
        """
        value = self._get_speed_z()
        if value is None or math.isnan(value):
            logging.warning('Invalid Drone Speed Z value: {}'.format(value))
            value = 0
        return value

    def get_temperature(self) -> int:
        """
        Returns the drone temperature in C
        """
        value = self._get_temperature()
        if value is None or math.isnan(value):
            logging.warning('Invalid Drone Temperature value: {}'.format(value))
            value = 0
        return value

    def get_yaw(self) -> int:
        """
        Returns the drone yaw
        """
        value = self._get_yaw()
        if value is None or math.isnan(value):
            logging.warning('Invalid Drone Yaw value: {}'.format(value))
            value = 0
        return value

    def land(self) -> None:
        """
        Lands the drone
        """
        if self.get_is_flying():
            Logger.main_logger.info('Drone Land')
            self._land()

    def send_rc_controls(self, left_right_velocity: int, forward_backward_velocity: int, up_down_velocity: int,
                         yaw_velocity: int) -> None:
        """
        Sends RC Controls to the drone
        """
        if self._is_motor_enabled and self.get_is_flying():
            Logger.main_logger.info(
                'Drone Send RC Controls lr:{0} fb:{1} ud:{2} yaw:{3}'.format(left_right_velocity,
                                                                             forward_backward_velocity,
                                                                             up_down_velocity, yaw_velocity))
            self._send_rc_controls(left_right_velocity, forward_backward_velocity, up_down_velocity, yaw_velocity)
        else:
            Logger.main_logger.info(
                'Drone Send RC Controls but Motor is {} and Is Flying is {}'.format(self._is_motor_enabled,
                                                                                    self.get_is_flying()))

    def set_is_motor_enabled(self, is_motor_enabled: bool) -> None:
        """
        Sets the motor enabled status that allows take off
        """
        # Lands the drone is flying, and the motor is disabled
        if not is_motor_enabled:
            self.land()
        if self._is_motor_enabled is not is_motor_enabled:
            Logger.main_logger.info('Drone motor enabled: {}'.format(is_motor_enabled))
            self._is_motor_enabled = is_motor_enabled

    def streamoff(self) -> None:
        """
        Disable the image stream
        """
        if self._is_image_enabled:
            Logger.main_logger.info('Drone Streamoff')
            self._streamoff()
            self._is_image_enabled = False

    def streamon(self) -> None:
        """
        Enables the image stream for later image capture
        """
        if not self._is_image_enabled:
            Logger.main_logger.info('Drone Streamon')
            self._streamon()
            self._is_image_enabled = True

    def takeoff(self) -> None:
        """
        Makes the drone take off
        """
        if self._is_motor_enabled and not self.get_is_flying():
            Logger.main_logger.info('Drone Takeoff')
            self._takeoff()

    def turn_motor_off(self) -> None:
        """
        Turn the drone motors off (Cooling mode Off)
        """
        Logger.main_logger.info('Drone Motor Off')
        self._turn_motor_off()

    def turn_motor_on(self) -> None:
        """
        Turn the drone motors on (Cooling mode on)
        """
        if self._is_motor_enabled:
            Logger.main_logger.info('Drone Motor On')
            self._turn_motor_on()

    @abstractmethod
    def _connect(self) -> None:
        pass

    @abstractmethod
    def _emergency(self) -> None:
        pass

    @abstractmethod
    def _end(self) -> None:
        pass

    @abstractmethod
    def _get_barometer_altitude(self):
        pass

    @abstractmethod
    def _get_battery(self) -> int:
        pass

    @abstractmethod
    def _get_current_state(self) -> dict:
        pass

    @abstractmethod
    def _get_flight_time(self) -> int:
        pass

    @abstractmethod
    def _get_ground_altitude(self) -> float:
        pass

    @abstractmethod
    def _get_is_flying(self) -> bool:
        pass

    @abstractmethod
    def _get_heading(self) -> float:
        pass

    @abstractmethod
    def _get_pitch(self) -> int:
        pass

    @abstractmethod
    def _get_roll(self) -> int:
        pass

    @abstractmethod
    def _get_speed_x(self) -> int:
        pass

    def _get_speed_y(self) -> int:
        pass

    def _get_speed_z(self) -> int:
        pass

    @abstractmethod
    def _get_temperature(self) -> int:
        pass

    @abstractmethod
    def _get_yaw(self) -> int:
        pass

    @abstractmethod
    def _land(self) -> None:
        pass

    @abstractmethod
    def _send_rc_controls(self, left_right_velocity: int, forward_backward_velocity: int, up_down_velocity: int,
                          yaw_velocity: int) -> None:
        pass

    @abstractmethod
    def _streamoff(self) -> None:
        pass

    @abstractmethod
    def _streamon(self) -> None:
        pass

    @abstractmethod
    def _takeoff(self) -> None:
        pass

    @abstractmethod
    def _turn_motor_off(self) -> None:
        pass

    @abstractmethod
    def _turn_motor_on(self) -> None:
        pass
