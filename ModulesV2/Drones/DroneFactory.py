import logging

from djitellopy import tello

from ModulesV2.Drones.AbstractDrone import AbstractDrone
from ModulesV2.Drones.DJITelloDrone import DJITelloDrone
from ModulesV2.Drones.StubDrone import StubDrone
from ModulesV2.Loggers import Logger

DJI_TELLO = 'dji.tello'
STUB = 'stub'


def create(drone_type: str) -> AbstractDrone:
    """
    Creates a drone Object that represents the drone to be controlled
    """
    if drone_type == DJI_TELLO:
        drone = tello.Tello(retry_count=1)
        drone.RESPONSE_TIMEOUT = 7
        drone.LOGGER.setLevel(logging.WARNING)
        logger_formatter = logging.Formatter('DRONE {}'.format(Logger.MESSAGE_FORMAT_TAB))
        drone.FORMATTER = logger_formatter
        drone.HANDLER.setFormatter(logger_formatter)
        return DJITelloDrone(drone)
    if drone_type == STUB:
        return StubDrone({})
    else:
        raise ValueError("The provided drone type is not supported: {}".format(drone_type))
