from abc import ABC

from djitellopy import tello as Drone

from ModulesV2.Drones.AbstractDrone import AbstractDrone


class StubDrone(AbstractDrone, ABC):
    """
    Represents a fake drone that returns actual flight data
    """

    def __init__(self, drone: Drone):
        super().__init__(drone)

        self._max_barometer_altitude = 1000.0  # Overrides the max barometer altitude
        self._max_ground_altitude = 10.0  # Overrides the max ground altitude
        self._camera_pitch_angle = -5  # The camera has a natural pitch angle pointing downward

        # Keeps flight data as internal variables
        self._initial_barometer_altitude = 1000
        self._ground_altitude = 0
        self._battery = 100
        self._battery_decrease = 0.01
        self._flight_time = 0
        self._flight_time_increase = 0.1
        self._attitude_pitch = 0
        self._attitude_roll = 0
        self._attitude_yaw = 0
        self._speed_x = 0
        self._speed_y = 0
        self._speed_z = 0
        self._temperature = 60
        self._attitude_pitch_rc_coeff = 0.007
        self._attitude_roll_rc_coeff = 0.007
        self._speed_z_rc_coeff = 0.005
        self._yaw_rc_coeff = 0.002
        self._is_flying = False
        self._motor_enabled = False
        self._heading = 0

    def _connect(self) -> None:
        pass

    def _emergency(self) -> None:
        self._is_flying = False
        self._motor_enabled = False

    def _end(self) -> None:
        self.land()
        self.streamoff()

    def _get_barometer_altitude(self) -> float:
        return self._initial_barometer_altitude + self._ground_altitude

    def _get_battery(self) -> int:
        if self._battery >= 0:
            self._battery -= self._battery_decrease
        return self._battery

    def _get_current_state(self) -> dict:
        pass

    def _get_flight_time(self) -> int:
        self._flight_time += self._flight_time_increase
        return self._flight_time

    def _get_ground_altitude(self) -> float:
        return self._ground_altitude

    def _get_heading(self) -> float:
        return self._heading

    def _get_is_flying(self) -> bool:
        return self._is_flying

    def _get_pitch(self) -> int:
        return self._attitude_pitch

    def _get_roll(self) -> int:
        return self._attitude_roll

    def _get_speed_x(self) -> int:
        return self._speed_x

    def _get_speed_y(self) -> int:
        return self._speed_y

    def _get_speed_z(self) -> int:
        return self._speed_z

    def _get_temperature(self) -> int:
        return self._temperature

    def _get_yaw(self) -> int:
        return self._attitude_yaw

    def _land(self) -> None:
        self._is_flying = False
        self._ground_altitude = 0
        self._attitude_roll = 0

    def _send_rc_controls(
            self,
            left_right_velocity: int,
            forward_backward_velocity: int,
            up_down_velocity: int,
            yaw_velocity: int
    ) -> None:
        self._speed_y = left_right_velocity
        self._speed_x = forward_backward_velocity
        self._speed_z = up_down_velocity
        self._attitude_pitch += forward_backward_velocity * self._attitude_pitch_rc_coeff
        self._attitude_roll += left_right_velocity * self._attitude_roll_rc_coeff
        self._ground_altitude += up_down_velocity * self._speed_z_rc_coeff
        self._heading += yaw_velocity * self._yaw_rc_coeff

    def _streamoff(self) -> None:
        pass

    def _streamon(self) -> None:
        pass

    def _takeoff(self) -> None:
        self._is_flying = True

    def _turn_motor_off(self) -> None:
        pass

    def _turn_motor_on(self) -> None:
        pass
