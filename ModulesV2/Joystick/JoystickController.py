import ModulesV2.Joystick.Joystick as Joystick
import ModulesV2.Geometry.Point2D as Point2D


class JoystickController:
    """
    Controls a joystick
    """

    def __init__(self):
        self._joystick = Joystick.Joystick()

    def get_joystick_xy(self) -> Point2D:
        """
        Returns the joystick x and y axis values
        """
        return self._joystick.get_xy()

    def reset_joystick_coordinates(self) -> None:
        """
        Reset the joystick x and y axis values to 0, 0
        """
        self._joystick.set_x(0)
        self._joystick.set_y(0)

    def set_joystick_xy(self, coordinates: Point2D):
        """
        Sets the joystick x and y axis values
        """
        self._joystick.set_xy(coordinates)
