import ModulesV2.Geometry.Point2D as Point2D


class Joystick:
    """
    The Model object of a joystick that controls the values from -100 to 100 on two axis
    """

    def __init__(self):
        self._xy = Point2D.Point2D(0, 0)

    def get_xy(self) -> Point2D:
        """
        Returns the x and y of the joystick axis between -100 and 100
        """
        return self._xy

    def get_x(self) -> int:
        """
        Returns the x value of the joystick between -100 and 100
        """
        return self._xy.x

    def get_y(self) -> int:
        """
        Returns the y value of the joystick between -100 and 100
        """
        return self._xy.y

    def set_xy(self, xy: Point2D):
        """
        Sets the x and y axis values of the joystick
        """
        self.set_x(xy.x)
        self.set_y(xy.y)

    def set_x(self, x: int) -> None:
        """
        Set the x value of the joystick between -100 and 100
        """
        if x > 100 or x < -100:
            raise Exception("Values for x must be between -100 and 100")

        self._xy.x = x

    def set_y(self, y: int) -> None:
        """
        Set the y value of the joystick between -100 and 100
        """
        if y > 100 or y < -100:
            raise Exception("Values for x must be between -100 and 100")

        self._xy.y = y
