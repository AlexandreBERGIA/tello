import math
from typing import List


class Point2D:
    """
    Defines a Geometry point with an x and y
    """

    def __init__(self, x: float, y: float):
        self._coordinates = []
        self._coordinates.append(x)
        self._coordinates.append(y)

    def __add__(self, other):
        """
        Add operation sums the x coordinates together, then the y coordinates together
        """
        return Point2D(self.x + other.x, self.y + other.y)

    def __truediv__(self, other):
        """
        Division operator that divides the x and y coordinates
        """
        return Point2D(self.x / other.x, self.y / other.y)

    def __sub__(self, other):
        """
        Subtract operation subtract the x coordinates together, then the y coordinates together
        """
        return Point2D(self.x - other.x, self.y - other.y)

    def __repr__(self):
        """
        Returns the x and y coordinates
        """
        return self.to_string()

    def __str__(self):
        """
        Returns the x and y coordinates
        """
        return self.to_string()

    def rotate(self, rotation_center: [float, float], rotation_rad: float) -> None:
        """
        Make the current Point rotate around the rotation center for the provided rad angle
        """
        # Optimize the computation by calculating the cos and sin only once
        angle_sin = math.sin(rotation_rad)
        angle_cos = math.cos(rotation_rad)
        diff_x = self._coordinates[0] - rotation_center[0]
        diff_y = self._coordinates[1] - rotation_center[1]
        self._coordinates[0] = rotation_center[0] + angle_cos * diff_x - angle_sin * diff_y
        self._coordinates[1] = rotation_center[1] + angle_sin * diff_x + angle_cos * diff_y

    def to_string(self):
        """
        Stringifies the Point2D object
        """
        return 'Point2D({},{})'.format(self.x, self.y)

    @property
    def x(self) -> float:
        return self._coordinates[0]

    @x.setter
    def x(self, x: float) -> None:
        self._coordinates[0] = x

    @property
    def y(self) -> float:
        return self._coordinates[1]

    @y.setter
    def y(self, y: float) -> None:
        self._coordinates[1] = y

    @property
    def point(self) -> List[float]:
        return [self._coordinates[0], self._coordinates[1]]

    @point.setter
    def point(self, point: List[float]) -> None:
        self._coordinates[0] = point[0]
        self._coordinates[1] = point[1]

    @property
    def point_int(self) -> List[int]:
        return [int(self._coordinates[0]), int(self._coordinates[1])]
