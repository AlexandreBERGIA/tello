import logging


class NameThreadFilter(logging.Filterer):
    """
    This logger filter, filters on the provided Thread name
    """

    def __init__(self, thread_name: str):
        super().__init__()

        self._thread_name = thread_name

    def filter(self, record: logging.LogRecord):
        return record.threadName == self._thread_name
