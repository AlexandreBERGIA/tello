import logging
from typing import Dict

from ModulesV2.Time.Chronometer import Chronometer

MESSAGE_FORMAT_TAB = '%(name)-11s %(levelname)-11s %(processName)-s - %(threadName)-20s %(message)-50s %(filename)s:%(lineno)d'
MESSAGE_FORMAT_CSV = '%(asctime)s.%(msecs)03d,%(levelname)s,%(name)s,%(processName)s,%(threadName)s,%(message)s,%(filename)s:%(lineno)d'
MESSAGE_FORMAT_STATISTICS = '%(levelname)s,%(name)s,%(processName)s,%(threadName)s,%(message)s'
TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

LOGGER_MAIN = 'Main'
LOGGER_STATISTICS = 'Statistics'

statistics_logger = logging.getLogger(LOGGER_STATISTICS)
main_logger = logging.getLogger(LOGGER_MAIN)

_statistic_chronometers: Dict[str, Chronometer] = {}


def statistics_start(name: str) -> None:
    """
    Starts a chronometer for the provided name
    """
    chronometer = Chronometer()
    chronometer.start()
    _statistic_chronometers[name] = chronometer


def statistics_stop(name: str) -> None:
    try:
        chronometer = _statistic_chronometers[name]
        if not chronometer.is_started:
            main_logger.warning('The Chronometer was already stopped: {}'.format(name))
        statistics_logger.debug(name + ',' + str(chronometer.get_elapsed_time()))
        chronometer.stop()
    except KeyError:
        main_logger.warning('Invalid Chronometer Name: {}'.format(name))
