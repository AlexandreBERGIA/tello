import cv2
import numpy as np
from cvzone.HandTrackingModule import HandDetector
from djitellopy import tello
import numpy
import time
from Modules.Control import Joystick as JoystickControl, DroneControl
from Modules.HUD import Drone as DroneHUD, Joystick as JoystickHUD, Inputs as InputsHUD

# Declare the control variables
longitRange = [8000, 9000]
maxSpeed = [-30, 30]
workingImageSize = [1600, 1000]
pid = [0.4, 0.4, 0]
pError = (0, 0)
controls = [0, 0, 0, 0]

# Declare the safety variables
droneEnabled = False
handControlEnabled = True
droneControlEnabled = False
lowBatteryLevel = 5

# Init the hand detector
if handControlEnabled:
    detector = HandDetector(detectionCon=0.8, maxHands=2)
joyCap = cv2.VideoCapture(1)

# Init the drone frame capture. If the drone is disabled, we generate a black image
drone = None
if droneEnabled:
    drone = tello.Tello()
    drone.connect()
    drone.streamon()
    droneImage = drone.get_frame_read().frame
else:
    droneImage = np.zeros([workingImageSize[0], workingImageSize[1], 3], dtype=np.uint8)
    droneImage.fill(0)

joystickControl = JoystickControl.Joystick()
joystickHUD = JoystickHUD.Joystick(joystickControl)
inputHUD = InputsHUD.Inputs()
droneHUD = DroneHUD.Drone()
droneControl = DroneControl.DroneControl(drone, joystickControl, droneEnabled, droneControlEnabled)
droneControl.droneEnabled = droneEnabled
droneControl.droneControlEnabled = droneControlEnabled


# Declare the time variables to calculate the loop time
startTime = time.time()
endTime = time.time()
loopDuration = 0

while True:
    # startTime = time.time()

    # Get the images and resize them to the standard
    # _, joystickImage = joyCap.read()
    # joystickImage = cv2.flip(joystickImage, 1)
    # joystickImage = cv2.resize(joystickImage, workingImageSize)
    # if droneEnabled:
    #     droneImage = drone.get_frame_read().frame
    # droneImage = cv2.resize(droneImage, workingImageSize)
    #
    # # Get the Hands and the joystick inputs
    # if handControlEnabled:
    #     detectedHands, joystickImage = detector.findHands(joystickImage, True, False)
    #     joystickControl.hands = detectedHands
    #
    # # Apply the joystick inputs
    # if handControlEnabled:
    #     droneControl.setInputs(detectedHands, detector)
    # droneControl.applyInputs(joystickImage, joystickControl.getJoystickInputs())
    #
    # # Push the times from the previous loop
    # droneControl.droneInputs["SystemStatusLoopTime"].setValue(loopDuration)
    #
    # # Paint the Done and Input Hud
    # inputHUD.paint(joystickImage, droneControl)
    # joystickHUD.paint(joystickImage)
    # droneHUD.paint(joystickImage, droneImage, droneControl)
    #
    # # Concatenate the images and display them
    # workingImage = numpy.concatenate((joystickImage, droneImage), 0)
    # cv2.imshow("Output", workingImage)
    # cv2.waitKey(1)
    #
    # # Calculate the average time to loop over the n previous frames, and display it on the next loop
    # endTime = time.time()
    # loopDuration = round((endTime - startTime) * 1000, 0)

    _, joystickImage = joyCap.read()
    # joystickImage = cv2.flip(joystickImage, 1)
    # joystickImage = cv2.resize(joystickImage, workingImageSize)
    cv2.imshow("test Working Image", joystickImage)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

joyCap.release()
cv2.destroyAllWindows()
