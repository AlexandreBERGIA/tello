import logging
import time

import cv2
import pygame as pygame

from ModulesV2.Drones import DroneFactory
from ModulesV2.Geometry.Point2D import Point2D
from ModulesV2.Image import ImageFetcherFactory
from ModulesV2.Loggers import Logger
from ModulesV2.Time.Chronometer import Chronometer

# Declare the initial variables
# drone_ref = DroneFactory.DJI_TELLO
drone_ref = DroneFactory.STUB
computer_camera_index = 1  # -1 for a black image loader, >=0 to use a real connected camera
camera_ref = ImageFetcherFactory.FETCHER_CAMERA_COMPUTER if computer_camera_index >= 0 else ImageFetcherFactory.FETCHER_STUB_BLACK
working_image_size = Point2D(1000, 500)
logging_level = logging.INFO
# logging_level = main_logger.info

# Init the logger
logging.basicConfig(format=Logger.MESSAGE_FORMAT_TAB, level=logging_level, datefmt=Logger.TIME_FORMAT)
main_logger = logging.getLogger(Logger.LOGGER_MAIN)

# Defines the drone and connect to it
main_logger.info('Instance the Drone')
drone = DroneFactory.create(drone_ref)
main_logger.info('Drone instance: {}'.format(drone.__class__))
drone.connect()

# Defines the image fetchers to gather images
main_logger.info('Instance Image Fetchers')
image_fetcher_factory = ImageFetcherFactory.ImageFetcherFactory()
image_fetcher_factory.drone = drone
image_fetcher_factory.camera_index = computer_camera_index
drone_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_CAMERA_DRONE)
# hud_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_STUB_BLACK)
camera_image_fetcher = image_fetcher_factory.create(camera_ref)
# status_image_fetcher = image_fetcher_factory.create(ImageFetcherFactory.FETCHER_STUB_BLACK)
drone.image_fetcher = drone_image_fetcher

main_logger.info('Start Image Fetcher Threads')

main_loop_sleep_time = 0.01
main_loop_chronometer = Chronometer()

# Integration with pygame
pygame.init()
pygame.display.set_caption("Tello video stream")
screen = pygame.display.set_mode([200, 200])
# screen.fill([0, 0, 0])
pygame.display.update()


def keydown(key):
    print(key)
    if key == pygame.K_w:
        logging.info('Pressed key W')
    elif key == pygame.K_a:
        logging.info('Pressed key A')
    elif key == pygame.K_s:
        logging.info('Pressed key S')
    elif key == pygame.K_d:
        logging.info('Pressed key D')


def keyup(key):
    print(key)
    if key == pygame.K_w:
        logging.info('Unpressed key W')
    elif key == pygame.K_a:
        logging.info('Unpressed key A')
    elif key == pygame.K_s:
        logging.info('Unpressed key S')
    elif key == pygame.K_d:
        logging.info('Unpressed key D')


while True:
    main_loop_chronometer.start()
    should_stop = False

    # CV2 implementation --> Doesn't detect multiple keys
    # pressed_key = cv2.waitKey(1) & 0xFF
    # if pressed_key == ord('q'):
    #     exit_main_loop = True

    # Keyboard implementation --> Doesn't work with Mac due to security constraints
    # if keyboard.read_key() == "q":
    #     exit_main_loop = True

    # Pygame events --> works nicely but generates OS sound when combining cv2 display with pygame keyboard capture
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            should_stop = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                should_stop = True
            else:
                keydown(event.key)
        elif event.type == pygame.KEYUP:
            keyup(event.key)

    if should_stop:
        break

    camera_image_fetcher.fetch_image()
    drone_image_fetcher.fetch_image()

    drone_image = drone_image_fetcher.get_image()
    camera_image = camera_image_fetcher.get_image()
    if drone_image is not None:
        cv2.imshow('Drone', drone_image)
    if camera_image is not None:
        cv2.imshow('Camera', camera_image)

    cv2.pollKey()

    time.sleep(main_loop_sleep_time)
    main_loop_chronometer.save_elapsed_time()
    main_loop_time = main_loop_chronometer.get_elapsed_time()
    main_loop_chronometer.stop()
